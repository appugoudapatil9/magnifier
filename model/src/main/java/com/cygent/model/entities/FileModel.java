/*
package com.cygent.model.entities;

import android.util.Log;

import com.cygent.model.entities.database.ResourceEntity;

import java.util.ArrayList;
import java.util.List;

*/
/**
 * Created by hsnirmal on 5/24/2016.
 *//*

public class FileModel {

    public static final String TAG = FileModel.class.getSimpleName();

    int type;
    String imagePath;
    int imageId;

    Long folderId;
    String name;
    int numberOfElements;
    Long baseFolderId;
    List<FileModel> fileModelList;

    public List<FileModel> getFileModelList() {
        return fileModelList;
    }

    public void setFileModelList(List<FileModel> fileModelList) {
        this.fileModelList = fileModelList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public Long getFolderId() {
        return folderId;
    }

    public void setFolderId(Long folderId) {
        this.folderId = folderId;
    }

    public int getNumberOfElements() {
        return numberOfElements;
    }

    public void setNumberOfElements(int numberOfElements) {
        this.numberOfElements = numberOfElements;
    }

    public Long getBaseFolderId() {
        return baseFolderId;
    }

    public void setBaseFolderId(Long baseFolderId) {
        this.baseFolderId = baseFolderId;
    }

    @Override
    public String toString() {
        return "FileModel{" +
                "name='" + name + '\'' +
                ", type=" + type +
                ", imagePath='" + imagePath + '\'' +
                ", imageId=" + imageId +
                '}';
    }


    public static List<FileModel> getElementsForFolder(FileModel argmFileModel){
        List<FileModel> dataList = new ArrayList<>();
        Long folderId =  argmFileModel.getFolderId();

        int numberOfElements = argmFileModel.getNumberOfElements()/2;

        Log.d(TAG,"numberOfElements :"+numberOfElements);

        for (int i = 0; i < argmFileModel.getNumberOfElements() ; i++) {
            folderId++;
            String strfolderId = argmFileModel.getFolderId()+""+folderId;
            Long folderIdAssigned = Long.parseLong(strfolderId);
            FileModel fileModel = new FileModel();
            fileModel.setFolderId(folderIdAssigned);
            fileModel.setName("Android "+folderIdAssigned);
            fileModel.setNumberOfElements(numberOfElements);
            fileModel.setBaseFolderId(argmFileModel.getFolderId());
            dataList.add(fileModel);
        }
        return  dataList;
    }


*/
/*    public static List<ResourceEntity> getFragmentedList(ResourceEntity arg_ResourceEntity, final int L) {
        List<ResourceEntity> fragList = new ArrayList<ResourceEntity>();
        final int N = arg_ResourceEntity.getResourceEntityList().size();
        for (int i = 0; i < N; i += L) {

            ResourceEntity mResourceEntity = new ResourceEntity();
            mResourceEntity.setResourceEntityList(arg_ResourceEntity.getResourceEntityList().subList(i, Math.min(N, i + L)));
            fragList.add(mResourceEntity);
        }
        return fragList;
    }*//*


}

*/
