package com.cygent.model.entities.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hsnirmal on 1/24/2017.
 */

public class UpdateSyncStatus {

    @SerializedName("UserID")
    @Expose
    private Long userID;

    @SerializedName("AppUserResourceID")
    @Expose
    private Long userResourceID;

    @SerializedName("SyncStatus")
    @Expose
    private int  syncStatus;

    /**
     *
     * @return
     * The userID
     */
    public Long getUserID() {
        return userID;
    }

    /**
     *
     * @param userID
     * The UserID
     */
    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public Long getUserResourceID() {
        return userResourceID;
    }

    public void setUserResourceID(Long userResourceID) {
        this.userResourceID = userResourceID;
    }

    public int getSyncStatus() {
        return syncStatus;
    }

    public void setSyncStatus(int syncStatus) {
        this.syncStatus = syncStatus;
    }
}
