package com.cygent.model.entities.database;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.accounts.Account;
import android.content.Context;
import android.content.Intent;

import com.cygent.framework.utils.AppLog;
import com.cygent.model.entities.request.UpdateViewCountRequest;
import com.cygent.model.entities.response.Resource;
import com.cygent.model.utils.Constants;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

public class DatabaseManager {

    public static final String TAG = DatabaseManager.class.getSimpleName();

    private static DatabaseManager instance = null;
    private static DbHelper dbHelper;

    private DatabaseManager() {
        // TODO Auto-generated constructor stub
    }

    public static DatabaseManager getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseManager();
            dbHelper = new DbHelper(context);
        }
        return instance;
    }

    private DbHelper getHelper() {
        return this.dbHelper;
    }

    public void insertUpdateResource(List<ResourceEntity> resourceEntityList) {
        //All Folders before delete
        List<ResourceEntity> oldFolderList = new ArrayList<>();
        oldFolderList.addAll(getAllFolders());
        List<Long> oldFolderIds = new ArrayList<>();
        for (ResourceEntity resourceEntity : oldFolderList){
            oldFolderIds.add(resourceEntity.getFolderId());
        }
        try {
            //First delete all folder resources from database
            DeleteBuilder<ResourceEntity, Integer> deleteBuilder =
                    dbHelper.getResourceEntityDao().deleteBuilder();
            // only delete the rows where isResource is false
            deleteBuilder.where().eq(Constants.Database.COLUMN_IS_RESOURCE, false);
            int rowsDeleted = deleteBuilder.delete();

            AppLog.d(TAG, "Deleted rows :" + rowsDeleted);
            AppLog.d(TAG, "All folder deleted");

            for (ResourceEntity resourceEntity : resourceEntityList) {

                List<ResourceEntity> mResourceEntityList = new ArrayList<>();

                if (resourceEntity.getResourceId() != 0L) {//this is resource
                    QueryBuilder<ResourceEntity, Integer> qb = dbHelper.getResourceEntityDao().queryBuilder();
                    Where where = qb.where();
                    where.eq(Constants.Database.COLUMN_RESOURCE_ID, resourceEntity.getResourceId());
                    mResourceEntityList = qb.query();

                    if (mResourceEntityList.size() > 0) {
                        //Record is already exists so update it
                        if (resourceEntity.getSyncStatus() == 3)//deleted on server
                        {
                            //Delete record on local
                            dbHelper.getResourceEntityDao().delete(mResourceEntityList.get(0));

                        } else if (resourceEntity.getSyncStatus() == 2) {//updated on server
                            //update and give opt to download on local
                            resourceEntity.setId(mResourceEntityList.get(0).getId());
                            resourceEntity.setDownloaded(false);
                            dbHelper.getResourceEntityDao().update(resourceEntity);
                        } else {

                            if (resourceEntity.getSeqNumber() != mResourceEntityList.get(0).getSeqNumber()) {
                                //here sequence number is updated so update that entry
                                resourceEntity.setId(mResourceEntityList.get(0).getId());
                                resourceEntity.setDownloaded(false);
                                dbHelper.getResourceEntityDao().update(resourceEntity);
                            }
                        }
                    } else {
                        //New resource so insert new one if sycn status is not 3
                        if (resourceEntity.getSyncStatus() != 3)
                            dbHelper.getResourceEntityDao().create(resourceEntity);
                    }
                } else {// this is folder simply add it if and only if it is not empty
//                    if (resourceEntity.getResourceCount() != 0)
                    dbHelper.getResourceEntityDao().create(resourceEntity);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        //new Folder list
        List<ResourceEntity> newFolderList = new ArrayList<>();
        newFolderList.addAll(getAllFolders());

        List<Long> newFolderIds = new ArrayList<>();
        for (ResourceEntity resourceEntity : newFolderList){
            newFolderIds.add(resourceEntity.getFolderId());
        }


        //Deleted folder list
        List<Long> deletedFolderList = new ArrayList<>();
        for(Long lId : oldFolderIds){
            if(!(newFolderIds.contains(lId))){
                deletedFolderList.add(lId);
            }
        }

        //Now delete all resources under deleted folder
        for(Long deletedId : deletedFolderList){
            deleteResourcesUnderFolder(deletedId);
        }

    }

    public void deleteResourcesUnderFolder(Long folderId){
        DeleteBuilder<ResourceEntity, Integer> deleteBuilder =
                dbHelper.getResourceEntityDao().deleteBuilder();
        // only delete the rows where isResource is false
        try {
            deleteBuilder.where().eq(Constants.Database.COLUMN_RESOURCE_PARENT_FOLDER_ID, folderId);
            int rowsDeleted = deleteBuilder.delete();
            AppLog.d(TAG, "Deleted rows :" + rowsDeleted);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<ResourceEntity> getAllFolders(){
        List<ResourceEntity> resourceEntityList = new ArrayList<>();
        QueryBuilder<ResourceEntity, Integer> qb = dbHelper.getResourceEntityDao().queryBuilder();
        Where where = qb.where();

        try {
            where.eq(Constants.Database.COLUMN_IS_RESOURCE, false);
            resourceEntityList = qb.query();
        } catch (SQLException e) {
            e.printStackTrace();
            AppLog.d(TAG, "getResourceListForEntity : SQL Exception " + e.getMessage());
        }

        return resourceEntityList;
    }

    public void resetCountForFolders(){

        List<ResourceEntity> resourceEntityList = new ArrayList<>();
        QueryBuilder<ResourceEntity, Integer> qb = dbHelper.getResourceEntityDao().queryBuilder();
        Where where = qb.where();
        // the name field must be equal to "foo"
        try {
            where.eq(Constants.Database.COLUMN_IS_RESOURCE, 0);
            resourceEntityList = qb.query();

            //now we have all folders set resource count for all folders

            //First of all Find all base folders in app
            //and make list of all base folders


            List<ResourceEntity> allBaseFolders = new ArrayList<>();
            for(ResourceEntity folderEntity : resourceEntityList){

                if(getNumberOfChildFolders(folderEntity) == 0){
                    allBaseFolders.add(folderEntity);
                }


                for(ResourceEntity resourceEntity : allBaseFolders){
                        setResourceCountValueForFodler(resourceEntity);
                }

            }

        } catch (SQLException e) {
            e.printStackTrace();
            AppLog.d(TAG, "getResourceListForEntity : SQL Exception " + e.getMessage());
        }


    }

    public void setResourceCountValueForFodler(ResourceEntity arg_resourceEntity){
        int numberOfChildResourcesToDownload = 0;
        try {

            QueryBuilder<ResourceEntity, Integer> qb = dbHelper.getResourceEntityDao().queryBuilder();
            Where where = qb.where();

            where.eq(Constants.Database.COLUMN_RESOURCE_PARENT_FOLDER_ID, arg_resourceEntity.folderId);
            // and
            where.and();

            where.eq(Constants.Database.COLUMN_IS_RESOURCE, 1);
            where.and();

            where.eq(Constants.Database.COLUMN_RESOURCE_IS_DOWNLOADED, 0);

            numberOfChildResourcesToDownload = (int) qb.countOf();


            //now update count values for each folder that we got in above query
            UpdateBuilder<ResourceEntity, Integer> updateBuilder = dbHelper.getResourceEntityDao().updateBuilder();
            // set the criteria like you would a QueryBuilder
            updateBuilder.where().eq(Constants.Database.COLUMN_FOLDER_ID, arg_resourceEntity.folderId);
            // update the value of your field(s)
            updateBuilder.updateColumnValue(Constants.Database.COLUMN_RESOURCE_RES_COUNT, numberOfChildResourcesToDownload);
            updateBuilder.update();


        } catch (SQLException e) {
            e.printStackTrace();
        }



    }

    public int getNumberOfChildFolders(ResourceEntity arg_enEntity) {

        int numberOfChildFolders = 0;
        try {

            QueryBuilder<ResourceEntity, Integer> qb = dbHelper.getResourceEntityDao().queryBuilder();
            Where where = qb.where();

            where.eq(Constants.Database.COLUMN_RESOURCE_PARENT_FOLDER_ID, arg_enEntity.folderId);
            // and
            where.and();

            where.eq(Constants.Database.COLUMN_IS_RESOURCE, 0);
            numberOfChildFolders = (int) qb.countOf();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return numberOfChildFolders;
    }


    public List<ResourceEntity> getResourceListForEntity(Long a_folderId) {
        List<ResourceEntity> resourceEntityList = new ArrayList<>();
        QueryBuilder<ResourceEntity, Integer> qb = dbHelper.getResourceEntityDao().queryBuilder();
        Where where = qb.where();
        // the name field must be equal to "foo"
        try {
            where.eq(Constants.Database.COLUMN_RESOURCE_PARENT_FOLDER_ID, a_folderId);
            resourceEntityList = qb.query();
        } catch (SQLException e) {
            e.printStackTrace();
            AppLog.d(TAG, "getResourceListForEntity : SQL Exception " + e.getMessage());
        }

        List<ResourceEntity> fileResourceList = new ArrayList<>();
        List<ResourceEntity> folderResourceList = new ArrayList<>();

        if(resourceEntityList!= null && resourceEntityList.size()>0) {
            for (ResourceEntity re : resourceEntityList) {
                if (re.isResource) {
                    fileResourceList.add(re);
                } else {
                    folderResourceList.add(re);
                }
            }

            resourceEntityList.clear();
        }

        if(fileResourceList.size()>0){
            Collections.sort(fileResourceList, new ResourceEntity.SequenceComparator());
            resourceEntityList.addAll(fileResourceList);
        }

        if(folderResourceList.size()>0){
            Collections.sort(folderResourceList, new ResourceEntity.SequenceComparator());
            resourceEntityList.addAll(folderResourceList);
        }

        //Sort all resources based on sequence number
//        Collections.sort(resourceEntityList, new ResourceEntity.SequenceComparator());
        //

        return resourceEntityList;
    }

    public void updateResourceLocalPath(ResourceEntity resourceEntity, String updatedPath) {

        try {
            UpdateBuilder<ResourceEntity, Integer> updateBuilder = dbHelper.getResourceEntityDao().updateBuilder();
// set the criteria like you would a QueryBuilder
            updateBuilder.where().eq(Constants.Database.COLUMN_ID, resourceEntity.getId());
// update the value of your field(s)
            updateBuilder.updateColumnValue(Constants.Database.COLUMN_RESOURCE_LOCAL_PATH, updatedPath);
            updateBuilder.update();
        } catch (SQLException e) {
            e.printStackTrace();
            AppLog.d(TAG, "updateResourceLocalPath : SQL Exception " + e.getMessage());
        }
    }

    public void updateResource(ResourceEntity resourceEntity) {
        if (resourceEntity != null && resourceEntity.getId() != 0L) {
            try {
                dbHelper.getResourceEntityDao().update(resourceEntity);
            } catch (SQLException e) {
                AppLog.d(TAG, "SQLException updateResource :" + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public void updateFolderHierarchy(ResourceEntity resourceEntity) {
        ResourceEntity mResourceEntity = resourceEntity;
        if(mResourceEntity.getParentFolderId()!=0L){
            do {
                mResourceEntity = getParentResourceEntity(mResourceEntity);
                updateResourceCount(mResourceEntity);
            } while (mResourceEntity.getParentFolderId() != 0L);
        }
    }

    public void updateFolderHierarchyForDownloadManager(ResourceEntity resourceEntity) {
        ResourceEntity mResourceEntity = resourceEntity;
        if(mResourceEntity.getParentFolderId()!=0L){
            do {
                mResourceEntity = getParentResourceEntity(mResourceEntity);
                updateResourceCountForAdd(mResourceEntity);
            } while (mResourceEntity.getParentFolderId() != 0L);
        }
    }

    public void updateResourceCountForAdd(ResourceEntity resourceEntity) {
        try {
            UpdateBuilder<ResourceEntity, Integer> updateBuilder = dbHelper.getResourceEntityDao().updateBuilder();
// set the criteria like you would a QueryBuilder
// update the value of your field(s)
            updateBuilder.where().eq(Constants.Database.COLUMN_ID, resourceEntity.getId());
            updateBuilder.updateColumnValue(Constants.Database.COLUMN_RESOURCE_RES_COUNT, resourceEntity.getResourceCount() + 1);
            updateBuilder.update();
        } catch (SQLException e) {
            e.printStackTrace();
            AppLog.d(TAG, "updateResourceViewCount : SQL Exception :" + e.getMessage());
        }
    }

    public ResourceEntity getParentResourceEntity(ResourceEntity resourceEntity) {
        ResourceEntity mResourceEntity = resourceEntity;
        List<ResourceEntity> resourceEntityList = new ArrayList<>();
        QueryBuilder<ResourceEntity, Integer> queryBuilder = dbHelper.getResourceEntityDao().queryBuilder();
        try {
            queryBuilder.where().eq(Constants.Database.COLUMN_FOLDER_ID, mResourceEntity.getParentFolderId());
            resourceEntityList = queryBuilder.query();
            if (resourceEntityList.size() > 0)
                return resourceEntityList.get(0);
        } catch (SQLException e) {
            AppLog.d(TAG, "getResourceList : SQL Exception :" + e.getMessage());
            e.printStackTrace();
            // return empty list
            return null;
        }

        return null;
    }

    public void updateResourceViewCount(ResourceEntity resourceEntity) {
        try {
            UpdateBuilder<ResourceEntity, Integer> updateBuilder = dbHelper.getResourceEntityDao().updateBuilder();
// set the criteria like you would a QueryBuilder
// update the value of your field(s)
            updateBuilder.where().eq(Constants.Database.COLUMN_ID, resourceEntity.getId());
            updateBuilder.updateColumnValue(Constants.Database.COLUMN_RESOURCE_NumberOfViews, resourceEntity.getNumberOfViews());
            updateBuilder.update();
        } catch (SQLException e) {
            e.printStackTrace();
            AppLog.d(TAG, "updateResourceViewCount : SQL Exception :" + e.getMessage());
        }
    }


    public void updateResourceCount(ResourceEntity resourceEntity) {
        try {
            UpdateBuilder<ResourceEntity, Integer> updateBuilder = dbHelper.getResourceEntityDao().updateBuilder();
// set the criteria like you would a QueryBuilder
// update the value of your field(s)
            updateBuilder.where().eq(Constants.Database.COLUMN_ID, resourceEntity.getId());
            updateBuilder.updateColumnValue(Constants.Database.COLUMN_RESOURCE_RES_COUNT, resourceEntity.getResourceCount() - 1);
            updateBuilder.update();
        } catch (SQLException e) {
            e.printStackTrace();
            AppLog.d(TAG, "updateResourceViewCount : SQL Exception :" + e.getMessage());
        }
    }

    public void resetResourceViewCount(List<ResourceEntity> arg_list)
    {
        for(ResourceEntity resourceEntity : arg_list){
            try {
                UpdateBuilder<ResourceEntity, Integer> updateBuilder = dbHelper.getResourceEntityDao().updateBuilder();
// set the criteria like you would a QueryBuilder
// update the value of your field(s)
                updateBuilder.where().eq(Constants.Database.COLUMN_ID, resourceEntity.getId());
                updateBuilder.updateColumnValue(Constants.Database.COLUMN_RESOURCE_NumberOfViews, 0);
                updateBuilder.updateColumnValue(Constants.Database.COLUMN_TIME_SPENT,0L);
                updateBuilder.update();
            } catch (SQLException e) {
                e.printStackTrace();
                AppLog.d(TAG, "resetResourceViewCount : SQL Exception :" + e.getMessage());
            }
        }

    }

    public List<ResourceEntity> getDownloadedResourceList() {
        List<ResourceEntity> resourceEntityList = new ArrayList<>();
        QueryBuilder<ResourceEntity, Integer> queryBuilder = dbHelper.getResourceEntityDao().queryBuilder();
        try {
            queryBuilder.where().eq(Constants.Database.COLUMN_RESOURCE_IS_DOWNLOADED, true);
            resourceEntityList = queryBuilder.query();
            return resourceEntityList;
        } catch (SQLException e) {
            AppLog.d(TAG, "getDownloadedResourceList : SQL Exception :" + e.getMessage());
            e.printStackTrace();
            // return empty list
            return resourceEntityList;
        }
    }

    public int deleteResource(ResourceEntity resourceEntity) {
        try {
            return dbHelper.getResourceEntityDao().delete(resourceEntity);
        } catch (SQLException e) {
            AppLog.d(TAG, "deleteResource : SQLException :" + e.getMessage());
            e.printStackTrace();
            return 0;
        }
    }

    public List<ResourceEntity> getResourceList() {
        List<ResourceEntity> resourceEntityList = new ArrayList<>();
        QueryBuilder<ResourceEntity, Integer> queryBuilder = dbHelper.getResourceEntityDao().queryBuilder();
        try {
            queryBuilder.where().eq(Constants.Database.COLUMN_IS_RESOURCE, true);
            resourceEntityList = queryBuilder.query();
            return resourceEntityList;
        } catch (SQLException e) {
            AppLog.d(TAG, "getResourceList : SQL Exception :" + e.getMessage());
            e.printStackTrace();
            // return empty list
            return resourceEntityList;
        }
    }

    public List<ResourceEntity> getAllAccessedResources() {
        List<ResourceEntity> resourceEntityList = new ArrayList<>();
        QueryBuilder<ResourceEntity, Integer> queryBuilder = dbHelper.getResourceEntityDao().queryBuilder();
        try {
            queryBuilder.where().gt(Constants.Database.COLUMN_RESOURCE_NumberOfViews, 0);
            resourceEntityList = queryBuilder.query();
            return resourceEntityList;
        } catch (SQLException e) {
            AppLog.d(TAG, "getResourceList : SQL Exception :" + e.getMessage());
            e.printStackTrace();
            // return empty list
            return resourceEntityList;
        }
    }


}
