package com.cygent.model.entities.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hsnirmal on 5/11/2016.
 */
public class GetResourceRequest {

    @SerializedName("UserID")
    @Expose
    private Long UserID;
//    @SerializedName("Username")
//    @Expose
//    private String Username;
    @SerializedName("BUId")
    @Expose
    private Long ClientID;

    /**
     *
     * @return
     * The UserID
     */
    public Long getUserID() {
        return UserID;
    }

    /**
     *
     * @param UserID
     * The UserID
     */
    public void setUserID(Long UserID) {
        this.UserID = UserID;
    }

    /**
     *
     * @return
     * The Username
     */
//    public String getUsername() {
//        return Username;
//    }

    /**
     *
     * @param Username
     * The Username
     */
//    public void setUsername(String Username) {
//        this.Username = Username;
//    }

    /**
     *
     * @return
     * The ClientID
     */
    public Long getClientID() {
        return ClientID;
    }

    /**
     *
     * @param ClientID
     * The ClientID
     */
    public void setClientID(Long ClientID) {
        this.ClientID = ClientID;
    }
}
