package com.cygent.model.entities.request;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by hsnirmal on 12/5/2016.
 */

public class LwActivateRequest {


    public LwActivateRequest() {}

    public LwActivateRequest(String iPadPin) {
        this.iPadPin = iPadPin;
    }

    @SerializedName("iPadPin")
    @Expose
    private String iPadPin;

    /**
     *
     * @return
     * The iPadPin
     */
    public String getIPadPin() {
        return iPadPin;
    }

    /**
     *
     * @param iPadPin
     * The iPadPin
     */
    public void setIPadPin(String iPadPin) {
        this.iPadPin = iPadPin;
    }

}