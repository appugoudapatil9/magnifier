package com.cygent.model.entities.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hsnirmal on 6/8/2016.
 */
public class AcknowledgementRequest {

    @SerializedName("UserID")
    @Expose
    private Long userID;
//    @SerializedName("Username")
//    @Expose
//    private String username;
//    @SerializedName("ClientID")
//    @Expose
//    private Long clientID;
    @SerializedName("AppUserResourceID")
    @Expose
    private Long userResourceID;

    /**
     *
     * @return
     * The userID
     */
    public Long getUserID() {
        return userID;
    }

    /**
     *
     * @param userID
     * The UserID
     */
    public void setUserID(Long userID) {
        this.userID = userID;
    }

    /**
     *
     * @return
     * The username
     */
//    public String getUsername() {
//        return username;
//    }

    /**
     *
     * @param username
     * The Username
     */
//    public void setUsername(String username) {
//        this.username = username;
//    }

    /**
     *
     * @return
     * The clientID
     */
//    public Long getClientID() {
//        return clientID;
//    }

    /**
     *
     * @param clientID
     * The ClientID
     */
//    public void setClientID(Long clientID) {
//        this.clientID = clientID;
//    }

    /**
     *
     * @return
     * The userResourceID
     */
    public Long getUserResourceID() {
        return userResourceID;
    }

    /**
     *
     * @param userResourceID
     * The UserResourceID
     */
    public void setUserResourceID(Long userResourceID) {
        this.userResourceID = userResourceID;
    }

}
