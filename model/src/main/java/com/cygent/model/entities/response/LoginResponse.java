package com.cygent.model.entities.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hsnirmal on 5/9/2016.
 */
public class LoginResponse{


    @SerializedName("ClientAdminEmail")
    @Expose
    private String ClientAdminEmail;
    @SerializedName("BUId")
    @Expose
    private Long ClientID;
    @SerializedName("Disclaimer")
    @Expose
    private String LibraryDisclaimer;
    @SerializedName("SiteName")
    @Expose
    private String SiteName;
    @SerializedName("UserID")
    @Expose
    private Long UserID;
    @SerializedName("Username")
    @Expose
    private String Username;
    @SerializedName("hasRightsforLibraryDisclaimer")
    @Expose
    private Boolean hasRightsforLibraryDisclaimer;

    @SerializedName("ClientName")
    @Expose
    private String clientName;


    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    /**
     * @return The ClientAdminEmail
     */
    public String getClientAdminEmail() {
        return ClientAdminEmail;
    }

    /**
     * @param ClientAdminEmail The ClientAdminEmail
     */
    public void setClientAdminEmail(String ClientAdminEmail) {
        this.ClientAdminEmail = ClientAdminEmail;
    }

    /**
     * @return The ClientID
     */
    public Long getClientID() {
        return ClientID;
    }

    /**
     * @param ClientID The ClientID
     */
    public void setClientID(Long ClientID) {
        this.ClientID = ClientID;
    }

    /**
     * @return The LibraryDisclaimer
     */
    public String getLibraryDisclaimer() {
        return LibraryDisclaimer;
    }

    /**
     * @param LibraryDisclaimer The LibraryDisclaimer
     */
    public void setLibraryDisclaimer(String LibraryDisclaimer) {
        this.LibraryDisclaimer = LibraryDisclaimer;
    }

    /**
     * @return The SiteName
     */
    public String getSiteName() {
        return SiteName;
    }

    /**
     * @param SiteName The SiteName
     */
    public void setSiteName(String SiteName) {
        this.SiteName = SiteName;
    }

    /**
     * @return The UserID
     */
    public Long getUserID() {
        return UserID;
    }

    /**
     * @param UserID The UserID
     */
    public void setUserID(Long UserID) {
        this.UserID = UserID;
    }

    /**
     * @return The Username
     */
    public String getUsername() {
        return Username;
    }

    /**
     * @param Username The Username
     */
    public void setUsername(String Username) {
        this.Username = Username;
    }

    /**
     * @return The hasRightsforLibraryDisclaimer
     */
    public Boolean getHasRightsforLibraryDisclaimer() {
        return hasRightsforLibraryDisclaimer;
    }

    /**
     * @param hasRightsforLibraryDisclaimer The hasRightsforLibraryDisclaimer
     */
    public void setHasRightsforLibraryDisclaimer(Boolean hasRightsforLibraryDisclaimer) {
        this.hasRightsforLibraryDisclaimer = hasRightsforLibraryDisclaimer;
    }

    @Override
    public String toString() {
        return "LoginResponse{" +
                "ClientAdminEmail='" + ClientAdminEmail + '\'' +
                ", ClientID=" + ClientID +
                ", LibraryDisclaimer='" + LibraryDisclaimer + '\'' +
                ", SiteName='" + SiteName + '\'' +
                ", UserID=" + UserID +
                ", Username='" + Username + '\'' +
                ", hasRightsforLibraryDisclaimer=" + hasRightsforLibraryDisclaimer +
                ", clientName='" + clientName + '\'' +
                '}';
    }
}
