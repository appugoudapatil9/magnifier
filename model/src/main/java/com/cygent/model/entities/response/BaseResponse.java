package com.cygent.model.entities.response;

import android.text.TextUtils;

import com.cygent.model.ModelApp;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hrdudhat on 1/8/2016.
 */
public class BaseResponse<T> {

    @SerializedName("ResponseCode")
    @Expose
    private Integer responseCode;
    @SerializedName("ResponseMessage")
    @Expose
    private String responseMessage;
    @SerializedName("ServerDateTime")
    @Expose
    private String serverDateTime;
    @SerializedName("ResponseJSON")
    @Expose
    private T responseJSON;

    /**
     *
     * @return
     * The responseCode
     */
    public Integer getResponseCode() {
        return responseCode;
    }

    /**
     *
     * @param responseCode
     * The ResponseCode
     */
    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    /**
     *
     * @return
     * The responseMessage
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     *
     * @param responseMessage
     * The ResponseMessage
     */
    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /**
     *
     * @return
     * The serverDateTime
     */
    public String getServerDateTime() {
        return serverDateTime;
    }

    /**
     *
     * @param serverDateTime
     * The ServerDateTime
     */
    public void setServerDateTime(String serverDateTime) {
        this.serverDateTime = serverDateTime;
    }

    /**
     *
     * @return
     * The responseJSON
     */
    public T getResponseJSON() {
        return responseJSON;
    }

    /**
     *
     * @param responseJSON
     * The ResponseJSON
     */
    public void setResponseJSON(T responseJSON) {
        this.responseJSON = responseJSON;
    }

}
