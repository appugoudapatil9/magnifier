package com.cygent.model.entities.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeviceTokenRequest {

    @SerializedName("UserID")
    @Expose
    private long UserID;
    //    @SerializedName("Username")
//    @Expose
//    private String Username;
    @SerializedName("BUId")
    @Expose
    private long ClientID;
    @SerializedName("DeviceToken")
    @Expose
    private String DeviceToken;
    @SerializedName("ThemeVersion")
    @Expose
    private Integer ThemeVersion;

    /**
     * @return The UserID
     */
    public long getUserID() {
        return UserID;
    }

    /**
     * @param UserID The UserID
     */
    public void setUserID(long UserID) {
        this.UserID = UserID;
    }

    /**
     * @return The Username
     */
//    public String getUsername() {
//        return Username;
//    }

    /**
     * @param Username The Username
     */
//    public void setUsername(String Username) {
//        this.Username = Username;
//    }

    /**
     * @return The ClientID
     */
    public long getClientID() {
        return ClientID;
    }

    /**
     * @param ClientID The ClientID
     */
    public void setClientID(long ClientID) {
        this.ClientID = ClientID;
    }

    /**
     * @return The DeviceToken
     */
    public String getDeviceToken() {
        return DeviceToken;
    }

    /**
     * @param DeviceToken The DeviceToken
     */
    public void setDeviceToken(String DeviceToken) {
        this.DeviceToken = DeviceToken;
    }

    public Integer getThemeVersion() {
        return ThemeVersion;
    }

    public void setThemeVersion(Integer themeVersion) {
        ThemeVersion = themeVersion;
    }
}