package com.cygent.model.entities.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hsnirmal on 6/2/2016.
 */
public class FileDownloadResponse {

    @SerializedName("ImageByteString")
    @Expose
    String ImageByteString;

    public String getImageByteString() {
        return ImageByteString;
    }

    public void setImageByteString(String imageByteString) {
        ImageByteString = imageByteString;
    }
}
