package com.cygent.model.entities.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hsnirmal on 6/24/2016.
 */
public class NotificationResponse {

    @SerializedName("ResponseJSON")
    @Expose
    private ThemeCustomizeSetting responseJSON;

    /**
     *
     * @return
     * The responseJSON
     */
    public ThemeCustomizeSetting getResponseJSON() {
        return responseJSON;
    }

    /**
     *
     * @param responseJSON
     * The ResponseJSON
     */
    public void setResponseJSON(ThemeCustomizeSetting responseJSON) {
        this.responseJSON = responseJSON;
    }
}
