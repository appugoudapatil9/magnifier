package com.cygent.model.entities.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateResourceViewCount {

@SerializedName("LastAccessDate")
@Expose
private String lastAccessDate;
@SerializedName("NumberOfViews")
@Expose
private String numberOfViews;
@SerializedName("ResourceStatus")
@Expose
private String resourceStatus;
@SerializedName("TimeSpent")
@Expose
private String timeSpent;
@SerializedName("AppUserResourceID")
@Expose
private String userResourceID;

/**
*
* @return
* The lastAccessDate
*/
public String getLastAccessDate() {
return lastAccessDate;
}

/**
*
* @param lastAccessDate
* The LastAccessDate
*/
public void setLastAccessDate(String lastAccessDate) {
this.lastAccessDate = lastAccessDate;
}

/**
*
* @return
* The numberOfViews
*/
public String getNumberOfViews() {
return numberOfViews;
}

/**
*
* @param numberOfViews
* The NumberOfViews
*/
public void setNumberOfViews(String numberOfViews) {
this.numberOfViews = numberOfViews;
}

/**
*
* @return
* The resourceStatus
*/
public String getResourceStatus() {
return resourceStatus;
}

/**
*
* @param resourceStatus
* The ResourceStatus
*/
public void setResourceStatus(String resourceStatus) {
this.resourceStatus = resourceStatus;
}

/**
*
* @return
* The timeSpent
*/
public String getTimeSpent() {
return timeSpent;
}

/**
*
* @param timeSpent
* The TimeSpent
*/
public void setTimeSpent(String timeSpent) {
this.timeSpent = timeSpent;
}

/**
*
* @return
* The userResourceID
*/
public String getUserResourceID() {
return userResourceID;
}

/**
*
* @param userResourceID
* The UserResourceID
*/
public void setUserResourceID(String userResourceID) {
this.userResourceID = userResourceID;
}

}
