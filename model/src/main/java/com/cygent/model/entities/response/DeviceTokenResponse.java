package com.cygent.model.entities.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeviceTokenResponse {

    @SerializedName("DeviceTokenResponse")
    @Expose
    private Disclaimer deviceTokenResponse;
    @SerializedName("IpadCustomizeSetting")
    @Expose
    private ThemeCustomizeSetting ipadCustomizeSetting;
    @SerializedName("IsThemeUpdateRequire")
    @Expose
    private Boolean isThemeUpdateRequire;

    /**
     *
     * @return
     * The deviceTokenResponse
     */
    public Disclaimer getDeviceTokenResponse() {
        return deviceTokenResponse;
    }

    /**
     *
     * @param deviceTokenResponse
     * The DeviceTokenResponse
     */
    public void setDeviceTokenResponse(Disclaimer deviceTokenResponse) {
        this.deviceTokenResponse = deviceTokenResponse;
    }

    /**
     *
     * @return
     * The ipadCustomizeSetting
     */
    public ThemeCustomizeSetting getIpadCustomizeSetting() {
        return ipadCustomizeSetting;
    }

    /**
     *
     * @param ipadCustomizeSetting
     * The IpadCustomizeSetting
     */
    public void setIpadCustomizeSetting(ThemeCustomizeSetting ipadCustomizeSetting) {
        this.ipadCustomizeSetting = ipadCustomizeSetting;
    }

    /**
     *
     * @return
     * The isThemeUpdateRequire
     */
    public Boolean getIsThemeUpdateRequire() {
        return isThemeUpdateRequire;
    }

    /**
     *
     * @param isThemeUpdateRequire
     * The IsThemeUpdateRequire
     */
    public void setIsThemeUpdateRequire(Boolean isThemeUpdateRequire) {
        this.isThemeUpdateRequire = isThemeUpdateRequire;
    }

}