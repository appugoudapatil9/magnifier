package com.cygent.model.entities.response;

/**
 * Created by hsnirmal on 5/30/2016.
 */
public class ErrorMessage {

    String errorMessage;

    public static final String DOWNLOAD_ERROR = "File is moved or deleted.";
    public static final String DOWNLOAD_CANCELED = "Download cancelled.";
    public static final String DOWNLOAD_FAILED = "Download failed.";
    public static final String DOWNLOAD_MEMORY_ISSUE = "Your device does not have enough space to download this file, Please free some space and try again later.";
    public static final String RESOURCE_DELETE_ERROR = "Error occurred in deleting record";
    public static final String DEVICE_TOKEN_REGISTER_ERROR = "Error in register device token.";
    public static final String LOGIN_ERROR = "Please enter valid pin number.";
    public static final String LOGIN_UNKNOWN_ERROR = "Unknown error occurred.";
    public static final String THEME_ERROR = "Error getting theme.";
    public static final String USER_DEACTIVATED = "User has been deactivated.";
    public static final String USER_EXPIRED = "User has been expired.";
    public static final String DELETE_RESOURCE_ERROR = "Error occurred in deleting resource please try again later.";

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
