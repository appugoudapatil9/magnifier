package com.cygent.model.entities.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hsnirmal on 6/24/2016.
 */
public class UpdateViewCountRequest{
        @SerializedName("UserID")
        @Expose
        private Long userID;

        @SerializedName("UpdateResourceViewCount")
        @Expose
        private List<UpdateResourceViewCount> updateResourceViewCount = new ArrayList<UpdateResourceViewCount>();

        /**
         *
         * @return
         * The userID
         */
        public Long getUserID() {
                return userID;
        }

        /**
         *
         * @param userID
         * The UserID
         */
        public void setUserID(Long userID) {
                this.userID = userID;
        }

        /**
         *
         * @return
         * The updateResourceViewCount
         */
        public List<UpdateResourceViewCount> getUpdateResourceViewCount() {
            return updateResourceViewCount;
        }

        /**
         *
         * @param updateResourceViewCount
         * The UpdateResourceViewCount
         */
        public void setUpdateResourceViewCount(List<UpdateResourceViewCount> updateResourceViewCount) {
            this.updateResourceViewCount = updateResourceViewCount;
        }
}
