package com.cygent.model.entities.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hsnirmal on 6/2/2016.
 */
public class ThemeCustomizeSetting {

    @SerializedName("LandscapeImage")
    @Expose
    private String backgroundImage;
    @SerializedName("PortraitImage")
    @Expose
    private String PortraitImage;
    @SerializedName("BUID")
    @Expose
    private Integer clientID;
    @SerializedName("FontColor")
    @Expose
    private String fontColor;
    @SerializedName("HeaderColor")
    @Expose
    private String headerColor;
    @SerializedName("LogoImage")
    @Expose
    private String logoImage;
    @SerializedName("VersionNumber")
    @Expose
    private Integer versionNumber;
    @SerializedName("NotificationType")
    @Expose
    private Integer notificationType;
    @SerializedName("NotificationMessage")
    @Expose
    private String notificationMessage;
    @SerializedName("ConfirmationMessage")
    @Expose
    private String confirmationMessage;

    @SerializedName("ConfirmationMessageModifiedDate")
    @Expose
    private String confirmationMessageModifiedDate;

    public String getConfirmationMessage() {
        return confirmationMessage;
    }

    public void setConfirmationMessage(String confirmationMessage) {
        this.confirmationMessage = confirmationMessage;
    }

    public String getConfirmationMessageModifiedDate() {
        return confirmationMessageModifiedDate;
    }

    public void setConfirmationMessageModifiedDate(String confirmationMessageModifiedDate) {
        this.confirmationMessageModifiedDate = confirmationMessageModifiedDate;
    }

    public String getNotificationMessage() {
        return notificationMessage;
    }

    public void setNotificationMessage(String notificationMessage) {
        this.notificationMessage = notificationMessage;
    }

    public Integer getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(Integer notificationType) {
        this.notificationType = notificationType;
    }

    /**
     *
     * @return
     * The backgroundImage
     */
    public String getBackgroundImage() {
        return backgroundImage;
    }

    /**
     *
     * @param backgroundImage
     * The BackgroundImage
     */
    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }


    public String getPortraitImage() {
        return PortraitImage;
    }

    public void setPortraitImage(String portraitImage) {
        PortraitImage = portraitImage;
    }

    /**
     *
     * @return
     * The clientID
     */
    public Integer getClientID() {
        return clientID;
    }

    /**
     *
     * @param clientID
     * The ClientID
     */
    public void setClientID(Integer clientID) {
        this.clientID = clientID;
    }

    /**
     *
     * @return
     * The fontColor
     */
    public String getFontColor() {
        return fontColor;
    }

    /**
     *
     * @param fontColor
     * The FontColor
     */
    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }

    /**
     *
     * @return
     * The headerColor
     */
    public String getHeaderColor() {
        return headerColor;
    }

    /**
     *
     * @param headerColor
     * The HeaderColor
     */
    public void setHeaderColor(String headerColor) {
        this.headerColor = headerColor;
    }

    /**
     *
     * @return
     * The logoImage
     */
    public String getLogoImage() {
        return logoImage;
    }

    /**
     *
     * @param logoImage
     * The LogoImage
     */
    public void setLogoImage(String logoImage) {
        this.logoImage = logoImage;
    }

    /**
     *
     * @return
     * The versionNumber
     */
    public Integer getVersionNumber() {
        return versionNumber;
    }

    /**
     *
     * @param versionNumber
     * The VersionNumber
     */
    public void setVersionNumber(Integer versionNumber) {
        this.versionNumber = versionNumber;
    }

    @Override
    public String toString() {
        return "ThemeCustomizeSetting{" +
                "backgroundImage='" + backgroundImage + '\'' +
                ", clientID=" + clientID +
                ", fontColor='" + fontColor + '\'' +
                ", headerColor='" + headerColor + '\'' +
                ", logoImage='" + logoImage + '\'' +
                ", versionNumber=" + versionNumber +
                ", notificationType=" + notificationType +
                ", notificationMessage='" + notificationMessage + '\'' +
                '}';
    }
}

