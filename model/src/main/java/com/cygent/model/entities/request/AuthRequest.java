package com.cygent.model.entities.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by appupatil on 15/10/18.
 */

public class AuthRequest {

    @SerializedName("UserID")
    @Expose
    private long UserID;
    @SerializedName("BUId")
    @Expose
    private long ClientID;

    /**
     * @return The UserID
     */
    public long getUserID() {
        return UserID;
    }

    /**
     * @param UserID The UserID
     */
    public void setUserID(long UserID) {
        this.UserID = UserID;
    }



    /**
     * @return The ClientID
     */
    public long getClientID() {
        return ClientID;
    }

    /**
     * @param ClientID The ClientID
     */
    public void setClientID(long ClientID) {
        this.ClientID = ClientID;
    }
}
