package com.cygent.model.entities.response;

/**
 * Created by hsnirmal on 5/25/2016.
 */

import java.util.ArrayList;
        import java.util.List;
        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

public class GetResourceResponse {

    @SerializedName("ResourceList")
    @Expose
    private List<Resource> resourceList = new ArrayList<Resource>();
    @SerializedName("FolderList")
    @Expose
    private List<Folder> folderList = new ArrayList<Folder>();

    /**
     *
     * @return
     * The resourceList
     */
    public List<Resource> getResourceList() {
        return resourceList;
    }

    /**
     *
     * @param resourceList
     * The ResourceList
     */
    public void setResourceList(List<Resource> resourceList) {
        this.resourceList = resourceList;
    }

    /**
     *
     * @return
     * The folderList
     */
    public List<Folder> getFolderList() {
        return folderList;
    }

    /**
     *
     * @param folderList
     * The FolderList
     */
    public void setFolderList(List<Folder> folderList) {
        this.folderList = folderList;
    }

}

