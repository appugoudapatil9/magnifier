package com.cygent.model.entities.database;

import android.nfc.Tag;
import android.os.Parcel;
import android.os.Parcelable;

import com.cygent.framework.utils.AppLog;
import com.cygent.model.utils.Constants;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by hsnirmal on 5/30/2016.
 */
@DatabaseTable(tableName = Constants.Database.TABLE_RESOURCE)
public class ResourceEntity implements  Parcelable {

    public static final String  TAG = ResourceEntity.class.getSimpleName();

    @DatabaseField(columnName = Constants.Database.COLUMN_ID, generatedId = true)
    Long Id;

    @DatabaseField(columnName = Constants.Database.COLUMN_RESOURCE_PARENT_FOLDER_ID)
    Long parentFolderId;

    @DatabaseField(columnName = Constants.Database.COLUMN_FOLDER_ID)
    Long folderId;

    @DatabaseField(columnName = Constants.Database.COLUMN_RESOURCE_ID)
    Long resourceId;

    @DatabaseField(columnName = Constants.Database.COLUMN_RESOURCE_ISSendViaMail)
    Boolean isSendViaMail;

    @DatabaseField(columnName = Constants.Database.COLUMN_RESOURCE_NumberOfViews)
    Integer numberOfViews;

    @DatabaseField(columnName = Constants.Database.COLUMN_RESOURCE_NAME)
    String name;

    @DatabaseField(columnName = Constants.Database.COLUMN_RESOURCE_LOCAL_PATH)
    String localPath;

    @DatabaseField(columnName = Constants.Database.COLUMN_RESOURCE_SERVER_PATH)
    String serverPath;

    @DatabaseField(columnName = Constants.Database.COLUMN_RESOURCE_SEQ_NUMBER)
    Integer seqNumber;

    @DatabaseField(columnName = Constants.Database.COLUMN_RESOURCE_RES_COUNT)
    Integer resourceCount;

    @DatabaseField(columnName = Constants.Database.COLUMN_RESOURCE_RES_IMGPATH)
    String imagePath;

    @DatabaseField(columnName = Constants.Database.COLUMN_IS_RESOURCE)
    Boolean isResource;

    @DatabaseField(columnName = Constants.Database.COLUMN_RESOURCE_TYPE)
    String resourceType;

    @DatabaseField(columnName = Constants.Database.COLUMN_SYNC_STATUS)
    Integer syncStatus;

    @DatabaseField(columnName = Constants.Database.COLUMN_USER_STATUS)
    Integer userStatus;

    @DatabaseField(columnName = Constants.Database.COLUMN_RESOURCE_SIZE)
    String resourceSize;

    @DatabaseField(columnName = Constants.Database.COLUMN_RESOURCE_DOWNLOAD_DATE_TIME)
    String resourceDownloadDateTime;

    @DatabaseField(columnName = Constants.Database.COLUMN_RESOURCE_IS_DOWNLOADED)
    Boolean isDownloaded;

    @DatabaseField(columnName = Constants.Database.COLUMN_RESOURCE_HIERARCHY)
    String resourceHierarchy;

    @DatabaseField(columnName = Constants.Database.COLUMN_RESOURCE_SIZE_IN_KB)
    Long resourceSizeInKB;

    @DatabaseField(columnName = Constants.Database.COLUMN_TIME_SPENT)
    Long timeSpent;

    @DatabaseField(columnName = Constants.Database.COLUMN_LAST_ACCESS_DATE)
    String lastAccessDate;

    @DatabaseField(columnName = Constants.Database.COLUMN_LAUNCHER_FILE)
    String launcherFile;

    private boolean isSelected;

    private Integer listPosition;

    private boolean isEditable;

    List<ResourceEntity> resourceEntityList;

    public boolean isEditable() {
        return isEditable;
    }

    public void setEditable(boolean editable) {
        isEditable = editable;
    }

    public Integer getListPosition() {
        return listPosition;
    }

    public void setListPosition(Integer listPosition) {
        this.listPosition = listPosition;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public Long getTimeSpent() {
        return timeSpent;
    }

    public void setTimeSpent(Long timeSpent) {
        this.timeSpent = timeSpent;
    }

    public String getLastAccessDate() {
        return lastAccessDate;
    }

    public void setLastAccessDate(String lastAccessDate) {
        this.lastAccessDate = lastAccessDate;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Boolean getSendViaMail() {
        return isSendViaMail;
    }

    public void setSendViaMail(Boolean sendViaMail) {
        isSendViaMail = sendViaMail;
    }

    public Integer getNumberOfViews() {
        return numberOfViews;
    }

    public void setNumberOfViews(Integer numberOfViews) {
        this.numberOfViews = numberOfViews;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getServerPath() {
        return serverPath;
    }

    public void setServerPath(String serverPath) {
        this.serverPath = serverPath;
    }

    public Integer getSeqNumber() {
        return seqNumber;
    }

    public void setSeqNumber(Integer seqNumber) {
        this.seqNumber = seqNumber;
    }

    public Integer getResourceCount() {
        return resourceCount;
    }

    public void setResourceCount(Integer resourceCount) {
        this.resourceCount = resourceCount;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Long getParentFolderId() {
        return parentFolderId;
    }

    public void setParentFolderId(Long parentFolderId) {
        this.parentFolderId = parentFolderId;
    }

    public Long getFolderId() {
        return folderId;
    }

    public void setFolderId(Long folderId) {
        this.folderId = folderId;
    }

    public Long getResourceId() {
        return resourceId;
    }

    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }

    public String getLocalPath() {
        return localPath;
    }

    public void setLocalPath(String localPath) {
        this.localPath = localPath;
    }

    public Boolean getIsResource() {
        return isResource;
    }

    public void setIsResource(Boolean resource) {
        isResource = resource;
    }

    public Boolean getResource() {
        return isResource;
    }

    public void setResource(Boolean resource) {
        isResource = resource;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public Integer getSyncStatus() {
        return syncStatus;
    }

    public void setSyncStatus(Integer syncStatus) {
        this.syncStatus = syncStatus;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public List<ResourceEntity> getResourceEntityList() {
        return resourceEntityList;
    }

    public void setResourceEntityList(List<ResourceEntity> resourceEntityList) {
        this.resourceEntityList = resourceEntityList;
    }

    public String getResourceSize() {
        return resourceSize;
    }

    public void setResourceSize(String resourceSize) {
        this.resourceSize = resourceSize;
    }

    public String getResourceDownloadDateTime() {
        return resourceDownloadDateTime;
    }

    public void setResourceDownloadDateTime(String resourceDownloadDateTime) {
        this.resourceDownloadDateTime = resourceDownloadDateTime;
    }

    public Long getResourceSizeInKB() {
        return resourceSizeInKB;
    }

    public void setResourceSizeInKB(Long resourceSizeInKB) {
        this.resourceSizeInKB = resourceSizeInKB;
    }

    //    public Boolean getIsDownloaded() {
//        return isDownloaded;
//    }
//
//    public void setIsDownloaded(Boolean downloaded) {
//        isDownloaded = downloaded;
//    }

    public Boolean getDownloaded() {
        return isDownloaded;
    }

    public void setDownloaded(Boolean downloaded) {
        isDownloaded = downloaded;
    }

    public String getResourceHierarchy() {
        return resourceHierarchy;
    }

    public void setResourceHierarchy(String resourceHierarchy) {
        this.resourceHierarchy = resourceHierarchy;
    }

    public String getLauncherFile() { return  launcherFile; }

    public void setLauncherFile(String launcherFile) {
        this.launcherFile = launcherFile;
    }

    @Override
    public boolean equals(Object o) {
//        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ResourceEntity that = (ResourceEntity) o;

        return Id.equals(that.Id);

    }

    @Override
    public int hashCode() {
        AppLog.d(TAG,"Check Point");
        AppLog.d(TAG,"Current Obj : "+this);
        return Id.hashCode();
    }

    public static class SequenceComparator implements Comparator<ResourceEntity>{

        @Override
        public int compare(ResourceEntity lhs, ResourceEntity rhs) {
            return lhs.seqNumber.compareTo(rhs.seqNumber);
        }
    }

    public static List<ResourceEntity> getFragmentedList(ResourceEntity arg_ResourceEntity, final int L) {
        List<ResourceEntity> fragList = new ArrayList<ResourceEntity>();
        final int N = arg_ResourceEntity.getResourceEntityList().size();
        for (int i = 0; i < N; i += L) {

            ResourceEntity mResourceEntity = new ResourceEntity();
            mResourceEntity.setResourceEntityList(arg_ResourceEntity.getResourceEntityList().subList(i, Math.min(N, i + L)));
            fragList.add(mResourceEntity);
        }
        return fragList;
    }

    public static List<ResourceEntity> getFragmentedList(List<ResourceEntity> arg_resourceList, final int L) {
        List<ResourceEntity> fragList = new ArrayList<ResourceEntity>();
        final int N = arg_resourceList.size();
        for (int i = 0; i < N; i += L) {

            ResourceEntity mResourceEntity = new ResourceEntity();
            mResourceEntity.setResourceEntityList(arg_resourceList.subList(i, Math.min(N, i + L)));
            fragList.add(mResourceEntity);
        }
        return fragList;
    }





    @Override
    public String toString() {
        return "ResourceEntity{" +
                "Id=" + Id +
                ", parentFolderId=" + parentFolderId +
                ", folderId=" + folderId +
                ", resourceId=" + resourceId +
                ", isSendViaMail=" + isSendViaMail +
                ", numberOfViews=" + numberOfViews +
                ", name='" + name + '\'' +
                ", localPath='" + localPath + '\'' +
                ", serverPath='" + serverPath + '\'' +
                ", seqNumber=" + seqNumber +
                ", resourceCount=" + resourceCount +
                ", imagePath='" + imagePath + '\'' +
                ", isResource=" + isResource +
                ", resourceType='" + resourceType + '\'' +
                ", syncStatus=" + syncStatus +
                ", userStatus=" + userStatus +
                ", resourceSize='" + resourceSize + '\'' +
                ", resourceDownloadDateTime='" + resourceDownloadDateTime + '\'' +
                ", isDownloaded=" + isDownloaded +
                ", resourceHierarchy='" + resourceHierarchy + '\'' +
                ", resourceSizeInKB=" + resourceSizeInKB +
                ", resourceEntityList=" + resourceEntityList + '\'' +
                ", launcherFile=" + launcherFile +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.Id);
        dest.writeValue(this.parentFolderId);
        dest.writeValue(this.folderId);
        dest.writeValue(this.resourceId);
        dest.writeValue(this.isSendViaMail);
        dest.writeValue(this.numberOfViews);
        dest.writeString(this.name);
        dest.writeString(this.localPath);
        dest.writeString(this.serverPath);
        dest.writeValue(this.seqNumber);
        dest.writeValue(this.resourceCount);
        dest.writeString(this.imagePath);
        dest.writeValue(this.isResource);
        dest.writeString(this.resourceType);
        dest.writeValue(this.syncStatus);
        dest.writeValue(this.userStatus);
        dest.writeString(this.resourceSize);
        dest.writeString(this.resourceDownloadDateTime);
        dest.writeValue(this.isDownloaded);
        dest.writeString(this.resourceHierarchy);
        dest.writeValue(this.resourceSizeInKB);
        dest.writeValue(this.timeSpent);
        dest.writeString(this.lastAccessDate);
        dest.writeList(this.resourceEntityList);
        dest.writeString(this.launcherFile);
    }

    public ResourceEntity() {
    }

    protected ResourceEntity(Parcel in) {
        this.Id = (Long) in.readValue(Long.class.getClassLoader());
        this.parentFolderId = (Long) in.readValue(Long.class.getClassLoader());
        this.folderId = (Long) in.readValue(Long.class.getClassLoader());
        this.resourceId = (Long) in.readValue(Long.class.getClassLoader());
        this.isSendViaMail = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.numberOfViews = (Integer) in.readValue(Integer.class.getClassLoader());
        this.name = in.readString();
        this.localPath = in.readString();
        this.serverPath = in.readString();
        this.seqNumber = (Integer) in.readValue(Integer.class.getClassLoader());
        this.resourceCount = (Integer) in.readValue(Integer.class.getClassLoader());
        this.imagePath = in.readString();
        this.isResource = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.resourceType = in.readString();
        this.syncStatus = (Integer) in.readValue(Integer.class.getClassLoader());
        this.userStatus = (Integer) in.readValue(Integer.class.getClassLoader());
        this.resourceSize = in.readString();
        this.resourceDownloadDateTime = in.readString();
        this.isDownloaded = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.resourceHierarchy = in.readString();
        this.resourceSizeInKB = (Long) in.readValue(Long.class.getClassLoader());
        this.timeSpent = (Long) in.readValue(Long.class.getClassLoader());
        this.lastAccessDate = in.readString();
        this.resourceEntityList = new ArrayList<ResourceEntity>();
        this.launcherFile = in.readString();
        in.readList(this.resourceEntityList, ResourceEntity.class.getClassLoader());
    }

    public static final Parcelable.Creator<ResourceEntity> CREATOR = new Parcelable.Creator<ResourceEntity>() {
        @Override
        public ResourceEntity createFromParcel(Parcel source) {
            return new ResourceEntity(source);
        }

        @Override
        public ResourceEntity[] newArray(int size) {
            return new ResourceEntity[size];
        }
    };
}

