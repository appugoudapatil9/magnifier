package com.cygent.model.entities.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hsnirmal on 6/8/2016.
 */
public class AcknowledgementResponse {

    @SerializedName("Disclaimer")
    @Expose
    private String libraryDisclaimer;
    @SerializedName("Result")
    @Expose
    private String result;
//    @SerializedName("hasRightsforHideDateandSize")
//    @Expose
//    private Boolean hasRightsforHideDateandSize;
//    @SerializedName("hasRightsforLibraryDisclaimer")
//    @Expose
//    private Boolean hasRightsforLibraryDisclaimer;

    /**
     *
     * @return
     * The libraryDisclaimer
     */
    public String getLibraryDisclaimer() {
        return libraryDisclaimer;
    }

    /**
     *
     * @param libraryDisclaimer
     * The LibraryDisclaimer
     */
    public void setLibraryDisclaimer(String libraryDisclaimer) {
        this.libraryDisclaimer = libraryDisclaimer;
    }

    /**
     *
     * @return
     * The result
     */
    public String getResult() {
        return result;
    }

    /**
     *
     * @param result
     * The Result
     */
    public void setResult(String result) {
        this.result = result;
    }

    /**
     *
     * @return
     * The hasRightsforHideDateandSize
     */
//    public Boolean getHasRightsforHideDateandSize() {
//        return hasRightsforHideDateandSize;
//    }

    /**
     *
     * @param hasRightsforHideDateandSize
     * The hasRightsforHideDateandSize
     */
//    public void setHasRightsforHideDateandSize(Boolean hasRightsforHideDateandSize) {
//        this.hasRightsforHideDateandSize = hasRightsforHideDateandSize;
//    }

    /**
     *
     * @return
     * The hasRightsforLibraryDisclaimer
     */
//    public Boolean getHasRightsforLibraryDisclaimer() {
//        return hasRightsforLibraryDisclaimer;
//    }

    /**
     *
     * @param hasRightsforLibraryDisclaimer
     * The hasRightsforLibraryDisclaimer
     */
//    public void setHasRightsforLibraryDisclaimer(Boolean hasRightsforLibraryDisclaimer) {
//        this.hasRightsforLibraryDisclaimer = hasRightsforLibraryDisclaimer;
//    }

}
