package com.cygent.model.entities.request;

/**
 * Created by hsnirmal on 12/5/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by hsnirmal on 12/5/2016.
 */

public class GetThemeRequest {


    public GetThemeRequest() {}

    public GetThemeRequest(Long a_bId) {
        this.bId = a_bId;
    }

    @SerializedName("BUId")
    @Expose
    private Long bId;

    /**
     *
     * @return
     * The bId
     */
    public Long getIPadPin() {
        return this.bId;
    }

    /**
     *
     * @param a_bId
     * The bId
     */
    public void setIPadPin(Long a_bId) {
        this.bId = a_bId;
    }

}