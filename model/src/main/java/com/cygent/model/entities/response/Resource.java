package com.cygent.model.entities.response;

/**
 * Created by hsnirmal on 5/25/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Resource {

    @SerializedName("AppFolderID")
    @Expose
    private Long resourceFolderID;
    @SerializedName("ResourceSequence")
    @Expose
    private Integer resourceSequence;
    @SerializedName("IsSendViaMail")
    @Expose
    private Boolean isSendViaMail;
    @SerializedName("ResourceName")
    @Expose
    private String resourceName;
    @SerializedName("ResourcePath")
    @Expose
    private String resourcePath;
    @SerializedName("ResourceType")
    @Expose
    private String resourceType;
    @SerializedName("SyncStatus")
    @Expose
    private Integer syncStatus;
    @SerializedName("AppUserResourceID")
    @Expose
    private Long userResourceID;
    @SerializedName("UserStatus")
    @Expose
    private Integer userStatus;
    @SerializedName("ResourceImage")
    @Expose
    private String resourceImage;
    @SerializedName("ResourceSizeInKB")
    @Expose
    private String resourceSizeInKB;

    @SerializedName("ResourceFolderHierarchy")
    @Expose
    private String resourceFolderHierarchy;

    @SerializedName("LauncherFile")
    @Expose
    private String launcherFile;

    /**
     *
     * @return
     * The resourceFolderID
     */
    public Long getResourceFolderID() {
        return resourceFolderID;
    }

    /**
     *
     * @param resourceFolderID
     * The ResourceFolderID
     */
    public void setResourceFolderID(Long resourceFolderID) {
        this.resourceFolderID = resourceFolderID;
    }

    /**
     *
     * @return
     * The resourceSequence
     */
    public Integer getResourceSequence() {
        return resourceSequence;
    }

    /**
     *
     * @param resourceSequence
     * The ResourceSequence
     */
    public void setResourceSequence(Integer resourceSequence) {
        this.resourceSequence = resourceSequence;
    }

    /**
     *
     * @return
     * The isSendViaMail
     */
    public Boolean getIsSendViaMail() {
        return isSendViaMail;
    }

    /**
     *
     * @param isSendViaMail
     * The IsSendViaMail
     */
    public void setIsSendViaMail(Boolean isSendViaMail) {
        this.isSendViaMail = isSendViaMail;
    }

    /**
     *
     * @return
     * The resourceName
     */
    public String getResourceName() {
        return resourceName;
    }

    /**
     *
     * @param resourceName
     * The ResourceName
     */
    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    /**
     *
     * @return
     * The resourcePath
     */
    public String getResourcePath() {
        return resourcePath;
    }

    /**
     *
     * @param resourcePath
     * The ResourcePath
     */
    public void setResourcePath(String resourcePath) {
        this.resourcePath = resourcePath;
    }

    /**
     *
     * @return
     * The resourceType
     */
    public String getResourceType() {
        return resourceType;
    }

    /**
     *
     * @param resourceType
     * The ResourceType
     */
    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    /**
     *
     * @return
     * The syncStatus
     */
    public Integer getSyncStatus() {
        return syncStatus;
    }

    /**
     *
     * @param syncStatus
     * The SyncStatus
     */
    public void setSyncStatus(Integer syncStatus) {
        this.syncStatus = syncStatus;
    }

    /**
     *
     * @return
     * The userResourceID
     */
    public Long getUserResourceID() {
        return userResourceID;
    }

    /**
     *
     * @param userResourceID
     * The UserResourceID
     */
    public void setUserResourceID(Long userResourceID) {
        this.userResourceID = userResourceID;
    }

    /**
     *
     * @return
     * The userStatus
     */
    public Integer getUserStatus() {
        return userStatus;
    }

    /**
     *
     * @param userStatus
     * The UserStatus
     */
    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }


    public Boolean getSendViaMail() {
        return isSendViaMail;
    }

    public void setSendViaMail(Boolean sendViaMail) {
        isSendViaMail = sendViaMail;
    }

    public String getResourceImage() {
        return resourceImage;
    }

    public void setResourceImage(String resourceImage) {
        this.resourceImage = resourceImage;
    }

    public String getResourceSizeInKB() {
        return resourceSizeInKB;
    }

    public void setResourceSizeInKB(String resourceSizeInKB) {
        this.resourceSizeInKB = resourceSizeInKB;
    }

    public String getResourceFolderHierarchy() {
        return resourceFolderHierarchy;
    }

    public void setResourceFolderHierarchy(String resourceFolderHierarchy) {
        this.resourceFolderHierarchy = resourceFolderHierarchy;
    }

    public String getLauncherFile() {
        return launcherFile;
    }

    public void setLauncherFile(String launcherFile) {
        this.launcherFile = launcherFile;
    }

}