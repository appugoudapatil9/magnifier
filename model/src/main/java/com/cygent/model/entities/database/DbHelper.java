package com.cygent.model.entities.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.cygent.framework.utils.AppLog;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by hsnirmal on 5/27/2016.
 */
public class DbHelper extends OrmLiteSqliteOpenHelper {

    public static final String TAG = DbHelper.class.getSimpleName();
    public static final String DB_NAME = "HGS01.db";
    public static final int DB_VERSION = 1;

    private Dao<ResourceEntity, Integer> resourceEntityDao = null;

    public DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {

        try {
            TableUtils.createTable(connectionSource,ResourceEntity.class);
        } catch (SQLException e) {
            e.printStackTrace();
            AppLog.d(TAG,"Tables not created");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

    }


    public Dao<ResourceEntity, Integer> getResourceEntityDao()
    {
        if (resourceEntityDao == null) {
            try {
                resourceEntityDao = getDao(ResourceEntity.class);
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return resourceEntityDao;
    }

//    public Dao
}
