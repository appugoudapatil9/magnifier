package com.cygent.model.entities.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by appupatil on 22/04/19.
 */

public class AppVersionRequest {

    @SerializedName("UserID")
    @Expose
    private long UserID;
    @SerializedName("AndroidVersion")
    @Expose
    private String AndroidVersion;

    /**
     * @return The UserID
     */
    public long getUserID() {
        return UserID;
    }

    /**
     * @param UserID The UserID
     */
    public void setUserID(long UserID) {
        this.UserID = UserID;
    }



    /**
     * @return The androidVersion
     */
    public String getAndroidVersion() {
        return AndroidVersion;
    }

    /**
     * @param androidVersion The androidVersion
     */
    public void setAndroidVersion(String androidVersion) {
        this.AndroidVersion = androidVersion;
    }
}

