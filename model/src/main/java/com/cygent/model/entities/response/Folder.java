package com.cygent.model.entities.response;

/**
 * Created by hsnirmal on 5/25/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Folder {

    @SerializedName("AppFolderID")
    @Expose
    private Long folderID;
    @SerializedName("FolderName")
    @Expose
    private String folderName;
    @SerializedName("ParentFolderID")
    @Expose
    private Long parentFolderID;
    @SerializedName("SequenceID")
    @Expose
    private Integer sequence;
    @SerializedName("ResourceFolderImage")
    @Expose
    private String resourceFolderImage;
    @SerializedName("ResourcesCount")
    @Expose
    private Integer resourcesCount;

    /**
     *
     * @return
     * The folderID
     */
    public Long getFolderID() {
        return folderID;
    }

    /**
     *
     * @param folderID
     * The FolderID
     */
    public void setFolderID(Long folderID) {
        this.folderID = folderID;
    }

    /**
     *
     * @return
     * The folderName
     */
    public String getFolderName() {
        return folderName;
    }

    /**
     *
     * @param folderName
     * The FolderName
     */
    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    /**
     *
     * @return
     * The parentFolderID
     */
    public Long getParentFolderID() {
        return parentFolderID;
    }

    /**
     *
     * @param parentFolderID
     * The ParentFolderID
     */
    public void setParentFolderID(Long parentFolderID) {
        this.parentFolderID = parentFolderID;
    }

    /**
     *
     * @return
     * The sequence
     */
    public Integer getSequence() {
        return sequence;
    }

    /**
     *
     * @param sequence
     * The Sequence
     */
    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    /**
     *
     * @return
     * The resourceFolderImage
     */
    public String getResourceFolderImage() {
        return resourceFolderImage;
    }

    /**
     *
     * @param resourceFolderImage
     * The ResourceFolderImage
     */
    public void setResourceFolderImage(String resourceFolderImage) {
        this.resourceFolderImage = resourceFolderImage;
    }

    /**
     *
     * @return
     * The resourcesCount
     */
    public Integer getResourcesCount() {
        return resourcesCount;
    }

    /**
     *
     * @param resourcesCount
     * The ResourcesCount
     */
    public void setResourcesCount(Integer resourcesCount) {
        this.resourcesCount = resourcesCount;
    }

}