package com.cygent.model.entities.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by hrdudhat on 3/2/2016.
 */
public class Login {
    @Expose
    @SerializedName("user_id")
    private String userId;
}
