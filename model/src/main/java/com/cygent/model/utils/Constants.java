package com.cygent.model.utils;

import com.cygent.model.entities.response.ErrorMessage;

/**
 * Created by hrdudhat on 3/2/2016.
 * <p/>
 * Contains all constants use in Model Module.
 */
public class Constants {
    public static class ApiHeader {
    }

    public static class ApiCodes {
    }

    public static class ApiMethods {
        public static final String VERSION_CHECK = "https://www.test-lw.com/iep/ClientMaterials/admin/AndroidApp/android_version.json";
    }

    public static class Database{

        public static final String TABLE_RESOURCE = "Resource";

        public static final String COLUMN_RESOURCE_NAME = "Name";

        public static final String COLUMN_ID  = "Id";

        public static final String COLUMN_FOLDER_ID = "FolderId";

        public static final String COLUMN_RESOURCE_ID = "ResourceId";

        public static final String COLUMN_RESOURCE_PARENT_FOLDER_ID = "ParentFolderId";

        public static final String COLUMN_RESOURCE_ISSendViaMail = "IsSendViaMail";

        public static final String COLUMN_RESOURCE_NumberOfViews = "NumberOfView";

        public static final String COLUMN_RESOURCE_SERVER_PATH = "ServerPath";

        public static final String COLUMN_RESOURCE_LOCAL_PATH = "LocalPath";

        public static final String COLUMN_RESOURCE_SEQ_NUMBER = "SeqNumber";

        public static final String COLUMN_RESOURCE_RES_COUNT = "ResCount";

        public static final String COLUMN_RESOURCE_RES_IMGPATH = "ImgPath";

        public static final String COLUMN_IS_RESOURCE = "IsResource";

        public static final String COLUMN_RESOURCE_TYPE = "ResourceType";

        public static final String COLUMN_SYNC_STATUS = "SyncStatus";

        public static final String COLUMN_USER_STATUS = "UserStatus";

        public static final String COLUMN_RESOURCE_SIZE = "ResourceSize";

        public static final String COLUMN_RESOURCE_DOWNLOAD_DATE_TIME = "ResourceDownloadDateTime";

        public static final String COLUMN_RESOURCE_IS_DOWNLOADED = "IsResourceDownloaded";

        public static final String COLUMN_RESOURCE_HIERARCHY = "ResourceHierarchy";

        public static final String COLUMN_RESOURCE_SIZE_IN_KB = "ResourceSizeInKB";

        public static final String COLUMN_TIME_SPENT = "TimeSpent";

        public static final String COLUMN_LAST_ACCESS_DATE = "LastAccessDate";

        public static final String COLUMN_LAUNCHER_FILE = "LauncherFile";
    }

}
