package com.cygent.model.rest;

import android.content.Intent;
import android.os.AsyncTask;

/**
 * Created by hsnirmal on 7/11/2016.
 */
public abstract class MyDownLoadTask extends AsyncTask<Void,Integer,Void>{
    public abstract void doProgress(int progress);
}
