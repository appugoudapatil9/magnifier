package com.cygent.model.rest;

import com.cygent.model.entities.request.AcknowledgementRequest;
import com.cygent.model.entities.request.AppVersionRequest;
import com.cygent.model.entities.request.AuthRequest;
import com.cygent.model.entities.request.DeviceTokenRequest;
import com.cygent.model.entities.request.DownloadResourceRequest;
import com.cygent.model.entities.request.GetResourceRequest;
import com.cygent.model.entities.request.GetThemeRequest;
import com.cygent.model.entities.request.LwActivateRequest;
import com.cygent.model.entities.request.UpdateResourceViewCount;
import com.cygent.model.entities.request.UpdateSyncStatus;
import com.cygent.model.entities.request.UpdateViewCountRequest;
import com.cygent.model.entities.response.AcknowledgementResponse;
import com.cygent.model.entities.response.AuthorizationResponse;
import com.cygent.model.entities.response.BaseResponse;
import com.cygent.model.entities.response.DeviceTokenResponse;
import com.cygent.model.entities.response.FileDownloadResponse;
import com.cygent.model.entities.response.GetResourceResponse;
import com.cygent.model.entities.response.LoginResponse;
import com.cygent.model.entities.response.ThemeCustomizeSetting;
import com.cygent.model.entities.response.VersionCheck;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

/**
 * Created by hrdudhat on 3/2/2016.
 * <p/>
 * Contains web services use in application
 */
public interface RestApis {
    //Static file path For Version check
    //Test
//    String VERSION_CHECK = "https://www.test-lw.com/iep/ClientMaterials/admin/AndroidApp/android_version.json";
//    String APP_UPDATE = "https://www.test-lw.com/iep/ClientMaterials/admin/AndroidApp/lwc.apk";

    //Testing new changes
//    String VERSION_CHECK = "https://www.test-lw.com/iep/ClientMaterials/admin/AndroidApp/android_version.json";
//    String APP_UPDATE = "https://www.test-lw.com/iep/ClientMaterials/admin/AndroidApp/lwc.apk";


    //Live
//    String VERSION_CHECK = "https://www.learn-wise.com/lw/ClientMaterials/admin/AndroidApp/android_version.json";
//    String APP_UPDATE = "https://www.learn-wise.com/lw/ClientMaterials/admin/AndroidApp/lwc.apk";

    // new changes
//    String VERSION_CHECK = "http://learnwiseapp.cloudapp.net/learnwiseapplication/android/android_version.json";
//    String APP_UPDATE = "http://learnwiseapp.cloudapp.net/learnwiseapplication/android/lwc.apk";

    //UAT environment
//    String VERSION_CHECK = "https://cipl1.learn-wize.com/learnwiseapplication/android/android_version.json";
//    String APP_UPDATE = "https://cipl1.learn-wize.com/learnwiseapplication/android/lwc.apk";

    //Local Server URL
//    String VERSION_CHECK = "http://192.192.6.200:1010/learnwiseapplication/android/android_version.json";
//    String APP_UPDATE = "http://192.192.6.200:1010/learnwiseapplication/android/lwc.apk";

    //Staging URL
//    String VERSION_CHECK = "http://learn-wisestg.cloudapp.net/api/learnwiseapplication/android/android_version.json";
//    String APP_UPDATE = "http://learn-wisestg.cloudapp.net/api/learnwiseapplication/android/lwc.apk";


    /*//URL on 01-March-2017
    String VERSION_CHECK = "https://www.endologixuniversity.com/lw/ClientMaterials/USA/AndroidApp/Android_version.JSON";
    String APP_UPDATE = "https://www.endologixuniversity.com/lw/ClientMaterials/USA/AndroidApp/LWC.apk";*/

    // For lima app
    //URL on 01-March-2017
//    String VERSION_CHECK = "https://usa.lima-academy.com/learnwiseapplication/Android/android_version_lima.JSON";
//    String APP_UPDATE = "https://usa.lima-academy.com/learnwiseapplication/Android/LWC_lima.apk";

    // For sightpath app
    //URL on 28-March-2017
    /*String VERSION_CHECK = "https://learn.sightpathuniversity.com/learnwiseapplication/Android/android_version_sightpath.JSON";
    String APP_UPDATE = "https://learn.sightpathuniversity.com/learnwiseapplication/Android/LWC_sightpath.apk";*/

    // For corin app
    //URL on 27-August-2018
    String VERSION_CHECK = "https://learnwisepreproduction.blob.core.windows.net/learnwiseapplication/Android/android_version_corin.JSON";
    String APP_UPDATE = "https://learnwisepreproduction.blob.core.windows.net/learnwiseapplication/Android/LWC_CORIN.apk";


    @POST("LwActivateApp")
    Call<BaseResponse<LoginResponse>> loginApi(@Body LwActivateRequest request);

    @POST("LWResourceSync")
    Call<BaseResponse<GetResourceResponse>> getResourceApi(@Body GetResourceRequest request);

    @Streaming
    @POST("GetiPadResourceinStream")
    Call<ResponseBody> downloadFileWithDynamicUrlSync(@Body DownloadResourceRequest request);

//    @POST("LwGetipadResourceinByte")
//    Call<BaseResponse<FileDownloadResponse>> downloadFileWithDynamicUrlSync(@Body DownloadResourceRequest request);

    @GET("GetIpadCustomizeSetting")
    Call<BaseResponse<ThemeCustomizeSetting>> getThemeCustomization(@Body GetThemeRequest request);

//    @POST("DeviceToken")
    @POST("AndroidDeviceToken")
    Call<BaseResponse<DeviceTokenResponse>> registerDeviceTokem(@Body DeviceTokenRequest aDeviceTokenRequest);

    @POST("LwResourceSyncStatusUpdate")
    Call<BaseResponse<AcknowledgementResponse>> downloadAcknowledgement(@Body AcknowledgementRequest aAcknowledgementRequest);

    @POST("UpdateResourceViewCount")
    Call<BaseResponse> updateResourceViewCount(@Body UpdateViewCountRequest aUpdateViewCountRequest);

    @GET
    Call<VersionCheck> getVersionInfo(@Url String url);

    @GET
    @Streaming
    Call<ResponseBody> downloadApkFile(@Url String url);

    @POST("LwUpdateToDownloadSyncStatus")
    Call<BaseResponse> updateSyncStatus(@Body UpdateSyncStatus aUpdateSyncStatus);

    @POST("HasAccess")
    Call<BaseResponse<AuthorizationResponse>> HasAccess(@Body AuthRequest request);

    @POST("LwUserAppVersion")
    Call<Boolean> updateAndroidAppVersion(@Body AppVersionRequest appVersionRequest);

}
