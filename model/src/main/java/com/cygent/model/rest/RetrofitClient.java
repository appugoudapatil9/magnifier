package com.cygent.model.rest;

import com.cygent.model.ModelApp;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by hsnirmal on 5/9/2016.
 */
public class RetrofitClient {
    public static RestApis RETROFIT_CLIENT;
    //    public static String ROOT = "http://m1sales.testapplication4.com/";
//    public static String ROOT = "http://localhost:60859/ProductRestService.svc/";
//    private static String ROOT = "https://www.integrau.com/iep/Services/IPadServices.svc/";
    //Test
//    private static String ROOT = "http://192.192.6.76/IEP/Services/IPadServices.svc/";

    //Test
//     private static String ROOT = "https://jkclient.test-lw.com/iep/Services/IPadServices.svc/";
//    private static String ROOT = "https://www.test-lw.com/iep/Services/IPadServices.svc/";

    //Live
//    private static String ROOT = "https://www.learn-wise.com/lw/Services/IPadServices.svc/";

    //new changes staging
//    private static String ROOT = "http://learnwiseapp.cloudapp.net/Api/";

    //UAT server
//    private static String ROOT = "https://cipl1.learn-wize.com/";

    //Local URL
//    private static String ROOT = "http://192.192.6.200:1010/api/";

    //Stagging
//    private static String ROOT = "http://learn-wisestg.cloudapp.net/api/";

//    private static String ROOT = "http://learnwiseapp.cloudapp.net/Api/";

    //private static String ROOT = "https://www.endologixuniversity.com/lw/Services/IPadServices.svc/";

    //lima
//    private static String ROOT = "https://www.learn-wisego.com/Api/";

    //Pramod
//    private static String ROOT = "https://pramod.learn-wize.com/Api/";

    // UAT
    private static String ROOT = "https://www.learn-wize.com/Api/";

    // PROD
//    private static String ROOT = "https://www.learn-wisego.com/Api/";

//    private static String ROOT = "https://kristinu.learn-wisego.com/Api/";

//    private static  String ROOT = "http://bu.goavega.learn-wize.com/Api/";

    private static String TAG = RetrofitClient.class.getSimpleName();


    private RetrofitClient() {

    }

    public static RestApis get() {
        if (RETROFIT_CLIENT == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ROOT)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(ModelApp.getAppInstance().getOkHttpClient())
                    .build();

            RETROFIT_CLIENT = retrofit.create(RestApis.class);
        }
        return RETROFIT_CLIENT;
    }
}
