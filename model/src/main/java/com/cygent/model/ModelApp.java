package com.cygent.model;

import android.content.SharedPreferences;
import android.os.Environment;
import android.support.v4.util.ArrayMap;

import com.cygent.framework.BaseApplication;
import com.cygent.framework.utils.Constants;
import com.cygent.framework.utils.GsonFactory;
import com.cygent.framework.utils.network.OkHttpFactory;
import com.cygent.model.entities.database.DatabaseManager;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by hrdudhat on 3/2/2016.
 * <p/>
 * Application class of Model module. App module's Application should have this class in super class hierarchy.
 */
public class ModelApp extends BaseApplication {
    private Gson mGson;
    private OkHttpClient mClient;
    private static ModelApp mAppInstance;

    private static SharedPreferences.Editor sEditor;
    private static SharedPreferences sPreferences;
    //Event Bus
    public static EventBus eventBus;

    public static File APP_INTERNAL_ROOT_FOLDER;
    public static File APP_EXTERNAL_ROOT_FOLDER;

    public static final String IMG_FOLDER = "Images";
//    public static final String IMG_RESOURCE = "img_resource_";
//    public static final String IMG_FOLDER = "img_folder_";

    @Override
    public void onCreate() {
        super.onCreate();
        mAppInstance = this;

        sPreferences = getSharedPreferences(Constants.SharedPrefName.SHARED_PREF_NAME,
                MODE_PRIVATE);
        sEditor = sPreferences.edit();

        //@hn Event bus
        eventBus = EventBus.getDefault();
        //e  n   d

        //Init Database for Application
        DatabaseManager.getInstance(getApplicationContext());
        //

        //Init ROOT_FOLDERS
        APP_INTERNAL_ROOT_FOLDER = getFilesDir();
        APP_EXTERNAL_ROOT_FOLDER = Environment.getExternalStorageDirectory();
    }

    /**
     * Created by Dev 1064 on 5/5/2016
     * Modified by Dev 1064 on 5/5/2016
     * <p/>
     * To get singleton instance of {@link ModelApp}
     *
     * @return ModelApp instance
     */
    public static ModelApp getAppInstance() {
        return mAppInstance;
    }

    /**
     * Created by Dev 1064 on 5/5/2016
     * Modified by Dev 1064 on 5/5/2016
     * <p/>
     * To get singleton instance of {@link OkHttpClient}. It will generate {@link OkHttpClient} instance only if existing instance is null
     *
     * @return OkHttpClient instance
     */
    public OkHttpClient getOkHttpClient() {
        if (mClient == null) {
            OkHttpFactory.Builder builder = new OkHttpFactory.Builder();
            if (BuildConfig.DEBUG) {
                builder.enableHttpLog(HttpLoggingInterceptor.Level.BODY);

                HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
                // set your desired log level
                logging.setLevel(HttpLoggingInterceptor.Level.BODY);
                builder.addInterceptor(logging);
            }

            ArrayMap<String,String> headers = new ArrayMap<>();
            headers.put("Content-Type","application/json");
            builder.setConnectTimeOut(300000, TimeUnit.MILLISECONDS);
            builder.setReadTimeOut(300000,TimeUnit.MILLISECONDS);
            builder.addHeaders(headers);
            mClient = builder.build();
        }
        return mClient;
    }

    /**
     * Created by Dev 1064 on 5/5/2016
     * Modified by Dev 1064 on 5/5/2016
     * <p/>
     * To get singleton instance of {@link Gson} with exclude field without expose. It will generate {@link Gson} instance only if existing instance is null
     *
     * @return OkHttpClient instance
     */
    public Gson getGson() {
        if (mGson == null) {
            GsonFactory.Builder builder = new GsonFactory.Builder();
            mGson = builder.buildByExcludeFieldsWithoutExpose();
        }
        return mGson;
    }

    public static void preferencePutString(String asKey, String asValue) {
        sEditor.putString(asKey, asValue);
        sEditor.commit();
    }

    public static void preferencePutInt(String asKey, Integer asValue) {
        sEditor.putInt(asKey,asValue);
        sEditor.commit();
    }

    public static void preferencePutLong(String asKey, Long asValue) {
        sEditor.putLong(asKey,asValue);
        sEditor.commit();
    }

    public static void preferencePutBoolean(String asKey, Boolean asValue) {
        sEditor.putBoolean(asKey,asValue);
        sEditor.commit();
    }

    public static Boolean preferenceGetBoolen(String asKey, Boolean asValue) {
        return sPreferences.getBoolean(asKey,asValue);
    }

    public static Long preferenceGetLong(String asKey, Long asValue) {
        return sPreferences.getLong(asKey,asValue);
    }

    public static Integer preferenceGetInteger(String asKey, Integer asValue) {
        return sPreferences.getInt(asKey, asValue);
    }

    public static String preferenceGetString(String asKey, String asValue) {
        return sPreferences.getString(asKey, asValue);
    }

}
