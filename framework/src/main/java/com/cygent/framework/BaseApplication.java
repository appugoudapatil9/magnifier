package com.cygent.framework;

import android.app.Application;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by 1064 on 5/5/2016.
 */
public class BaseApplication extends Application{

    private RefWatcher mRefWatcher;
    public static ImageLoader imageLoader = null;


    @Override
    public void onCreate() {
        mRefWatcher = LeakCanary.install(this);
        super.onCreate();

    }

    public RefWatcher getRefWatcher() {
        return mRefWatcher;
    }
}
