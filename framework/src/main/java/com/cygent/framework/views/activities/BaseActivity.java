package com.cygent.framework.views.activities;


import android.support.v7.app.AppCompatActivity;

/**
 * Created by 1064 on 5/5/2016.
 */
public class BaseActivity extends AppCompatActivity {
    public static boolean isAppWentToBg = false;
    public static boolean isWindowFocused = false;
    public static boolean isBackPressed = false;


    @Override
    public void onStart() {
        super.onStart();
        applicationWillEnterForeground();
    }

    protected void applicationWillEnterForeground() {
        if (isAppWentToBg) {
            isAppWentToBg = false;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        applicationGoesInBackground();
    }

    protected void applicationGoesInBackground() {
        if (!isWindowFocused) {
            isAppWentToBg = true;
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        isWindowFocused = hasFocus;
        if (isBackPressed && !hasFocus) {
            isBackPressed = false;
            isWindowFocused = true;
        }
        super.onWindowFocusChanged(hasFocus);
    }
}
