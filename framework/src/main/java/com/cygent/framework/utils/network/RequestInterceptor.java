package com.cygent.framework.utils.network;


import android.support.v4.util.ArrayMap;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by hrdudhat on 3/2/2016
 * <p/>
 * This class is used to set headers before calling web service.
 * Here need to pass list of the header in the constructor
 * <p/>
 * <p/>
 * How to use:
 * <p/>
 * <Code>
 * ArrayMap<String,String> headers = new Arraymap<>();
 * headers.put("header1","header-value1");
 * final RequestInterceptor mRequestInterceptor = new RequestInterceptor(headers);
 * OkHttpClient client = new OkHttpClient.Builder().addInterceptor(mRequestInterceptor).build();
 * </Code>
 */
public class RequestInterceptor implements Interceptor {

    private ArrayMap<String, String> mHeaders;

    public RequestInterceptor(ArrayMap<String, String> aHeaders) {
        mHeaders = aHeaders;
    }

    @Override
    public Response intercept(Chain aChain) throws IOException {

        Request.Builder builder = aChain.request().newBuilder();
        for (String key :
                mHeaders.keySet()) {
            builder.addHeader(key, mHeaders.get(key));
        }
        Request newRequest = builder.build();

        return aChain.proceed(newRequest);
    }
}