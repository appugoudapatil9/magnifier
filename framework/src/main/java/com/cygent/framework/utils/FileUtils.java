package com.cygent.framework.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Base64;
import android.util.Base64DataException;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;

import com.cygent.framework.R;
import com.cygent.framework.utils.network.DownLoadUtils;

import org.apache.commons.io.IOUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import okhttp3.ResponseBody;

//import static android.support.v4.content.FileProvider.getUriForFile;

/**
 * Created by Dev 1064 on 5/9/2016
 * <p/>
 * Contains all the method for the File Operation.
 */
public class FileUtils {

    public static final String TAG = FileUtils.class.getSimpleName();

    /**
     * Created by Dev 1064 on 5/9/2016
     * Modified by Dev 1064 on 5/9/2016
     * <p/>
     * This method is used to extract the Zip File.
     *
     * @param aZipFile        a zip file that you want to unpack
     * @param aDestinationDir destination directory where you want to unpack the zip file
     * @return true if zip will be unpacked
     * @throws FileNotFoundException
     */
    public static boolean unpackZipFile(File aZipFile, File aDestinationDir) throws FileNotFoundException {
        FileInputStream fileInputStream;
        ZipInputStream zipInputStream;
        final String sWorkingDir = aDestinationDir.getAbsolutePath() + File.separator;
        try {
            fileInputStream = new FileInputStream(aZipFile);
            zipInputStream = new ZipInputStream(fileInputStream);

            byte buffer[] = new byte[4096];
            int iByteRead;
            ZipEntry zipEntry;

            while ((zipEntry = zipInputStream.getNextEntry()) != null) {
                if (zipEntry.isDirectory()) {
                    final File dir = new File(sWorkingDir, zipEntry.getName());
                    if (!dir.exists()) {
                        dir.mkdir();
                    }
                } else {
                    final FileOutputStream fout = new FileOutputStream(sWorkingDir + zipEntry.getName());
                    while ((iByteRead = zipInputStream.read(buffer)) != -1) {
                        fout.write(buffer, 0, iByteRead);
                    }
                    fout.close();
                }
            }
            zipInputStream.close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    /**
     * Created by Dev 1064 on 5/9/2016
     * Modified by Dev 1064 on 5/9/2016
     *
     * @param aSourceFile     SourceFile to be Copied.
     * @param aDestinationDir Destination directory where file should be copied.
     * @throws IOException if destination directory can't be created
     */
    public static void copyFile(File aSourceFile, File aDestinationDir) throws IOException {
        if (aSourceFile.isDirectory()) {
            if (!aDestinationDir.exists() && !aDestinationDir.mkdirs()) {
                throw new IOException("Cannot create dir " + aDestinationDir.getAbsolutePath());
            }

            String[] children = aSourceFile.list();
            for (int i = 0; i < children.length; i++) {
                copyFile(new File(aSourceFile, children[i]), new File(aDestinationDir, children[i]));
            }
        } else {
            // make sure the directory we plan to store the recording in exists
            File directory = aDestinationDir.getParentFile();
            if (directory != null && !directory.exists() && !directory.mkdirs()) {
                throw new IOException("Cannot create dir " + directory.getAbsolutePath());
            }
            final InputStream in = new FileInputStream(aSourceFile);
            final OutputStream out = new FileOutputStream(aDestinationDir);
            // Copy the bits from instream to outstream
            byte[] buf = new byte[2048];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        }
    }


    /**
     * Created by Dev 1064 on 5/9/2016
     * Modified by Dev 1064 on 5/9/2016
     * <p/>
     * This method is used to read the file line by line. In this
     * method given file will be read and each
     * line will be added to the Array list.
     *
     * @param aSourceFile   source file from where you want to read lines
     * @param aFileEncoding encoding of the dat written in the file
     * @returns ArrayList<String> list of the line read from file
     */
    public static ArrayList<String> readFile(File aSourceFile, String aFileEncoding) throws IOException {
        final ArrayList<String> alLinesList = new ArrayList<String>();
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(aSourceFile);
            if (fileInputStream != null) {
                final InputStreamReader reader = new InputStreamReader(fileInputStream, Charset.forName(aFileEncoding));
                final BufferedReader bufferedReader = new BufferedReader(reader);
                String sLine;
                while ((sLine = bufferedReader.readLine()) != null) {
                    alLinesList.add(sLine);
                }

                fileInputStream.close();
            }
        } catch (IOException e) {
            throw new IOException("File not found");
        }
        return alLinesList;
    }

    /**
     * @Name deleteDirectoryOrFile
     * @CreatedBy Dev 971
     * @CreatedDate Aug 28, 2014
     * @ModifiedBy Dev 971
     * @ModifiedDate Aug 28, 2014
     * @returns void
     * @Purpose This method is used to delete the Given File. In this method if
     * File is Directory then System list all the Files of the
     * Directory and then call this method Recursively to Delete the
     * Files of the Directory.
     */
    public static void deleteDirectoryOrFile(File afTargetFile) {
        if (afTargetFile.exists()) {
            if (afTargetFile.isDirectory()) {
                final File[] arrayFileList = afTargetFile.listFiles();
                if (arrayFileList != null) {
                    if (arrayFileList.length > 0) {
                        for (File file : arrayFileList) {
                            deleteDirectoryOrFile(file);
                        }
                        afTargetFile.delete();
                    } else {
                        afTargetFile.delete();
                    }
                }
            } else {
                afTargetFile.delete();
            }
        }
    }

    /**
     * Created by Dev 1064 on 5/9/2016
     * Modified by Dev 1064 on 5/9/2016
     * <p/>
     * To Remove all the Files from the Given
     * Directory. In this method System Get All the List of Files and
     * Remove all the Files if given file path is for Directory else delete file.
     */
    public static void deleteFileOrDirectory(String aFilePath) throws IOException {
        final File sDirectory = new File(aFilePath);
        deleteDirectoryOrFile(sDirectory);
    }

    /**
     * Created by Dev 1064 on 5/9/2016
     * Modified by Dev 1064 on 5/9/2016
     * <p/>
     * To convert the File to byte[].
     *
     * @param aFilePath path of file from which you need byte array
     * @throws Exception if file not exist or invalid file
     */
    public static byte[] convertFileToByteArray(String aFilePath) throws Exception {
        FileInputStream fileInputStream = null;
        ByteArrayOutputStream baos = null;
        try {
            fileInputStream = new FileInputStream(aFilePath);
            baos = new ByteArrayOutputStream();

            byte[] buffer = new byte[8192];
            int bytesRead;
            while ((bytesRead = fileInputStream.read(buffer)) > 0) {
                baos.write(buffer, 0, bytesRead);
            }
            return baos.toByteArray();
        } catch (Exception e) {
            throw new Exception("File not exist or invalid file");
        } finally {
            if (fileInputStream != null) {
                fileInputStream.close();
            }
            if (baos != null) {
                baos.close();
                baos.flush();
            }

        }
    }

    /**
     * Created by Dev 1064 on 5/9/2016
     * Modified by Dev 1064 on 5/9/2016
     * <p/>
     * To get the File path from the Uri. In this
     * method Uri is converted to File Path.
     */
    public static String getPathFromUri(Context aContext, Uri aUri) {
        String[] projection = {MediaStore.Audio.Media.DATA};
        Cursor cursor = aContext.getContentResolver().query(aUri, projection, null, null, null);
        if (cursor == null) {
            return null;
        }
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        final String aFilePath = cursor.getString(column_index);
        cursor.close();
        return aFilePath;

    }

    /**
     * Created by Dev 1064 on 5/9/2016
     * Modified by Dev 1064 on 5/9/2016
     * <p/>
     * To convert byte array data to File. In this
     * method byte is conveted to file and witteen to the given
     * Directory with given Extention.
     *
     * @param aDirectory Parent Directory when files to be stored.
     * @param aContent   byte array Data to be converted in to File.
     * @param aFileName  Sting Name of the File.
     * @param aExtn      Type of the File. (Extension)
     * @returns File Return the Created {@link File} in the Given Directory.
     */
    public static File getFileFromByteArray(File aDirectory, byte[] aContent, String aFileName, String aExtn) {
        FileOutputStream fileOutputStream = null;
        final File imgFile = new File(aDirectory, aFileName + aExtn);
        try {
            if (imgFile.exists()) {
                imgFile.delete();
            }
            fileOutputStream = new FileOutputStream(imgFile);
            fileOutputStream.write(aContent);

            fileOutputStream.flush();
            fileOutputStream.close();

            return imgFile;

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            aContent = null;
        }
        return null;
    }

    /**
     * To get the total size of all the Files in
     * the Array. In this method if Array is NULL then return 0.
     *
     * @param aFiles {@link Array} of {@link File} to get the size of all files.
     * @return double Returns the total size of all files in given array.
     */
    public static long getFileSize(File[] aFiles) {
        long lTotalSize = 0;
        try {
            /**
             * Array is NULL or Size of array is 0 then simple return 0.
             */
            if (aFiles == null || aFiles.length == 0) {
                return 0;
            }
            for (File file : aFiles) {
                // IF File Exist then Add the Current File size to total file size.
                if (file.exists()) {
                    lTotalSize = lTotalSize + file.length();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return lTotalSize;
    }

    /**
     * Created by Dev 1064 on 5/9/2016
     * Modified by Dev 1064 on 5/9/2016
     * <p/>
     * To create file in the SD card. This method
     * creates File with given Content and type and Stored at the
     * location.
     *
     * @param asFilePath File Path (Directory where file is to be created.)
     * @param asContent  Content to be written on the File.
     * @param asFileName File Name.
     * @returns File Returns the Created File.
     */
    public static File createFile(String asFilePath, String asContent, String asFileName) {
        /**
         * If the Given Parameters are not valida then Return false.
         */
        if (asFilePath == null || asFilePath.trim().length() == 0 || asFileName == null || asFileName.trim().length() == 0) {
            return null;
        }

        File file = new File(asFilePath, asFileName);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            fileWriter.write(asContent);
            return file;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileWriter != null) {
                    fileWriter.flush();
                    fileWriter.close();
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }

        }
        return null;
    }

    /**
     * Created by Dev 1064 on 5/9/2016
     * Modified by Dev 1064 on 5/9/2016
     * <p/>
     * To create file in the SD card. This method
     * creates File with given Content and type and Stored at the
     * location.
     * <p/>
     * In this method File content is written as line given in the
     * Parameter. For Each element of the Array New Line is Created in
     * the File and written to the File in New Line.
     *
     * @param aFilePath         File Path (Directory where file is to be created.)
     * @param aLinesTobeWritten Array of String for lines to be written in the File.
     * @param aFileName         File Name.
     * @returns Return the File created .
     */
    public static File createFile(String aFilePath, String[] aLinesTobeWritten, String aFileName) {
        /**
         * If the Given Parameters are not valida then Return false.
         */
        if (aFilePath == null || aFilePath.trim().length() == 0 || aFileName == null || aFileName.trim().length() == 0) {
            return null;
        }

        File file = new File(aFilePath, aFileName);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            for (String line : aLinesTobeWritten) {
                fileWriter.write(line);
                fileWriter.write("\r\n");
            }
            fileWriter.close();
            return file;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileWriter != null) {
                    fileWriter.flush();
                    fileWriter.close();
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }

        }
        return null;
    }

    /**
     * Created by Dev 1064 on 5/9/2016
     * Modified by Dev 1064 on 5/9/2016
     * <p/>
     * To create New File from the Existing File
     * with Given Character Set. In this method System Reads the File
     * from Given Character set and Creates new File In Given Character
     * Set.
     *
     * @param aSourceFile sourceFile to create file
     * @param aDestFile   Destination directory to create a new file
     * @param aCharSet    charset of existing file
     */
    public static void createFilewithCharacterset(File aSourceFile, File aDestFile, String aCharSet) {
        FileInputStream fileInputStream = null;
        FileOutputStream outputStream = null;
        Writer out = null;
        try {
            fileInputStream = new FileInputStream(aSourceFile);

            if (fileInputStream != null) {
                final InputStreamReader reader = new InputStreamReader(fileInputStream, Charset.forName(aCharSet));
                final BufferedReader bufferedReader = new BufferedReader(reader);

                outputStream = new FileOutputStream(aDestFile);
                out = new BufferedWriter(new OutputStreamWriter(outputStream, Charset.forName(aCharSet)));

                String sLine = "";
                while ((sLine = bufferedReader.readLine()) != null) {
                    out.write(sLine);
                }
                bufferedReader.close();
                out.flush();
                outputStream.flush();
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        } finally {
            if (outputStream != null && out != null) {
                try {
                    out.close();
                    fileInputStream.close();
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * To Create File from Given bytes[]
     *
     * @param aDataBytes          data bytes to write in file
     * @param aDestinationDirPath Destination directory to write data
     * @param aFileName           file name to create file in destination directory
     * @return true if data written successfully else false
     */
    public static boolean createFile(byte[] aDataBytes, String aDestinationDirPath, String aFileName) {
        if (aDataBytes == null || aDataBytes.length == 0) {
            return false;
        }
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(aDestinationDirPath.concat(File.separator).concat(aFileName));
            fileOutputStream.write(aDataBytes);
            fileOutputStream.flush();
            fileOutputStream.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }


    /**
     * Created by Dev 1064 on 5/9/2016
     * Modified by Dev 1064 on 5/9/2016
     * <p/>
     * To write the object to File.
     *
     * @param aContext  Context to write object in given file path using Context.getFileStreamPath()
     * @param aObject   object to write in file
     * @param aFileName File name to create file in application memory
     * @returns boolean true if object written successfully
     */
    public static boolean writeObjectToFile(Context aContext, Object aObject, String aFileName) {
        File file = null;
        FileOutputStream outputStream = null;
        ObjectOutputStream objectOutputStream = null;
        try {
            if (aFileName != null && aFileName.trim().length() > 0) {
                file = aContext.getFileStreamPath(aFileName);
                if (file.exists() || file.createNewFile()) {
                    outputStream = aContext.openFileOutput(aFileName, Context.MODE_PRIVATE);
                    objectOutputStream = new ObjectOutputStream(outputStream);
                    objectOutputStream.writeObject(aObject);
                    objectOutputStream.close();
                    outputStream.close();

                    return true;
                }
            }

        } catch (Exception e) {
            // TODO: handle exception
        } finally {
        }
        return false;
    }

    /**
     * Created by Dev 1064 on 5/9/2016
     * Modified by Dev 1064 on 5/9/2016
     * <p/>
     * To read the Object from File.
     *
     * @returns Object
     */
    public static Object readObjectFromFile(String strFileName, Context context) {
        File file = null;
        FileInputStream fileInputStream = null;
        ObjectInputStream objectInputStream = null;
        Object object = null;
        try {
            if (strFileName != null && strFileName.trim().length() > 0) {
                file = context.getFileStreamPath(strFileName);
                if (file.exists()) {
                    fileInputStream = context.openFileInput(strFileName);
                    objectInputStream = new ObjectInputStream(fileInputStream);
                    object = objectInputStream.readObject();
                    fileInputStream.close();
                    return object;
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
        return null;

    }

    /**
     * @MethodName readFileAsSring
     * @CreatedBy Dev 1064
     * @CreatedDate May 1, 2015
     * @ModifiedBy Dev 1064
     * @ModifiedDate May 1, 2015
     * @returns String
     * @Purpose This method is used to read the file line by line. In thie
     * method given file will be read and added to the Array list each
     * line.
     */
    public static String readFileAsSring(File afFile) {
        if (afFile == null || !afFile.exists()) {
            return null;
        }
        FileInputStream fin = null;
        String ret = null;
        try {
            fin = new FileInputStream(afFile);
            ret = convertStreamToString(fin);
        } catch (IOException aE) {
            aE.printStackTrace();
        } finally {
            try {
                fin.close();
            } catch (IOException e) {
            }
        }

        //Make sure you close all streams.

        return ret;
    }

    /**
     * To write bitmap object to give file path as png encoded image
     *
     * @param aBitmap   bitmap to write
     * @param aFilename file path to write bitmap as png image
     */
    public static void writeBitmapToPNGFile(Bitmap aBitmap, String aFilename) {
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(aFilename);
            aBitmap.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Created by Dev 1064 on 9/14/2015
     * Modified by Dev 1064 on 9/14/2015
     * <p/>
     * To get bitmap from file path
     *
     * @param aFilename file path of the bitmap
     * @return
     */
    public static final Bitmap readBitmapFromFile(String aFilename) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        final Bitmap bitmap = BitmapFactory.decodeFile(aFilename, options);
        return bitmap;
    }

    /**
     * Created by Dev 1064 on 4/4/2016
     * Modified by Dev 1064 on 4/4/2016
     * <p/>
     * To get file list with given extension from directory
     *
     * @param aDropboxCachedBackup directory path to get file list
     * @param aBackUpFileExtn      extension for which you want a list of files
     * @param sortBylastModified   true to sort list by last modified date
     * @return list of files
     */
    public static ArrayList<File> getFileFromDirectory(String aDropboxCachedBackup, String aBackUpFileExtn, boolean sortBylastModified) {
        File file = new File(aDropboxCachedBackup);
        ArrayList<File> alFiles = new ArrayList<>();
        if (file.isDirectory()) {
            File[] files = file.listFiles(new FileFilterByExtension(aBackUpFileExtn));
            if (sortBylastModified) {
                Arrays.sort(files, new Comparator<File>() {
                    public int compare(File f1, File f2) {
                        return Long.compare(f1.lastModified(), f2.lastModified());
                    }
                });
            }
            Collections.addAll(alFiles, files);
        }
        return alFiles;
    }

    /**
     * @Name: convertStreamToString
     * @Created By: Dev: 458
     * @Created Date: Jun 11, 2014
     * @Modified By: Dev: 458
     * @Modified Date: Jun 11, 2014
     * @Purpose: This method is used to convert specified InputStream to
     * readable String. We are using this method generally in API/Web
     * service response parsing
     */
    public static String convertStreamToString(InputStream is) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            throw new IOException("File not exist");
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }


    public static DownLoadUtils.DownloadResult writeBase64StringToDisk(Context context, String aStrBase64, String fileName) {
        DownLoadUtils.DownloadResult result = new DownLoadUtils.DownloadResult();
        FileOutputStream fos = null;

        File resourceDir = new File(context.getFilesDir(), "Resources");

        resourceDir.mkdirs();

        File file = new File(resourceDir, fileName);
//        File file = new File(context.getExternalFilesDir(null), fileName);
        try {
            try {
                if (aStrBase64 != null) {
//                    fos = context.openFileOutput(fileName, Context.MODE_PRIVATE);
                    fos = new FileOutputStream(file);

                    byte[] decodedString = android.util.Base64.decode(aStrBase64, android.util.Base64.DEFAULT);
                    fos.write(decodedString);

                    fos.flush();
                    fos.close();

                    result.setDownloadPath(file.getAbsolutePath());
                    result.setResultCode(1);
                    return result;
                } else {
                    AppLog.d(TAG, "Download Error");
                    result.setDownloadPath("");
                    result.setResultCode(2);
                    return result;
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                AppLog.d(TAG, e.getMessage());
                result.setDownloadPath("");
                result.setResultCode(2);
                return result;
            } catch (IOException e) {
                e.printStackTrace();
                AppLog.d(TAG, e.getMessage());
                result.setDownloadPath(fileName);
                result.setResultCode(2);
                return result;
            } finally {
                if (fos != null) {
                    fos.flush();
                    fos.close();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            AppLog.d(TAG, e.getMessage());
            result.setDownloadPath(fileName);
            result.setResultCode(2);
            return result;
        }
    }


    /**
     * Gets a field from the project's BuildConfig. This is useful when, for example, flavors
     * are used at the project level to set custom fields.
     * @param
     * @param fieldName     The name of the field-to-access
     * @return              The value of the field, or {@code null} if the field is not found.
     */
    private static Object getBuildConfigValue(String fieldName) {
        try {
            /*Class<?> clazz = Class.forName("com.mdrc.school.BuildConfig");*/
            //int resId = context.getResources().getIdentifier("build_config_package", "string", context.getPackageName());
            // try/catch blah blah
            //Class<?> clazz = Class.forName(msPakageName+ ".BuildConfig");
            Class<?> clazz = Class.forName("com.magnifigroup.haagstreitapp.BuildConfig");
            Field field = clazz.getField(fieldName);
            AppLog.d("PackagePrefix :",""+field.get(null));
            return field.get(null);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static void shareFileViaMail(final Context context, File url) throws IOException {

        // Create URI
        File file = url;
//        Uri uri = Uri.fromFile(file);

        Uri uri = FileProvider.getUriForFile(context, getBuildConfigValue("packagePrefix")+".fileprovider", file);


        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Document");//Subject
        intent.putExtra(Intent.EXTRA_TEXT, "");//Body
//        File root = Environment.getExternalStorageDirectory();
/*        File file = new File(root, xmlFilename);
        if (!file.exists() || !file.canRead()) {
            Toast.makeText(this, "Attachment Error", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        Uri uri = Uri.parse("file://" + file);*/
        intent.putExtra(Intent.EXTRA_STREAM, uri);

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        context.startActivity(Intent.createChooser(intent, "Send email..."));

        //Get matching application packages
        List<ResolveInfo> resInfoList = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

        List<String> listPackages = new ArrayList<>();
        listPackages.clear();

        for (ResolveInfo resolveInfo : resInfoList) {
            listPackages.add(resolveInfo.activityInfo.packageName);
        }

        if (!(listPackages.isEmpty())) {
            for (String packageName : listPackages) {
                context.grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }
            context.startActivity(intent);
        } else {
            //redirect user to play store
            DialogUtils.showErrorAlert(context, "Alert", "Supported application is not installed on your device", context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }

    }

    public static void openFileWithDefaultIntent(final Context context
            , File url) throws IOException {

        try {
            // Create URI
            File file = url;
//        Uri uri = Uri.fromFile(file);

            Uri uri = FileProvider.getUriForFile(context, getBuildConfigValue("packagePrefix")+".fileprovider", file);

            Intent intent = new Intent(Intent.ACTION_VIEW);
            // Check what kind of file you are trying to open, by comparing the url with extensions.
            // When the if condition is matched, plugin sets the correct intent (mime) type,
            // so Android knew what application to use to open the file
            if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
                // Word document
                intent.setDataAndType(uri, "application/msword");
            } else if (url.toString().contains(".pdf")) {
                // PDF file
                intent.setDataAndType(uri, "application/pdf");
            } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
                // Powerpoint file
                intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
            } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
                // Excel file
                intent.setDataAndType(uri, "application/vnd.ms-excel");
            } else if (url.toString().contains(".zip") || url.toString().contains(".rar")) {
                // WAV audio file
                intent.setDataAndType(uri, "application/x-wav");
            } else if (url.toString().contains(".rtf")) {
                // RTF file
                intent.setDataAndType(uri, "application/rtf");
            } else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
                // WAV audio file
                intent.setDataAndType(uri, "audio/x-wav");
            } else if (url.toString().contains(".gif")) {
                // GIF file
                intent.setDataAndType(uri, "image/gif");
            } else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
                // JPG file
                intent.setDataAndType(uri, "image/jpeg");
            } else if (url.toString().contains(".txt")) {
                // Text file
                intent.setDataAndType(uri, "text/plain");
            } else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") || url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
                // Video files
                intent.setDataAndType(uri, "video/*");
            } else {
                //if you want you can also define the intent type for any other file

                //additionally use else clause below, to manage other unknown extensions
                //in this case, Android will show all applications installed on the device
                //so you can choose which application to use
                intent.setDataAndType(uri, "*/*");
            }

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            //Get matching application packages
            List<ResolveInfo> resInfoList = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

            List<String> listPackages = new ArrayList<>();
            listPackages.clear();

            for (ResolveInfo resolveInfo : resInfoList) {
                listPackages.add(resolveInfo.activityInfo.packageName);
            }

            if (!(listPackages.isEmpty())) {
                for (String packageName : listPackages) {
                    context.grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
                context.startActivity(intent);
            } else {
                //redirect user to play store
                DialogUtils.showErrorAlert(context, "Alert", "Supported application is not installed on your device", context.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
            }

        } catch (Exception e) {
            AppLog.d(TAG, "openFileWithDefaultIntent : File can not be opened");
        }

    }


    public static void openFile(final Context context, File url) throws IOException {
        // Create URI
//        Uri uri = Uri.parse("content://your.package.name/" + url.getAbsolutePath());
        //Check if supported app is installed
        if (CommonMethods.isSupportedAppInstalled(context)) {
            //supported app is installed
            File file = url;
//        Uri uri = Uri.fromFile(file);

            Uri uri = FileProvider.getUriForFile(context, getBuildConfigValue("packagePrefix")+".fileprovider", file);

//        context.grantUriPermission("com.adobe.reader", uri, Intent.FLAG_GRANT_READ_URI_PERMISSION);

            Intent intent = new Intent(Intent.ACTION_VIEW);
            // Check what kind of file you are trying to open, by comparing the url with extensions.
            // When the if condition is matched, plugin sets the correct intent (mime) type,
            // so Android knew what application to use to open the file
            if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
                // Word document
                intent.setDataAndType(uri, "application/msword");
            } else if (url.toString().contains(".pdf")) {
                // PDF file
                intent.setDataAndType(uri, "application/pdf");
            } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
                // Powerpoint file
                intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
            } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
                // Excel file
                intent.setDataAndType(uri, "application/vnd.ms-excel");
            } else if (url.toString().contains(".rtf")) {
                // RTF file
                intent.setDataAndType(uri, "application/rtf");
            } else if (url.toString().contains(".txt")) {
                // Text file
                intent.setDataAndType(uri, "text/plain");
            } else {
                //if you want you can also define the intent type for any other file

                //additionally use else clause below, to manage other unknown extensions
                //in this case, Android will show all applications installed on the device
                //so you can choose which application to use
                intent.setDataAndType(uri, "*/*");
            }

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            //Get matching application packages
            List<ResolveInfo> resInfoList = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

            List<String> listPackages = new ArrayList<>();

            if (CommonMethods.appInstalledOrNot(Constants.DocViewerpackages.WPS_OFFICE, context)) {
                listPackages.add(Constants.DocViewerpackages.WPS_OFFICE);
            }

            if (CommonMethods.appInstalledOrNot(Constants.DocViewerpackages.QUICK_OFFICE, context)) {
                listPackages.add(Constants.DocViewerpackages.QUICK_OFFICE);
            }

            for (String packageName : listPackages) {
                context.grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }
            //
            context.startActivity(intent);
        } else {
            //redirect user to play store
            DialogUtils.showErrorAlert(context, "Alert", "Supported application is not installed on your device press 'ok' to get application", "Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + Constants.DocViewerpackages.WPS_OFFICE)));
                }
            });
        }
    }



    // int 3 error
    // 1 success
    // 2 memory problem
    public static DownLoadUtils.DownloadResult writeResponseBodyToDisk(Context context, ResponseBody body, String fileName) {
        DownLoadUtils.DownloadResult result = new DownLoadUtils.DownloadResult();
        try {
            // todo change the file location/name according to your needs
            long freeSize = FileUtils.getFreeInternalSpace(context);

//            File file = new File(context.getFilesDir(), fileName);


            File resourceDir = new File(context.getExternalFilesDir(null), "Resources");

            resourceDir.mkdirs();

            File file = new File(resourceDir, fileName);

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[1024];

                long fileSize = body.contentLength();

                if (freeSize > fileSize) {
                    long fileSizeDownloaded = 0;

                    inputStream = body.byteStream();

                   String base64Str =  Base64.encodeToString(body.bytes(),Base64.NO_WRAP);


                    outputStream = new FileOutputStream(file);

//                    InputStream in = new ByteArrayInputStream(body.bytes());

//                    outputStream.write(body.bytes());


                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    byte[] buf = new byte[1024];
                    try {
                        for (int readNum; (readNum = inputStream.read(buf)) != -1;) {
                            bos.write(buf, 0, readNum); //no doubt here is 0
                            //Writes len bytes from the specified byte array starting at offset off to this byte array output stream.
                            System.out.println("read " + readNum + " bytes,");
                        }
                    } catch (IOException ex) {
//                        Logger.getLogger(genJpeg.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    byte[] bytes = bos.toByteArray();

                    outputStream.write(bytes);
                    outputStream.flush();
                    outputStream.close();

//                    IOUtils.copy(in, outputStream);
//                    FileUtils.writeByteArrayToFile(new File("pathname"), myByteArray)

//                    IOUtils.write(body.bytes(),outputStream);

                   /* while (true) {
                        int read = inputStream.read(fileReader);

                        if (read == -1) {
                            break;
                        }

                        outputStream.write(fileReader, 0, read);

                        fileSizeDownloaded += read;

                        AppLog.d(TAG, "file download: " + fileSizeDownloaded + " of " + fileSize);


                    }*/

                //Update Database here with file path

                //
                //Re work

    /*                FileInputStream fis = new FileInputStream(file);
                    //System.out.println(file.exists() + "!!");
                    //InputStream in = resource.openStream();
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    byte[] buf = new byte[1024];
                    try {
                        for (int readNum; (readNum = fis.read(buf)) != -1;) {
                            bos.write(buf, 0, readNum); //no doubt here is 0
                            //Writes len bytes from the specified byte array starting at offset off to this byte array output stream.
                            System.out.println("read " + readNum + " bytes,");
                        }
                    } catch (IOException ex) {
//                        Logger.getLogger(genJpeg.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    byte[] bytes = bos.toByteArray();

                    //below is the different part
                    File someFile = new File(resourceDir, "new"+fileName);

                    FileOutputStream fos = new FileOutputStream(someFile);
                    fos.write(bytes);
                    fos.flush();
                    fos.close();*/

                //
//                    ByteArrayInputStream bInput = new ByteArrayInputStream(org.apache.commons.io.FileUtils.readFileToByteArray(file));

                //Converted file to

                //

                result.setDownloadPath(file.getAbsolutePath());
                result.setResultCode(Constants.DOWNLOAD_SUCCESS);

                return result;
            }else{

                result.setDownloadPath("");
                result.setResultCode(Constants.MEMORY_ERROR);

                return result;
            }

        } catch (IOException e) {
            result.setDownloadPath("");
            result.setResultCode(Constants.DOWNLOAD_FAILURE);

            return result;
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }

            if (outputStream != null) {
                outputStream.close();
            }
        }
    }

    catch(
    IOException e
    )

    {
        result.setDownloadPath("");
        result.setResultCode(Constants.DOWNLOAD_FAILURE);

        return result;
    }

}


    public static String getDownloadPath(Context context) {
        File destinationFile = new File(context.getExternalFilesDir(null) + File.separator + "HGSO1" + File.separator + "Downloads");
        return destinationFile.getAbsolutePath();
    }

    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public static long getFreeInternalSpace(Context context) {
        long freeBytesInternal = new File(context.getFilesDir().getAbsoluteFile().toString()).getFreeSpace();
        return freeBytesInternal;
    }

    public static boolean isSpaceAvailable(Context context, long fileSizeInBytes) {

        //file size in bytes
        AppLog.d(TAG, "File size : " + fileSizeInBytes);
//        long availableSize = getFreeInternalSpace(context);
        long availableSize = getAvailableInternalStorage(context);
        AppLog.d(TAG, "Available size : " + availableSize);
        AppLog.d(TAG, "fileSize : " + fileSizeInBytes);
        AppLog.d(TAG, "availableSize : " + availableSize);
        if (availableSize > fileSizeInBytes) {
            return true;
        }
        return false;
    }

    public static long getAvailableInternalStorage(Context context) {
//        File path = ;
        StatFs stat = new StatFs(context.getFilesDir().getPath());
        long availableBlocks = 0;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2){
             availableBlocks = stat.getAvailableBytes();
        }else{
            availableBlocks = ((long) stat.getAvailableBlocks() * (long) stat.getBlockSize());
        }

        return availableBlocks;
    }


    public static String formatFileSize(long alFileSize) {
        String msSizeWithUnit = null;

//        final double mdBytes = alFileSize;
//        final double mdKiloBytes = alFileSize / 1024.0;
//        final double mdMB = alFileSize / 1048576.0; // ((size / 1024.0) / 1024.0);
//        final double mdGB = alFileSize / 1073741824.0; //(((size / 1024.0) / 1024.0) / 1024.0);
        //		final double mdTB = alFileSize / 1099511627776.0;//((((alFileSize / 1024.0) / 1024.0) / 1024.0) / 1024.0);

        double mdBytes = alFileSize;
        double mdKiloBytes = alFileSize / 1024.0;
        double mdMB = ((alFileSize / 1024.0) / 1024.0);
        double mdGB = (((alFileSize / 1024.0) / 1024.0)/1024);
//        double t = ((((alFileSize/1024.0)/1024.0)/1024.0)/1024.0);

        DecimalFormat dec = new DecimalFormat("0.00");

		/*if (mdTB > 1)
        {
			msSizeWithUnit = dec.format(mdTB).concat(" TB");
		}
		else */
        if (mdGB >= 1) {
            msSizeWithUnit = dec.format(mdGB).concat(" GB");
        } else if (mdMB >= 1) {
            msSizeWithUnit = dec.format(mdMB).concat(" MB");
        } else if(mdKiloBytes >= 1 ){
            msSizeWithUnit = dec.format(mdKiloBytes).concat(" KB");
        }else if(mdBytes >= 0){
            msSizeWithUnit = dec.format(mdBytes).concat(" Byte");
        }else {
            msSizeWithUnit=null;
        }
        return msSizeWithUnit;
    }


}
