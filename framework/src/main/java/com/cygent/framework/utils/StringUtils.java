package com.cygent.framework.utils;

import android.text.TextUtils;
import android.util.Patterns;

/**
 * Created by hrdudhat on 1/8/2016.
 * <p/>
 * Contains utility methods to perform manipulation on string
 */
public class StringUtils {
    public static final String EMPTY = "";

    /**
     * Created by Dev hrdudhat on 12/21/2015
     * Modified by Dev hrdudhat on 12/21/2015
     * <p/>
     * To check out that given string is null or empty after trimming
     *
     * @param aString String to check
     * @return true if String is empty even if it is pad with white space
     */
    public static boolean isTrimmedEmpty(String aString) {
        return aString == null || aString.trim().length() == 0;
    }

    /**
     * To check is it a valid email id or not.
     *
     * @param email email addess to validate
     * @return
     */
    public static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
