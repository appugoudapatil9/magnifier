package com.cygent.framework.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by hrdudhat on 1/19/2016.
 * <p/>
 * Contains utility method for network operations. Like check internet connection
 *
 * @author hrdudhat
 */
public class NetworkUtils {
    /**
     * This method is used to check internet connections is available either by WiFi or Mobile network.
     *
     * @param context application Context to get CONNECTIVITY_SERVICE
     * @return true if internet on else false
     */
    public static boolean getConnectivityStatus(Context context) {
        final ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return true;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return true;
        }
        return false;
    }
}
