package com.cygent.framework.utils.network;

/**
 * Created by hsnirmal on 6/2/2016.
 */
public class DownLoadUtils {

    public static class DownloadResult{

        public static final int DOWNLOAD_SUCCESS = 1;
        public static final int DOWNLOAD_MEMORY_ERROR = 2;
        public static final int DOWNLOAD_FAILED= 3;


        int resultCode;
        String downloadPath;

        public int getResultCode() {
            return resultCode;
        }

        public void setResultCode(int resultCode) {
            this.resultCode = resultCode;
        }

        public String getDownloadPath() {
            return downloadPath;
        }

        public void setDownloadPath(String downloadPath) {
            this.downloadPath = downloadPath;
        }
    }
}
