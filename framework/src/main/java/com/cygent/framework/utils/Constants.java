package com.cygent.framework.utils;

/**
 * Created by hsnirmal on 5/10/2016.
 */
public class Constants {

    public static final int MEMORY_ERROR = 3;
    public static final int DOWNLOAD_SUCCESS = 1;
    public static final int DOWNLOAD_FAILURE = 2;

    public static class SharedPrefName{
        public static final String SHARED_PREF_NAME = "haggstreiteapppref";
    }

    public static class SharedPrefKeys{
        public static final String CLIENT_ADMIN_EMAIL = "clientadminemail";
        public static final String CLIENT_ID = "clientId";
        public static final String LIB_DISCLAIMER = "librarydisclaimer";
        public static final String SITE_NAME = "siteName";
        public static final String USER_ID = "userId";
        public static final String USER_NAME = "userName";
        public static final String HAS_RIGHTS_FOR_LIB_DISCLAIMER = "hasrightsforlibdisclaimer";
        public static final String HAS_LOGED_IN = "haslogedin";
        public static final String GCM_TOKEN = "gcm_token";
        public static final String GCM_TOKEN_REGISTERED = "gcm_token_registered";
        public static final String THEME_VERSION = "ThemeVersion";
        public static final String THEME_FONT_COLOR = "ThemeFontColor";
        public static final String THEME_HEADER_COLOR = "ThemeHeaderColor";
        public static final String THEME_IMG_LOGO = "ThemeImgLogo";
        public static final String THEME_IMG_BACKGROUND = "ThemeImgBackGround";
        public static final String THEME_IMG_PORTRAIT = "ThemeImgPortrait";
        public static final String IS_THEME_UPDATE_REQUIRED = "IsThemeUpdateIsRequired";
        public static final String IS_THEME_SAVED = "IsThemeSaved";

        public static final String DISCLAIMER_TEXT = "DisclaimerText";
        public static final String DISCLAIMER_HIDE_DATE_SIZE = "hasRightsforHideDateandSize";
        public static final String DISCLAIMER_RIGHTS_FLAG = "hasRightsforLibraryDisclaimer";
        public static final String REPORT_SYNC_LONG_DATE = "reportsynclongdate";
        public static final String CONFIRMATION_MSG = "confirmationMsg";
        public static final String CONFIRMATION_MODIFIED_DATE = "confirmationModifiedDate";
    }

    public static class IntentKeys{
        public static final String INTENT_VIDEO_FILE = "IntentVideoFile";
        public static final String INTENT_IMAGE_FILE = "IntentImageFile";
    }

    public static class DocViewerpackages{
        public static final String WPS_OFFICE = "cn.wps.moffice_eng";
        public static final String WPS_APP = "WPS Office";

        public static final String QUICK_OFFICE = "com.quickoffice.android";
        public static final String  QUICK_OFFICE_APP= "Quick Office";
    }

}
