package com.cygent.framework.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;

/**
 * Created by hsnirmal on 5/12/2016.
 */
public class CommonMethods {

    public static void showErrorAlert(final Context ctx, String sTitle, String sMessage) {

        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setMessage(sMessage);
        builder.setTitle(sTitle);
        builder.setPositiveButton(ctx.getString(android.R.string.ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }


    public static boolean appInstalledOrNot(String uri, Context context) {
        PackageManager pm = context.getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }
        catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    public static boolean isSupportedAppInstalled(Context context)
    {
        if(appInstalledOrNot(Constants.DocViewerpackages.WPS_OFFICE,context) || appInstalledOrNot(Constants.DocViewerpackages.QUICK_OFFICE,context)){

            return true;
        }
        return false;
    }

}
