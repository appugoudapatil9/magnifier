package com.cygent.framework.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by 1064 on 5/4/2016.
 * <p/>
 * To get Gson instance for application. It is a basic Gson factory class which can provide Gson with ignore or with expose field policy.
 * <p/>
 * To get Gson object use Builder class of GsonFactory
 */
public class GsonFactory {
    private static Gson mGson;


    public static class Builder {
        GsonBuilder mGsonBuilder;

        public Builder() {
            mGsonBuilder = new GsonBuilder();
        }

        public Gson buildByExcludeFieldsWithoutExpose() {
            mGsonBuilder.excludeFieldsWithoutExposeAnnotation();
            return mGsonBuilder.create();
        }

        public Gson build() {
            return mGsonBuilder.create();
        }
    }
}
