package com.cygent.framework.utils.network;

/**
 * Created by hsnirmal on 1/24/2017.
 */

public class DeleteUtils {

    public static final int DELETE_SUCCESS = 1;
    public static final int DELETE_ERROR = 2;

    int resultCode;

    public DeleteUtils(int deleteSuccess) {
        this.resultCode = deleteSuccess;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }
}
