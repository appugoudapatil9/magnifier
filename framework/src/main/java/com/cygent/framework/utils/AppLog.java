package com.cygent.framework.utils;

import android.util.Log;

/*************************************************************************************
 * @Name: AppLog
 * @CreatedBy: sameer
 * @CreatedDate: 13-10-2014
 * @ModifiedBy:
 * @ModifiedDate:
 * @Purpose: This class is used to show and hide application logs.
 **************************************************************************************/
public class AppLog {

	/**
	 * if you need to implement encryption in database shared preference and web servics
	 * 
	 * */
	public static boolean isEncryptionOn=false;
	private static boolean sFlagDebug= false;
	private static boolean sFlagInfo= false;
	private static boolean sFlagErr= false;
	
	/*************************************************************************************
    * Procedure Name: d
    * Created By: sameer
    * Created Date: 13-10-2014
    * Modified By:  
    * Modified Date: 
    * Purpose: This method is used to display log in debug fashion. 
     **************************************************************************************/
   
	public  static void d(String tag, String message){
		if(sFlagDebug)
		{
			 Log.d(tag,message);
//			 Log.e(tag,message);
		}
	}
	
	/*************************************************************************************
	    * Procedure Name: i
	    * Created By: sameer
	    * Created Date: 13-10-2014
	    * Modified By:  
	    * Modified Date: 
	    * Purpose: This method is used to display log in info fashion. 
	     **************************************************************************************/
    
	public  static void i(String tag, String message){
		if(sFlagInfo)
		{
			Log.i(tag,message);
//			Log.e(tag,message);
			
		}
	}
	
	/*************************************************************************************
	    * Procedure Name: e
	    * Created By: sameer
	    * Created Date: 13-10-2014
	    * Modified By:  
	    * Modified Date: 
	    * Purpose: This method is used to display log in error fashion. 
	     **************************************************************************************/
	public  static void e(String tag, String message){
		if(sFlagErr)
		{
			Log.e(tag,message);
		}
	}
}
