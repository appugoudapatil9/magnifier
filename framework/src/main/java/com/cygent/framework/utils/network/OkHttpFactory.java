package com.cygent.framework.utils.network;

import android.content.Context;
import android.support.v4.util.ArrayMap;

import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by hrdudhat on 5/4/2016.
 * <p/>
 * Factory class to get OkHttpClient instance by setting its interceptors and different properties
 */
public class OkHttpFactory {
    private OkHttpClient mOkHttpClient;
    private static final int DEFAULT_CONNECTION_TIMEOUT = 30; // second


    public void OkHttpFactory() {
        mOkHttpClient = new OkHttpClient();
    }

    /**
     * To get default OkHttpClient with out any interceptor or log level. To add OkHttpClient with its properties use {@link Builder} class
     *
     * @return default OkHttpClient
     */
    public OkHttpClient getOkHttpClient() {
        return mOkHttpClient;
    }

    /**
     * Created by hrdudhat on 5/4/2016.
     * <p/>
     * Builder class to add properties in OkHttpClient like read timeout, connection timeout, Interceptor
     */
    public static class Builder {
        private OkHttpClient.Builder mBuilder;

        public Builder() {
            mBuilder = new OkHttpClient.Builder();
            setConnectTimeOut(DEFAULT_CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        }

        /**
         * Created by hrdudhat on 5/4/2016
         * Modified by hrdudhat on 5/4/2016
         * <p/>
         * To get {@link OkHttpClient} instance with it properties set by using builder methods
         *
         * @return {@link OkHttpClient} instance
         */
        public OkHttpClient build() {
            return mBuilder.build();
        }

        /**
         * Created by hrdudhat on 5/4/2016
         * Modified by hrdudhat on 5/4/2016
         * <p/>
         * To add given headers with given key and value
         *
         * @param aHeaderKeyValue list of key value pairs
         * @return {@link Builder} instance to add new properties or to get {@link OkHttpClient}instance
         */
        public Builder addHeaders(ArrayMap<String, String> aHeaderKeyValue) {
            final RequestInterceptor requestInterceptor = new RequestInterceptor(aHeaderKeyValue);
            mBuilder.addInterceptor(requestInterceptor);
            return this;
        }

        /**
         * Created by hrdudhat on 5/4/2016
         * Modified by hrdudhat on 5/4/2016
         * To add Http request, response or header log based on given log level
         *
         * @param aLevel Log level to print in logcat
         * @return {@link Builder} instance to add new properties or to get {@link OkHttpClient}instance
         * @see HttpLoggingInterceptor.Level
         */
        public Builder enableHttpLog(HttpLoggingInterceptor.Level aLevel) {
            final HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
            httpLoggingInterceptor.setLevel(aLevel);
            mBuilder.addInterceptor(httpLoggingInterceptor);
            return this;
        }

        /**
         * Created by hrdudhat on 5/4/2016
         * Modified by hrdudhat on 5/4/2016
         * <p/>
         * To add cookie manager for session web api integration
         *
         * @param aContext Application or activity context to store cookies in shared pref. Recommend to use Application context
         * @return {@link Builder} instance to add new properties or to get {@link OkHttpClient}instance
         */
        public Builder addCookieManager(Context aContext) {
            final CookieHandler cookieHandler = new CookieManager(new PersistentCookieStore(aContext), CookiePolicy.ACCEPT_ALL);
            mBuilder.cookieJar(new JavaNetCookieJar(cookieHandler));
            return this;
        }

        /**
         * Created by hrdudhat on 5/4/2016
         * Modified by hrdudhat on 5/4/2016
         * <p/>
         * To add any Interceptor in OkHttpClient interceptor list
         *
         * @param aInterceptor
         * @return {@link Builder} instance to add new properties or to get {@link OkHttpClient}instance
         */
        public Builder addInterceptor(Interceptor aInterceptor) {
            if (aInterceptor != null)
                mBuilder.addInterceptor(aInterceptor);
            return this;
        }

        /**
         * Created by hrdudhat on 5/4/2016
         * Modified by hrdudhat on 5/4/2016
         * <p/>
         * To set read time out for web api
         *
         * @param aReadTimeOut Read time out for api calling.
         * @param aTimeUnit    Time unit for read time out
         * @return {@link Builder} instance to add new properties or to get {@link OkHttpClient}instance
         */
        public void setReadTimeOut(long aReadTimeOut, TimeUnit aTimeUnit) {
            mBuilder.readTimeout(aReadTimeOut, aTimeUnit);
        }

        /**
         * Created by hrdudhat on 5/4/2016
         * Modified by hrdudhat on 5/4/2016
         * <p/>
         * To set connection timeout for web api.
         *
         * @param aConnectionTimeout connection time out for api calling.
         * @param aTimeUnit          Time unit for read time out
         * @return {@link Builder} instance to add new properties or to get {@link OkHttpClient}instance
         */
        public void setConnectTimeOut(long aConnectionTimeout, TimeUnit aTimeUnit) {
            mBuilder.connectTimeout(aConnectionTimeout, aTimeUnit);
        }
    }
}
