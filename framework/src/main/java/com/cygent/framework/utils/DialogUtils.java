package com.cygent.framework.utils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;

import com.cygent.framework.R;


/**
 * Created by 1064 on 5/12/2016.
 */
public class DialogUtils {

    static ProgressDialog mProgressDialog;
    static ProgressDialog getmProgressDialogBar;
    static ProgressDialog mDownloadProgress;
    static AlertDialog errorAlert;

    /**
     * Created by Dev 1064 on 5/12/2016
     * Modified by Dev 1064 on 5/12/2016
     * <p/>
     * Simple one button dialog
     *
     * @param aContext activity context to show alert
     * @param sTitle   dialog title
     * @param sMessage message to show
     */
    public static void showErrorAlert(Context aContext, String sTitle, String sMessage, String aButtonText, DialogInterface.OnClickListener aOnClickListener) {
        if(errorAlert!=null) {
            errorAlert.dismiss();
            errorAlert = null;
        }
            final AlertDialog.Builder builder = new AlertDialog.Builder(aContext);
            builder.setMessage(sMessage);
            builder.setTitle(aContext.getString(R.string.app_name));
            builder.setPositiveButton(aButtonText, aOnClickListener);
            errorAlert = builder.create();
            errorAlert.show();
    }

    public static void showOptionDialog(Context aContext, String sTitle, String sMessage, String aButtonTextPositive,String aButtonTextNegative, DialogInterface.OnClickListener aOnPositiveClickListener,DialogInterface.OnClickListener aOnNagativeClickListener) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(aContext);
        builder.setMessage(sMessage);
        builder.setTitle(aContext.getString(R.string.app_name));
        builder.setPositiveButton(aButtonTextPositive, aOnPositiveClickListener);
        builder.setNegativeButton(aButtonTextNegative,aOnNagativeClickListener);
        AlertDialog alert = builder.create();
        alert.show();

    }

    public static void showProgressUpdateBar(Context aContext,String aMessage)
    {
        if(mProgressDialog!=null){
            mProgressDialog.dismiss();
            mProgressDialog=null;
        }
        mProgressDialog = new ProgressDialog(aContext);
        mProgressDialog.setMessage(aMessage);
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setMax(100);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public static void showDownloadProgress(Context aContext, String aMessage,DialogInterface.OnCancelListener aPositiveClick, DialogInterface.OnClickListener aNegativeClick){
        if(mDownloadProgress==null || !(mDownloadProgress.isShowing())) {
            mDownloadProgress = new ProgressDialog(aContext);
            mDownloadProgress.setMessage(aMessage);
            mDownloadProgress.setIndeterminate(false);
            mDownloadProgress.setMax(100);
            mDownloadProgress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mDownloadProgress.setCancelable(false);
            mDownloadProgress.setButton(DialogInterface.BUTTON_NEGATIVE,aContext.getString(android.R.string.cancel),aNegativeClick);
            mDownloadProgress.show();
        }else {
            if(aMessage!=null && !aMessage.isEmpty()) {
                mDownloadProgress.setMessage(aMessage);
            }
            mDownloadProgress.setMax(100);
            mDownloadProgress.setProgress(0);
            mDownloadProgress.setButton(DialogInterface.BUTTON_NEGATIVE,aContext.getString(android.R.string.cancel),aNegativeClick);
        }
    }

    public static void setDownloadProgress(int aProgress){
        if(mDownloadProgress!=null && mDownloadProgress.isShowing()){
            mDownloadProgress.setProgress(aProgress);
        }
    }

    public static void hideDownloadProgress(){
        if(mDownloadProgress!=null && mDownloadProgress.isShowing()){
            mDownloadProgress.dismiss();
            mDownloadProgress=null;
        }
    }





    public static void showProgressDialog(Context aContext,String aMessage,int style){

        if(mProgressDialog == null)
        {
            mProgressDialog = new ProgressDialog(aContext);
            mProgressDialog.setMessage(aMessage);
            mProgressDialog.setProgressStyle(style);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgress(0);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }else{

            if(mProgressDialog.isShowing()){
                mProgressDialog.setMessage(aMessage);
            }else{
                mProgressDialog.dismiss();
                mProgressDialog = null;
                mProgressDialog = new ProgressDialog(aContext);
                mProgressDialog.setMessage(aMessage);
                mProgressDialog.setProgressStyle(style);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.setProgress(0);
                mProgressDialog.setCancelable(false);
                mProgressDialog.show();
            }
        }

    }

    public static  void dismissProgressDialog()
    {
        if(mProgressDialog!=null)
        {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    public static void showConfirmationAlert(Context aContext, String sTitle, String sMessage, String aButtonText, DialogInterface.OnClickListener aOnClickListener) {
        if(errorAlert!=null) {
            errorAlert.dismiss();
            errorAlert = null;
        }
        final AlertDialog.Builder builder = new AlertDialog.Builder(aContext);
        builder.setMessage(Html.fromHtml(sMessage));
        builder.setTitle(sTitle);
        builder.setPositiveButton(aButtonText, aOnClickListener);
        errorAlert = builder.create();
        errorAlert.show();
    }

}
