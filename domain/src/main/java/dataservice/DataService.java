package dataservice;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.cygent.model.ModelApp;
import com.cygent.model.entities.database.DatabaseManager;
import com.cygent.model.entities.database.ResourceEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class DataService extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_FETCH_RESOURCES = "com.magnifigroup.haagstreitapp.intentservice.action.FETCH_RESOURCES";
    private static final String ACTION_GET_ENTITY_RESOURCES = "com.magnifigroup.haagstreitapp.intentservice.action.GET_ENTITY_RESOURCES";
    private static final String ACTION_DOWNLOAD_FILE = "com.magnifigroup.haagstreitapp.intentservice.action.DOWNLOAD_FILE";

    // TODO: Rename parameters
    private static final String EXTRA_PARAM_FOLDER_ID = "com.magnifigroup.haagstreitapp.intentservice.extra.FOLDER_ID";
    private static final String EXTRA_PARAM_RESULT_RECEIVER = "com.magnifigroup.haagstreitapp.intentservice.extra.RESULT_RECEIVER";

    public DataService() {
        super("DataService");
    }


    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionFetchResources(Context context,DataResultReceiver resultReceiver) {
        Intent intent = new Intent(context, DataService.class);
        intent.setAction(ACTION_FETCH_RESOURCES);
        intent.putExtra(EXTRA_PARAM_RESULT_RECEIVER,resultReceiver);
        context.startService(intent);
    }

    public static void startActionGetEntityResources(Context context,Long folderId,DataResultReceiver resultReceiver) {
        Intent intent = new Intent(context, DataService.class);
        intent.setAction(ACTION_GET_ENTITY_RESOURCES);
        intent.putExtra(EXTRA_PARAM_FOLDER_ID,folderId);
        intent.putExtra(EXTRA_PARAM_RESULT_RECEIVER,resultReceiver);
        context.startService(intent);
    }

    public static void startActionDownloadFile(Context context)
    {

    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_FETCH_RESOURCES.equals(action)) {
                final ResultReceiver resultReceiver = intent.getParcelableExtra(EXTRA_PARAM_RESULT_RECEIVER);
                handleActionFetchResources(this,resultReceiver);
            } else if (ACTION_GET_ENTITY_RESOURCES.equals(action)) {
                final Long folderId = intent.getLongExtra(EXTRA_PARAM_FOLDER_ID,0L);
                final ResultReceiver resultReceiver = intent.getParcelableExtra(EXTRA_PARAM_RESULT_RECEIVER);
                handleActionGetEntityResources(this,folderId,resultReceiver);
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionFetchResources(Context context,ResultReceiver resultReceiver) {

        List<ResourceEntity> resourceEntityList = new ArrayList<>();
        resourceEntityList.addAll(DatabaseManager.getInstance(context).getResourceList());
        ResourceEntity resourceEntity = new ResourceEntity();
        resourceEntity.setResourceEntityList(resourceEntityList);

//        DataReceiver<ResourceEntity> dataReceiver = new DataReceiver();
//        dataReceiver.setResultCode(DataReceiver.ACTION_FETCH_RESOURCE);
//        dataReceiver.setResultObject(resourceEntity);

        if(resultReceiver!=null)
        {
            Bundle bundle = new Bundle();
            bundle.putParcelable(DataResultReceiver.RESULT_OBJECT,resourceEntity);
            resultReceiver.send(DataResultReceiver.RESULT_FETCH_RESOURCE,bundle);
        }

//        ModelApp.eventBus.post(dataReceiver);
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleActionGetEntityResources(Context context, Long arg_folderId, ResultReceiver resultReceiver) {
        List<ResourceEntity> resourceEntityList = new ArrayList<>();
        resourceEntityList.addAll(DatabaseManager.getInstance(context).getResourceListForEntity(arg_folderId));
        ResourceEntity resourceEntity = new ResourceEntity();
        resourceEntity.setResourceEntityList(resourceEntityList);
//        DataReceiver<ResourceEntity> dataReceiver = new DataReceiver();
//        dataReceiver.setResultCode(DataReceiver.ACTION_GET_ENTITY_RESOURCES);
//        dataReceiver.setResultObject(resourceEntity);
//
//        ModelApp.eventBus.post(dataReceiver);


        if(resultReceiver!=null)
        {
            Bundle bundle = new Bundle();
            bundle.putParcelable(DataResultReceiver.RESULT_OBJECT,resourceEntity);
            resultReceiver.send(DataResultReceiver.RESULT_GET_ENTITY_RESOURCES,bundle);
        }

    }
}
