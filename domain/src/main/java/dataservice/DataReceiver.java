package dataservice;

import android.os.Bundle;

/**
 * Created by hsnirmal on 6/15/2016.
 */
public class DataReceiver<T>{


    //Static result <code>
    public static final int ACTION_FETCH_RESOURCE = 100;
    public static final int ACTION_GET_ENTITY_RESOURCES = 101;
    //

    int resultCode;
    T resultObject;

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public T getResultObject() {
        return resultObject;
    }

    public void setResultObject(T resultObject) {
        this.resultObject = resultObject;
    }
}
