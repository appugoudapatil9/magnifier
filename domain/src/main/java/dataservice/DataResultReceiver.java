package dataservice;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

/**
 * Created by hsnirmal on 6/15/2016.
 */
public class DataResultReceiver extends ResultReceiver {

    //Result Constants
    public static final int RESULT_FETCH_RESOURCE = 100;
    //bundle result keys
    public static final String RESULT_OBJECT = "resultobject";

    public static final int RESULT_GET_ENTITY_RESOURCES = 101;
    //

    private Listener listener;

    public DataResultReceiver(Handler handler) {
        super(handler);
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (listener != null)
            listener.onReceiveResult(resultCode, resultData);
    }

    public static interface Listener {
        void onReceiveResult(int resultCode, Bundle resultData);
    }
}
