package com.cygent.domain.webapi;

import com.cygent.model.entities.request.DeviceTokenRequest;

/**
 * Created by 1064 on 5/12/2016.
 */
public interface GCMTokenService {
    void registerDeviceToken(DeviceTokenRequest aDeviceTokenRequest);
}
