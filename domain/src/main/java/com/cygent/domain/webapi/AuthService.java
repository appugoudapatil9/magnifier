package com.cygent.domain.webapi;

import com.cygent.model.entities.request.AuthRequest;

/**
 * Created by appu on 10/15/2018.
 */
public interface AuthService {

    void isAuthorized(AuthRequest authRequest);
}
