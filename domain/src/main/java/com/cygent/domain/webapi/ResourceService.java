package com.cygent.domain.webapi;

import com.cygent.framework.utils.network.DownLoadUtils;
import com.cygent.model.entities.database.ResourceEntity;
import com.cygent.model.entities.request.AcknowledgementRequest;
import com.cygent.model.entities.request.DownloadResourceRequest;
import com.cygent.model.entities.request.GetResourceRequest;
import com.cygent.model.entities.request.UpdateViewCountRequest;

import java.util.List;

/**
 * Created by hsnirmal on 5/11/2016.
 */
public interface ResourceService {

    //Web apis
    public void getResourceList(GetResourceRequest requestObj);
    public void sendAcknowledgement(ResourceEntity resourceEntity, DownLoadUtils.DownloadResult result);
    public void downloadResource(ResourceEntity resourceEntity);

    //db operation  async
    public void insertUpdateResource(List<ResourceEntity> resourceEntityList);
    public void updateResourceViewCount(ResourceEntity resourceEntity);
    public List<ResourceEntity> getDownloadedResourceList();
    public int deleteResource(ResourceEntity resourceEntity);


    public void getAllResourceList();

    //db operation  sync
    public void getResourceListForEntity(ResourceEntity resourceEntity);

    //General operations
    public boolean isResourceExist(ResourceEntity resourceEntity);

    public void updateResource(ResourceEntity resourceEntity);

    public void callUpdateResourceViewCount();

    public void updateSyncStatus(ResourceEntity resourceEntity);
}
