package com.cygent.domain.webapi;

import android.content.Context;
import android.widget.Toast;

import com.cygent.framework.utils.AppLog;
import com.cygent.model.ModelApp;
import com.cygent.model.entities.request.AuthRequest;
import com.cygent.model.entities.response.AuthorizationResponse;
import com.cygent.model.entities.response.BaseResponse;
import com.cygent.model.entities.response.ErrorMessage;
import com.cygent.model.entities.response.GetResourceResponse;
import com.cygent.model.rest.RetrofitClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by appupatil on 15/10/18.
 */

public class AuthServiceImpl extends GCMTokenServiceImpl implements AuthService{

    private static final String TAG = "AuthServiceImpl";

    Context mContext;

    public AuthServiceImpl(Context context) {
        super(RetrofitClient.get());
        mContext = context;
    }

    @Override
    public void isAuthorized(AuthRequest authRequest) {
        Call<BaseResponse<AuthorizationResponse>> call = mApiService.HasAccess(authRequest);
        call.enqueue(new Callback<BaseResponse<AuthorizationResponse>>() {
            @Override
            public void onResponse(Call<BaseResponse<AuthorizationResponse>> call, Response<BaseResponse<AuthorizationResponse>> response) {
                if (response.isSuccessful()) {
                    if (response.code() == 200) {
                        if (response.body() != null) {
                            AppLog.d(TAG, "onResponse : " + response.body());

                            BaseResponse<AuthorizationResponse> baseResponse = response.body();

                            if (baseResponse.getResponseCode() == 200) {
                                Integer authResponse = baseResponse.getResponseCode();
                                //Toast.makeText(mContext, "Success"+200, Toast.LENGTH_SHORT).show();
                                ModelApp.eventBus.post(authResponse);
                            } else {
                                AppLog.d(TAG, "Authentication Error");
                                Integer authResponse = baseResponse.getResponseCode();
                                ModelApp.eventBus.post(authResponse);
                            }
                        } else {
                            Toast.makeText(mContext, "Error1", Toast.LENGTH_SHORT).show();
                            AppLog.d(TAG, "No Response Body Error");
                            Integer authResponse = response.code();
                            ModelApp.eventBus.post(authResponse);

                        }
                    } else {
                        AppLog.d(TAG, "Response Error");
                        Integer authResponse = response.code();
                        ModelApp.eventBus.post(authResponse);
                    }

                } else {
                    Integer authResponse = response.code();
                    ModelApp.eventBus.post(authResponse);
                }

            }

            @Override
            public void onFailure(Call<BaseResponse<AuthorizationResponse>> call, Throwable t) {
                AppLog.d(TAG, "null Response");
                Integer authResponse = 500;
                ModelApp.eventBus.post(authResponse);
            }
        });
    }
}
