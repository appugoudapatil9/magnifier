package com.cygent.domain.webapi;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.util.Log;

import com.cygent.domain.R;
import com.cygent.framework.BaseApplication;
import com.cygent.framework.utils.AppLog;
import com.cygent.framework.utils.Constants;
import com.cygent.framework.utils.DateUtil;
import com.cygent.framework.utils.DialogUtils;
import com.cygent.framework.utils.FileUtils;
import com.cygent.framework.utils.network.DeleteUtils;
import com.cygent.framework.utils.network.DownLoadUtils;
import com.cygent.model.ModelApp;
import com.cygent.model.entities.database.DatabaseManager;
import com.cygent.model.entities.database.ResourceEntity;
import com.cygent.model.entities.request.AcknowledgementRequest;
import com.cygent.model.entities.request.DownloadResourceRequest;
import com.cygent.model.entities.request.GetResourceRequest;
import com.cygent.model.entities.request.UpdateResourceViewCount;
import com.cygent.model.entities.request.UpdateSyncStatus;
import com.cygent.model.entities.request.UpdateViewCountRequest;
import com.cygent.model.entities.response.AcknowledgementResponse;
import com.cygent.model.entities.response.BaseResponse;
import com.cygent.model.entities.response.ErrorMessage;
import com.cygent.model.entities.response.Folder;
import com.cygent.model.entities.response.GetResourceResponse;
import com.cygent.model.entities.response.Resource;
import com.cygent.model.entities.response.VersionCheck;
import com.cygent.model.rest.MyDownLoadTask;
import com.cygent.model.rest.RestApis;
import com.cygent.model.rest.RetrofitClient;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.orhanobut.logger.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeoutException;
import java.util.zip.*;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hsnirmal on 5/11/2016.
 */
public class ResourceServiceImpl extends GCMTokenServiceImpl implements ResourceService {
    private static final String TAG = "GetResourceServiceImpl";

    private static final String RESOURCE_GET_ERROR = "Error in getting resource list.";


    protected DatabaseManager mDbManager;
    Context mContext;
    DownLoadTask mDownLoadTask = null;
    boolean isDownloadCanceled = false;

    public ResourceServiceImpl(Context context) {
        super(RetrofitClient.get());
        mContext = context;
        mDbManager = DatabaseManager.getInstance(context);
    }

    @Override
    public void getResourceList(final GetResourceRequest requestObj) {

        Call<BaseResponse<GetResourceResponse>> call = mApiService.getResourceApi(requestObj);
        call.enqueue(new Callback<BaseResponse<GetResourceResponse>>() {
            @Override
            public void onResponse(Call<BaseResponse<GetResourceResponse>> call, Response<BaseResponse<GetResourceResponse>> response) {
                if (response.isSuccessful()) {
                    if (response.code() == 200) {
                        if (response.body() != null) {
                            AppLog.d(TAG, "onResponse : " + response.body());

                            BaseResponse<GetResourceResponse> baseResponse = response.body();

                            if (baseResponse.getResponseCode() == 200) {
                                Log.d("Res Response JSON","Res=> "+baseResponse.getResponseJSON());
                                GetResourceResponse resourceResponse = baseResponse.getResponseJSON();

                                //Download All resource and Folder Images in Advance
                                //To prevent delay in loading
                                new DownloadResourceImageAsync().execute(resourceResponse);
                                //
//                                ModelApp.eventBus.post(resourceResponse);
                            } else {
                                ErrorMessage errorMessage = new ErrorMessage();
                                errorMessage.setErrorMessage(RESOURCE_GET_ERROR);
                                ModelApp.eventBus.post(errorMessage);
                            }
                        } else {
                            AppLog.d(TAG, " Response Error");
                            ErrorMessage errorMessage = new ErrorMessage();
                            errorMessage.setErrorMessage(RESOURCE_GET_ERROR);
                            ModelApp.eventBus.post(errorMessage);
                        }
                    } else {
                        AppLog.d(TAG, "Response Error");
                        ErrorMessage errorMessage = new ErrorMessage();
                        errorMessage.setErrorMessage(RESOURCE_GET_ERROR);
                        ModelApp.eventBus.post(errorMessage);
                    }

                } else {
                    AppLog.d(TAG, "NUll Response");
                    ErrorMessage errorMessage = new ErrorMessage();
                    errorMessage.setErrorMessage(RESOURCE_GET_ERROR);
                    ModelApp.eventBus.post(errorMessage);
                }

            }

            @Override
            public void onFailure(Call<BaseResponse<GetResourceResponse>> call, Throwable t) {
                AppLog.d(TAG, "null Response");
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.setErrorMessage(RESOURCE_GET_ERROR);
                ModelApp.eventBus.post(errorMessage);
            }
        });
    }

    private class DownloadResourceImageAsync extends AsyncTask<GetResourceResponse,Void,GetResourceResponse>{

        @Override
        protected GetResourceResponse doInBackground(GetResourceResponse... params) {
            GetResourceResponse mGetResourceResponse = params[0];
            //Download Resorce and folder images in advance
            List<Resource>  resourceList = mGetResourceResponse.getResourceList();
            if(resourceList!=null && resourceList.size()>0){
                Iterator<Resource> mIterator = resourceList.iterator();
                while(mIterator.hasNext()){
                    Resource mResource = mIterator.next();
                    downlaodResourceImageToLocal(mResource);
                }
            }

            List<Folder> folderList = mGetResourceResponse.getFolderList();
            if(folderList!=null && folderList.size()>0){
                Iterator<Folder> mIterator = folderList.iterator();
                while(mIterator.hasNext()){
                    Folder mFolder = mIterator.next();
                    downlaodFolderImageToLocal(mFolder);
                }
            }
            //

            return mGetResourceResponse;
        }

        private void downlaodResourceImageToLocal(Resource mResource) {
            //here we need to download images to internal storage
            // and save path to local db for further use
            DownLoadUtils.DownloadResult result = new DownLoadUtils.DownloadResult();
            try {
                // todo change the file location/name according to your needs

                File resourceDir = new File(ModelApp.APP_INTERNAL_ROOT_FOLDER, ModelApp.IMG_FOLDER);

                resourceDir.mkdirs();

                File file = new File(resourceDir, "img_resource_"+mResource.getUserResourceID());

//            File fileTemp = new File(resourceDir, "Temp");

                if (file.exists())
                    file.delete();

                InputStream inputStream = null;
                OutputStream outputStream = null;
                URL url = new URL(mResource.getResourceImage());

                try {
                    byte[] fileReader = new byte[4096];

                    URLConnection urlConnection = url.openConnection();
                    urlConnection.connect();
                    long fileSize = urlConnection.getContentLength();

                    //Here put available storage check
                    if (FileUtils.isSpaceAvailable(mContext, fileSize)) {
                        long fileSizeDownloaded = 0;

                        inputStream = urlConnection.getInputStream();
                        outputStream = new FileOutputStream(file);
                        while (true) {
                            int read = inputStream.read(fileReader);

                            if (read == -1) {
                                break;
                            }

                            outputStream.write(fileReader, 0, read);

                            fileSizeDownloaded += read;

                            AppLog.d(TAG, "file download: " + fileSizeDownloaded + " of " + fileSize);

                        }

                        outputStream.flush();
                        mResource.setResourceImage("file://"+file.getAbsolutePath());
                    } else {
                        AppLog.d(TAG, "Storage space Error");
                    }
                }
                catch (IOException e) {
                    AppLog.d(TAG, "Download Error");
                }catch (Exception e) {
                    AppLog.d(TAG, "Download Error");
                }
                finally
                {
                    if (inputStream != null) {
                        inputStream.close();
                    }

                    if (outputStream != null) {
                        outputStream.close();
                    }
                }
            } catch (IOException e) {
                AppLog.d(TAG, "Download Error");
            }catch (Exception e){
                AppLog.d(TAG,"Download Exception"+e.getMessage());
                AppLog.d(TAG, "Download Error");
            }

            Logger.d("Downloading Images From Path: "+mResource.getResourceImage());
            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .build();
            BaseApplication.imageLoader.loadImageSync(mResource.getResourceImage(),options);
        }


        private void downlaodFolderImageToLocal(Folder mFolder) {
            //here we need to download images to internal storage
            // and save path to local db for further use
            DownLoadUtils.DownloadResult result = new DownLoadUtils.DownloadResult();
            try {
                // todo change the file location/name according to your needs

                File resourceDir = new File(ModelApp.APP_INTERNAL_ROOT_FOLDER, ModelApp.IMG_FOLDER);

                resourceDir.mkdirs();

                File file = new File(resourceDir, "img_folder_"+mFolder.getFolderID());

//            File fileTemp = new File(resourceDir, "Temp");

                if (file.exists())
                    file.delete();

                InputStream inputStream = null;
                OutputStream outputStream = null;
                URL url = new URL(mFolder.getResourceFolderImage());

                try {
                    byte[] fileReader = new byte[4096];

                    URLConnection urlConnection = url.openConnection();
                    urlConnection.connect();
                    long fileSize = urlConnection.getContentLength();

                    //Here put available storage check
                    if (FileUtils.isSpaceAvailable(mContext, fileSize)) {
                        long fileSizeDownloaded = 0;

                        inputStream = urlConnection.getInputStream();
                        outputStream = new FileOutputStream(file);
                        while (true) {
                            int read = inputStream.read(fileReader);

                            if (read == -1) {
                                break;
                            }

                            outputStream.write(fileReader, 0, read);

                            fileSizeDownloaded += read;

                            AppLog.d(TAG, "file download: " + fileSizeDownloaded + " of " + fileSize);

                        }

                        outputStream.flush();
                        mFolder.setResourceFolderImage("file://"+file.getAbsolutePath());
                    } else {
                        AppLog.d(TAG, "Storage space Error");
                    }
                }
                catch (IOException e) {
                    AppLog.d(TAG, "Download Error");
                }catch (Exception e) {
                    AppLog.d(TAG, "Download Error");
                }
                finally
                {
                    if (inputStream != null) {
                        inputStream.close();
                    }

                    if (outputStream != null) {
                        outputStream.close();
                    }
                }
            } catch (IOException e) {
                AppLog.d(TAG, "Download Error");
            }catch (Exception e){
                AppLog.d(TAG,"Download Exception"+e.getMessage());
                AppLog.d(TAG, "Download Error");
            }

            Logger.d("Downloading Images From Path: "+mFolder.getResourceFolderImage());
            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .build();
            BaseApplication.imageLoader.loadImageSync(mFolder.getResourceFolderImage(),options);
        }



        @Override
        protected void onPostExecute(GetResourceResponse getResourceResponse) {
            ModelApp.eventBus.post(getResourceResponse);
        }
    }

    @Override
    public void insertUpdateResource(List<ResourceEntity> resourceEntityList) {
        mDbManager.insertUpdateResource(resourceEntityList);
    }

    @Override
    public void getResourceListForEntity(ResourceEntity resourceEntity) {
        //Make operation using Intent Service (Data service)
//        DataService.startActionGetEntityResources(mContext,resourceEntity.getFolderId());
    }

    @Override
    public void updateSyncStatus(final ResourceEntity resourceEntity){

        final UpdateSyncStatus request = new UpdateSyncStatus();
        request.setUserID(ModelApp.preferenceGetLong(Constants.SharedPrefKeys.USER_ID, 0L));
        request.setUserResourceID(resourceEntity.getResourceId());
        request.setSyncStatus(1);

        Call<BaseResponse> call = mApiService.updateSyncStatus(request);
        Log.d("AAupdateSyncStatus--",call.request().body().getClass().getDeclaredFields().toString());
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()) {
                    if (response.code() == 200) {
                        if (response.body() != null) {
                            BaseResponse<AcknowledgementResponse> baseResponse = response.body();
                            if (baseResponse.getResponseCode() == 200) {

                                if (baseResponse.getResponseMessage().equalsIgnoreCase("Success") ) {

                                        Logger.d("Got Success in sync status now we can delete this resource");
                                        ModelApp.eventBus.post(new DeleteUtils(DeleteUtils.DELETE_SUCCESS));

                                } else {

                                    ErrorMessage errorMessage = new ErrorMessage();
                                    errorMessage.setErrorMessage(ErrorMessage.DELETE_RESOURCE_ERROR);
                                    ModelApp.eventBus.post(errorMessage);
                                }

                            } else {
                                ErrorMessage errorMessage = new ErrorMessage();
                                errorMessage.setErrorMessage(ErrorMessage.DELETE_RESOURCE_ERROR);
                                ModelApp.eventBus.post(errorMessage);
                            }
                        } else {
                            ErrorMessage errorMessage = new ErrorMessage();
                            errorMessage.setErrorMessage(ErrorMessage.DELETE_RESOURCE_ERROR);
                            ModelApp.eventBus.post(errorMessage);
                        }
                    } else {
                        ErrorMessage errorMessage = new ErrorMessage();
                        errorMessage.setErrorMessage(ErrorMessage.DELETE_RESOURCE_ERROR);
                        ModelApp.eventBus.post(errorMessage);
                    }

                } else {
                    ErrorMessage errorMessage = new ErrorMessage();
                    errorMessage.setErrorMessage(ErrorMessage.DELETE_RESOURCE_ERROR);
                    ModelApp.eventBus.post(errorMessage);
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.setErrorMessage(ErrorMessage.DELETE_RESOURCE_ERROR);
                ModelApp.eventBus.post(errorMessage);
            }
        });

    }


    @Override
    public void sendAcknowledgement(final ResourceEntity resourceEntity, final DownLoadUtils.DownloadResult result) {
        //send download acknowledgement
        final AcknowledgementRequest request = new AcknowledgementRequest();
//        request.setClientID(ModelApp.preferenceGetLong(Constants.SharedPrefKeys.CLIENT_ID, 0L));
        request.setUserID(ModelApp.preferenceGetLong(Constants.SharedPrefKeys.USER_ID, 0L));
//        request.setUsername(ModelApp.preferenceGetString(Constants.SharedPrefKeys.USER_NAME, ""));
        request.setUserResourceID(resourceEntity.getResourceId());

        Call<BaseResponse<AcknowledgementResponse>> call = mApiService.downloadAcknowledgement(request);
        call.enqueue(new Callback<BaseResponse<AcknowledgementResponse>>() {
            @Override
            public void onResponse(Call<BaseResponse<AcknowledgementResponse>> call, Response<BaseResponse<AcknowledgementResponse>> response) {


                if (response.isSuccessful()) {
                    if (response.code() == 200) {
                        if (response.body() != null) {
                            BaseResponse<AcknowledgementResponse> baseResponse = response.body();
                            if (baseResponse.getResponseCode() == 200) {

                                AcknowledgementResponse acknowledgementResponse = baseResponse.getResponseJSON();
                                if (acknowledgementResponse.getResult() != null && !(acknowledgementResponse.getResult().isEmpty())) {

                                    if (acknowledgementResponse.getResult().equals("0")) {
                                        resourceEntity.setDownloaded(true);
                                        resourceEntity.setLocalPath(result.getDownloadPath());
                                        resourceEntity.setResourceDownloadDateTime(DateUtil.getCurrentDateInFormat("dd-MM-yyyy HH:mm:ss"));
                                        //update resource
                                        mDbManager.updateResource(resourceEntity);
                                        mDbManager.updateFolderHierarchy(resourceEntity);
                                        AppLog.d(TAG, "Download completed and file is writen to disk");
                                        AppLog.d(TAG, "Database updated with resource local path");
                                        ModelApp.eventBus.post(result);

                                    } else {

                                        ErrorMessage errorMessage = new ErrorMessage();
                                        errorMessage.setErrorMessage(ErrorMessage.DOWNLOAD_ERROR);
                                        ModelApp.eventBus.post(errorMessage);
                                    }
                                } else {

                                    ErrorMessage errorMessage = new ErrorMessage();
                                    errorMessage.setErrorMessage(ErrorMessage.DOWNLOAD_ERROR);
                                    ModelApp.eventBus.post(errorMessage);
                                }

                            } else {
                                ErrorMessage errorMessage = new ErrorMessage();
                                errorMessage.setErrorMessage(ErrorMessage.DOWNLOAD_ERROR);
                                ModelApp.eventBus.post(errorMessage);
                            }
                        } else {
                            ErrorMessage errorMessage = new ErrorMessage();
                            errorMessage.setErrorMessage(ErrorMessage.DOWNLOAD_ERROR);
                            ModelApp.eventBus.post(errorMessage);
                        }
                    } else {
                        ErrorMessage errorMessage = new ErrorMessage();
                        errorMessage.setErrorMessage(ErrorMessage.DOWNLOAD_ERROR);
                        ModelApp.eventBus.post(errorMessage);
                    }

                } else {
                    ErrorMessage errorMessage = new ErrorMessage();
                    errorMessage.setErrorMessage(ErrorMessage.DOWNLOAD_ERROR);
                    ModelApp.eventBus.post(errorMessage);
                }

            }

            @Override
            public void onFailure(Call<BaseResponse<AcknowledgementResponse>> call, Throwable t) {
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.setErrorMessage(ErrorMessage.DOWNLOAD_ERROR);
                ModelApp.eventBus.post(errorMessage);
            }
        });
    }


    public void downloadApkFile(final int versionCode) {
        Call<ResponseBody> call = mApiService.downloadApkFile(RestApis.APP_UPDATE);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.code() == 200) {
                        if (response.body() != null) {
                            String folderName = "LWC";
                            String fileName = "lwc_V"+versionCode+".apk";
                            DownloadApkTask mDownLoadTask = new DownloadApkTask(mContext, response, folderName, fileName);
                            mDownLoadTask.execute();
                        } else {
                            AppLog.d(TAG, "Null Response");
                            ErrorMessage errorMessage = new ErrorMessage();
                            errorMessage.setErrorMessage(ErrorMessage.DOWNLOAD_ERROR);
                            ModelApp.eventBus.post(errorMessage);
                        }
                    } else {
                        AppLog.d(TAG, "Server Error");
                        ErrorMessage errorMessage = new ErrorMessage();
                        errorMessage.setErrorMessage(ErrorMessage.DOWNLOAD_ERROR);
                        ModelApp.eventBus.post(errorMessage);
                    }
                } else {
                    AppLog.d(TAG, "Server Error");
                    ErrorMessage errorMessage = new ErrorMessage();
                    errorMessage.setErrorMessage(ErrorMessage.DOWNLOAD_ERROR);
                    ModelApp.eventBus.post(errorMessage);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                AppLog.d(TAG, "Download Failed");
                AppLog.d(TAG, "Server Error");
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.setErrorMessage(ErrorMessage.DOWNLOAD_ERROR);
                ModelApp.eventBus.post(errorMessage);
            }
        });
    }


    public void cancelDownload(){
        isDownloadCanceled = true;
        if(mDownLoadTask!=null ){
            mDownLoadTask.cancel(true);
        }
    }


    @Override
    public void downloadResource(final ResourceEntity resourceEntity) {

        isDownloadCanceled = false;
        ErrorMessage errorMessage = new ErrorMessage();

        final DownloadResourceRequest request = new DownloadResourceRequest();
        request.setUserResourceID(resourceEntity.getResourceId());
        request.setClientID(ModelApp.preferenceGetLong(Constants.SharedPrefKeys.CLIENT_ID, 0L));
        request.setUserID(ModelApp.preferenceGetLong(Constants.SharedPrefKeys.USER_ID, 0L));
//        request.setUsername(ModelApp.preferenceGetString(Constants.SharedPrefKeys.USER_NAME, ""));

//        final String fileName = resourceEntity.getResourceId() + "" + resourceEntity.getName().substring(resourceEntity.getName().lastIndexOf("."),resourceEntity.getName().length());
        final String resourceName = resourceEntity.getName().substring(0, resourceEntity.getName().lastIndexOf("."));
        final String fileName = resourceName+resourceEntity.getName().substring(resourceEntity.getName().lastIndexOf("."),resourceEntity.getName().length());
        final String folderName = "Resources";

        Call<ResponseBody> call = mApiService.downloadFileWithDynamicUrlSync(request);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {

                if (response.isSuccessful()) {
                    if (response.code() == 200) {
                        if (response.body() != null) {
                            if(!isDownloadCanceled) {
                                mDownLoadTask = new DownLoadTask(mContext, response, resourceEntity, folderName, fileName);
                                mDownLoadTask.execute();
                            }else{
                                AppLog.d(TAG, "Download canceled");
                                ErrorMessage errorMessage = new ErrorMessage();
                                errorMessage.setErrorMessage(ErrorMessage.DOWNLOAD_CANCELED);
                                ModelApp.eventBus.post(errorMessage);
                            }
                        } else {
                            AppLog.d(TAG, "Null Response");
                            ErrorMessage errorMessage = new ErrorMessage();
                            errorMessage.setErrorMessage(ErrorMessage.DOWNLOAD_ERROR);
                            ModelApp.eventBus.post(errorMessage);
                        }
                    } else {
                        AppLog.d(TAG, "Server Error");
                        ErrorMessage errorMessage = new ErrorMessage();
                        errorMessage.setErrorMessage(ErrorMessage.DOWNLOAD_ERROR);
                        ModelApp.eventBus.post(errorMessage);
                    }
                } else {
                    AppLog.d(TAG, "Server Error");
                    ErrorMessage errorMessage = new ErrorMessage();
                    errorMessage.setErrorMessage(ErrorMessage.DOWNLOAD_ERROR);
                    ModelApp.eventBus.post(errorMessage);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

                AppLog.d(TAG, "Download Failed");
                AppLog.d(TAG, "Server Error");
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.setErrorMessage(ErrorMessage.DOWNLOAD_ERROR);
                ModelApp.eventBus.post(errorMessage);
            }
        });

    }


    public class DownLoadTask extends MyDownLoadTask{

        public Response<ResponseBody> response;
        Context mContext;
        String fileName;
        String folderName;
        ResourceEntity resourceEntity;
        ErrorMessage errorMessage;
//        ProgressDialog mProgressDialog;

        public DownLoadTask(Context mContext, Response<ResponseBody> response, ResourceEntity resourceEntity, String folderName, String fileName) {
            this.response = response;
            this.mContext = mContext;
            this.fileName = fileName;
            this.folderName = folderName;
            this.resourceEntity = resourceEntity;

            /*mProgressDialog = new ProgressDialog(mContext);
            mProgressDialog.setMessage(null);
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE,mContext.getString(android.R.string.cancel), cancelClick);*/

        }

        DialogInterface.OnClickListener cancelClick = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DownLoadTask.this.cancel(true);
                errorMessage = new ErrorMessage();
                errorMessage.setErrorMessage(ErrorMessage.DOWNLOAD_CANCELED);
                ModelApp.eventBus.post(errorMessage);
                dialog.dismiss();
            }
        };


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            DialogUtils.dismissProgressDialog();
            DialogUtils.showDownloadProgress(mContext,null,null,cancelClick);
        }

        @Override
        protected Void doInBackground(Void... params) {
            DownLoadUtils.DownloadResult result = writeResponseBodyToDisk(mContext, response.body(),ModelApp.APP_INTERNAL_ROOT_FOLDER,folderName, fileName, this);


            switch (result.getResultCode()) {
                case Constants.DOWNLOAD_SUCCESS:
//                                        mDbManager.updateResourceLocalPath(resourceEntity, result.getDownloadPath());
                    //here we got success
                    //Send acknowledgement
                    sendAcknowledgement(resourceEntity, result);
                    break;

                case Constants.DOWNLOAD_FAILURE:
                    AppLog.d(TAG, "Download Failed");
                    errorMessage = new ErrorMessage();
                    errorMessage.setErrorMessage(ErrorMessage.DOWNLOAD_FAILED);
                    break;

                case Constants.MEMORY_ERROR:
                    AppLog.d(TAG, "Download Failed low memory");
                    errorMessage = new ErrorMessage();
                    errorMessage.setErrorMessage(ErrorMessage.DOWNLOAD_MEMORY_ISSUE);
                    break;
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

//            mProgressDialog.setProgress(values[0]);
            DialogUtils.setDownloadProgress(values[0]);
        }

        public void doProgress(int arg_progress) {
            publishProgress(arg_progress);
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            /*if (mProgressDialog != null)
                mProgressDialog.dismiss();
            mProgressDialog = null;*/
            if (errorMessage != null)
                ModelApp.eventBus.post(errorMessage);
        }
    }

    class DownloadApkTask extends MyDownLoadTask{

        public Response<ResponseBody> response;
        Context mContext;
        String fileName;
        String folderName;
        ErrorMessage errorMessage;
        ProgressDialog mProgressDialog;
        DownLoadUtils.DownloadResult result;

        public DownloadApkTask(Context mContext, Response<ResponseBody> response, String folderName, String fileName) {
            this.response = response;
            this.mContext = mContext;
            this.fileName = fileName;
            this.folderName = folderName;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            DialogUtils.dismissProgressDialog();
            mProgressDialog = new ProgressDialog(mContext);
            mProgressDialog.setMessage("Downloading " + fileName);
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    cancel(true);
                    errorMessage = new ErrorMessage();
                    errorMessage.setErrorMessage(ErrorMessage.DOWNLOAD_CANCELED);
                    ModelApp.eventBus.post(errorMessage);
                    dialog.dismiss();
                }
            });
            mProgressDialog.show();
        }


        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            mProgressDialog.setProgress(values[0]);
        }

        public void doProgress(int arg_progress) {
            publishProgress(arg_progress);
        }

        @Override
        protected Void doInBackground(Void... params) {

            result = writeResponseBodyToDisk(mContext, response.body(),ModelApp.APP_EXTERNAL_ROOT_FOLDER,folderName, fileName, this);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (mProgressDialog != null)
                mProgressDialog.dismiss();
            mProgressDialog = null;
            switch (result.getResultCode()) {
                case Constants.DOWNLOAD_SUCCESS:
                    //here we got success
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(new File(result.getDownloadPath())), "application/vnd.android.package-archive");
                    mContext.startActivity(intent);

                    break;

                case Constants.DOWNLOAD_FAILURE:
                    AppLog.d(TAG, "Download Failed");
                    errorMessage = new ErrorMessage();
                    errorMessage.setErrorMessage(ErrorMessage.DOWNLOAD_FAILED);
                    if (errorMessage != null)
                        ModelApp.eventBus.post(errorMessage);
                    break;

                case Constants.MEMORY_ERROR:
                    AppLog.d(TAG, "Download Failed low memory");
                    errorMessage = new ErrorMessage();
                    errorMessage.setErrorMessage(ErrorMessage.DOWNLOAD_MEMORY_ISSUE);
                    if (errorMessage != null)
                        ModelApp.eventBus.post(errorMessage);
                    break;
            }

        }
    }

    @Override
    public boolean isResourceExist(ResourceEntity resourceEntity) {
        if (!resourceEntity.getLocalPath().isEmpty()) {
            File file = new File(resourceEntity.getLocalPath());
            if (file.exists()) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    @Override
    public void updateResourceViewCount(ResourceEntity resourceEntity) {
        mDbManager.updateResourceViewCount(resourceEntity);
    }

    @Override
    public List<ResourceEntity> getDownloadedResourceList() {
        return mDbManager.getDownloadedResourceList();
    }

    @Override
    public int deleteResource(ResourceEntity resourceEntity) {

        return mDbManager.deleteResource(resourceEntity);
    }


    @Override
    public void updateResource(ResourceEntity resourceEntity) {

        mDbManager.updateResource(resourceEntity);
    }

    public void updateFolderHierarchy(ResourceEntity resourceEntity){
        mDbManager.updateFolderHierarchy(resourceEntity);
    }

    public void updateFolderHierarchyForDownloadManager(ResourceEntity resourceEntity){
        mDbManager.updateFolderHierarchyForDownloadManager(resourceEntity);
    }

    @Override
    public void callUpdateResourceViewCount() {


        UpdateViewCountRequest request = null;
        //check if Any resource is accessed
        final List<ResourceEntity> accessedResourceList = mDbManager.getAllAccessedResources();


        if (!(accessedResourceList.isEmpty())) {
            List<UpdateResourceViewCount> mUpdateResourceViewCountList = new ArrayList<>();
            mUpdateResourceViewCountList.clear();
            for (ResourceEntity resourceEntity : accessedResourceList) {
                UpdateResourceViewCount mUpdateResourceViewCount = new UpdateResourceViewCount();
                mUpdateResourceViewCount.setLastAccessDate(DateUtil.convertDateToUTC(resourceEntity.getLastAccessDate()));
                mUpdateResourceViewCount.setNumberOfViews(resourceEntity.getNumberOfViews().toString());
                mUpdateResourceViewCount.setResourceStatus("accessed");
                mUpdateResourceViewCount.setTimeSpent(resourceEntity.getTimeSpent().toString());
                mUpdateResourceViewCount.setUserResourceID(resourceEntity.getResourceId().toString());

                mUpdateResourceViewCountList.add(mUpdateResourceViewCount);
            }
            request = new UpdateViewCountRequest();
            request.setUserID(ModelApp.preferenceGetLong(Constants.SharedPrefKeys.USER_ID, 0L));
            request.setUpdateResourceViewCount(mUpdateResourceViewCountList);
        }
        //
        if (request != null) {
            Call<BaseResponse> call = mApiService.updateResourceViewCount(request);
            call.enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    if (response.isSuccessful()) {
                        if (response.code() == 200) {
                            if (response.body() != null) {

                                BaseResponse baseResponse = response.body();
                                if (baseResponse.getResponseCode() == 200) {
                                    if (baseResponse.getResponseMessage().equalsIgnoreCase("Success")) {
                                        AppLog.d(TAG, "callUpdateResourceViewCount success");
                                        mDbManager.resetResourceViewCount(accessedResourceList);
                                        ModelApp.preferencePutLong(Constants.SharedPrefKeys.REPORT_SYNC_LONG_DATE,System.currentTimeMillis());
                                    }
                                }
                            } else {
                                AppLog.d(TAG, "callUpdateResourceViewCount failed");
                            }
                        } else {
                            AppLog.d(TAG, "callUpdateResourceViewCount failed");
                        }

                    } else {
                        AppLog.d(TAG, "callUpdateResourceViewCount failed");
                    }

                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    AppLog.d(TAG, "callUpdateResourceViewCount failed");
                }
            });
        }

    }

    @Override
    public void getAllResourceList() {

//        DataService.startActionFetchResources(mContext);
    }


    // int 3 error
    // 1 success
    // 2 memory problem
    public static DownLoadUtils.DownloadResult writeResponseBodyToDisk(Context context, ResponseBody body,File rootFolder ,String folderName, String fileName, MyDownLoadTask asyncTask) {
        DownLoadUtils.DownloadResult result = new DownLoadUtils.DownloadResult();
        String extension = "";
        try {
            // todo change the file location/name according to your needs

            File resourceDir = new File(rootFolder, folderName);

            resourceDir.mkdirs();

            File file = new File(resourceDir, fileName);

//            File fileTemp = new File(resourceDir, "Temp");

            int i = fileName.lastIndexOf('.');
            if (i >= 0) {
                extension = fileName.substring(i+1);
            }

            if (file.exists())
                file.delete();

            InputStream inputStream = null;
            OutputStream outputStream = null;


            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();

                //Here put available storage check
                if (FileUtils.isSpaceAvailable(context, fileSize)) {
                    long fileSizeDownloaded = 0;

                    inputStream = body.byteStream();
                    outputStream = new FileOutputStream(file);
                    while (true) {
                        int read = inputStream.read(fileReader);

                        if (read == -1) {
                            break;
                        }

                        outputStream.write(fileReader, 0, read);

                        fileSizeDownloaded += read;

                        //Here count progress percentage
                        int downloadPercentage = (int) (((float) fileSizeDownloaded / fileSize) * 100);
                        asyncTask.doProgress(downloadPercentage);
                        //

                        AppLog.d(TAG, "file download: " + fileSizeDownloaded + " of " + fileSize);

                        if (asyncTask.isCancelled()) {
                            if (inputStream != null) {
                                inputStream.close();
                            }

                            if (outputStream != null) {
                                outputStream.close();
                            }


                            AppLog.d(TAG, "Download Error");
                            result.setDownloadPath("");
                            result.setResultCode(2);
                            return result;

                        }
                    }

                    outputStream.flush();
                    result.setDownloadPath(file.getAbsolutePath());
                    result.setResultCode(1);

                    //unzipping file
                   // unzip(fileName, resourceDir.getAbsolutePath());

                } else {
                    AppLog.d(TAG, "Storage space Error");
                    result.setDownloadPath("");
                    result.setResultCode(3);
                }
                return result;
            }
            catch (IOException e) {
                AppLog.d(TAG, "Download Error");
                result.setDownloadPath("");
                result.setResultCode(2);
                return result;
            }catch (Exception e) {
                AppLog.d(TAG, "Download Error");
                result.setDownloadPath("");
                result.setResultCode(2);
                return result;
            }
            finally
            {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            AppLog.d(TAG, "Download Error");
            result.setDownloadPath("");
            result.setResultCode(2);
            return result;
        }catch (Exception e){
            AppLog.d(TAG,"Download Exception"+e.getMessage());
            AppLog.d(TAG, "Download Error");
            result.setDownloadPath("");
            result.setResultCode(2);
            return result;
        }

    }


    public void checkForAppUpdate() {
        Call<VersionCheck> call = mApiService.getVersionInfo(RestApis.VERSION_CHECK);
        call.enqueue(new Callback<VersionCheck>() {
            @Override
            public void onResponse(Call<VersionCheck> call, Response<VersionCheck> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        VersionCheck mVersionCheck = response.body();
                        AppLog.d(TAG, "Version Check Resposne : " + mVersionCheck);
                        ModelApp.eventBus.post(mVersionCheck);
                    }
                }
            }

            @Override
            public void onFailure(Call<VersionCheck> call, Throwable t) {

            }
        });
    }
}


