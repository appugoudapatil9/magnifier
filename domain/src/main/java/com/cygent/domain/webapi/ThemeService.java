package com.cygent.domain.webapi;

/**
 * Created by hsnirmal on 6/2/2016.
 */
public interface ThemeService {

    public void getAppTheme(Long clientId);
}
