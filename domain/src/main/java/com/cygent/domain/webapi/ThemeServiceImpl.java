package com.cygent.domain.webapi;

import com.cygent.framework.utils.AppLog;
import com.cygent.model.ModelApp;
import com.cygent.model.entities.request.GetThemeRequest;
import com.cygent.model.entities.request.LwActivateRequest;
import com.cygent.model.entities.response.BaseResponse;
import com.cygent.model.entities.response.DeviceTokenResponse;
import com.cygent.model.entities.response.ErrorMessage;
import com.cygent.model.entities.response.ThemeCustomizeSetting;
import com.cygent.model.rest.RestApis;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hsnirmal on 6/2/2016.
 */
public class ThemeServiceImpl implements ThemeService{
    public static final String TAG = ThemeServiceImpl.class.getSimpleName();

    protected RestApis mApiService;

    public ThemeServiceImpl(RestApis aApiService)
    {
        mApiService = aApiService;
    }


    @Override
    public void getAppTheme(Long clientId)
    {
        Call<BaseResponse<ThemeCustomizeSetting>> call = mApiService.getThemeCustomization(new GetThemeRequest(clientId));
        call.enqueue(new Callback<BaseResponse<ThemeCustomizeSetting>>() {
            @Override
            public void onResponse(Call<BaseResponse<ThemeCustomizeSetting>> call, Response<BaseResponse<ThemeCustomizeSetting>> response) {

                if(response.body()!=null)
                {
                    BaseResponse<ThemeCustomizeSetting> baseResponse = response.body();
                    AppLog.d(TAG,"onResponse : success"+baseResponse);
                    if(baseResponse.getResponseCode() == 200)
                    {
                        ThemeCustomizeSetting themeCustomizeSetting = baseResponse.getResponseJSON();
                        ModelApp.eventBus.post(themeCustomizeSetting);
                    }else{
                        AppLog.d(TAG,"onResponse : error message :"+baseResponse.getResponseMessage());
                        ErrorMessage errorMessage = new ErrorMessage();
                        errorMessage.setErrorMessage(ErrorMessage.THEME_ERROR);
                        ModelApp.eventBus.post(errorMessage);
                    }
                }else{
                    AppLog.d(TAG,"onResponse : error message : null response");
                    ErrorMessage errorMessage = new ErrorMessage();
                    errorMessage.setErrorMessage(ErrorMessage.THEME_ERROR);
                    ModelApp.eventBus.post(errorMessage);
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<ThemeCustomizeSetting>> call, Throwable t) {
                AppLog.d(TAG,"On Failure : "+t.getMessage());

                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.setErrorMessage(ErrorMessage.THEME_ERROR);
                ModelApp.eventBus.post(errorMessage);
            }
        });
    }
}
