package com.cygent.domain.webapi;

import android.content.Context;
import android.widget.Toast;

import com.cygent.framework.utils.AppLog;
import com.cygent.model.ModelApp;
import com.cygent.model.entities.request.AppVersionRequest;
import com.cygent.model.rest.RetrofitClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppVersionServiceImpl extends GCMTokenServiceImpl implements AppVersionService{

    private static final String TAG = AppVersionServiceImpl.class.getSimpleName().toString();

    Context mContext;

    public AppVersionServiceImpl(Context context) {
        super(RetrofitClient.get());
        mContext = context;
    }

    @Override
    public void updateAppVersion(AppVersionRequest appVersionRequest) {
        Call<Boolean> call = mApiService.updateAndroidAppVersion(appVersionRequest);

        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                    if (response.code() == 200) {
                        if (response.body() != null) {
                            AppLog.d(TAG, "onResponse : " + response.body());
                            Boolean appVersionResponse = response.body();
                            ModelApp.eventBus.post(appVersionResponse);
                        } else {
                            Toast.makeText(mContext, "Error1", Toast.LENGTH_SHORT).show();
                            AppLog.d(TAG, "No Response Body Error");
                            Boolean appVersionResponse = response.body();
                            ModelApp.eventBus.post(appVersionResponse);
                        }
                    } else {
                        AppLog.d(TAG, "Response Error");
                        Boolean appVersionResponse = response.body();
                        ModelApp.eventBus.post(appVersionResponse);
                    }

                } else {
                    Boolean appVersionResponse = response.body();
                    ModelApp.eventBus.post(appVersionResponse);
                }

            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                AppLog.d(TAG, "null Response");
                Boolean appVersionResponse = false;
                ModelApp.eventBus.post(appVersionResponse);
            }
        });

    }
}
