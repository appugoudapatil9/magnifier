package com.cygent.domain.webapi;

import com.cygent.model.entities.request.AppVersionRequest;

public interface AppVersionService {

    void updateAppVersion(AppVersionRequest appVersionRequest);
}
