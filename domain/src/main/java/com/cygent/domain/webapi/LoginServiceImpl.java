package com.cygent.domain.webapi;

import com.cygent.framework.utils.AppLog;
import com.cygent.model.ModelApp;
import com.cygent.model.entities.request.LwActivateRequest;
import com.cygent.model.entities.response.BaseResponse;
import com.cygent.model.entities.response.ErrorMessage;
import com.cygent.model.entities.response.LoginResponse;
import com.cygent.model.rest.RetrofitClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by hsnirmal on 5/9/2016.
 */
public class LoginServiceImpl extends GCMTokenServiceImpl implements LoginService {
    private static String TAG = "LoginServiceImpl";



    public LoginServiceImpl() {
        super(RetrofitClient.get());
    }

    @Override

    public void doLogin(String pin) {

        Call<BaseResponse<LoginResponse>> call = mApiService.loginApi(new LwActivateRequest(pin));
        call.enqueue(new Callback<BaseResponse<LoginResponse>>() {
            @Override
            public void onResponse(Call<BaseResponse<LoginResponse>> call, Response<BaseResponse<LoginResponse>> response) {

                if(response.isSuccessful())
                {
                    if(response.code() == 200)
                    {
                        if (response.body() != null) {
                            BaseResponse<LoginResponse> baseResponse = response.body();
                            AppLog.d(TAG, "onResponse : response code" + baseResponse.getResponseCode());
                            if(baseResponse.getResponseCode() == 200){
                                LoginResponse loginResponse = baseResponse.getResponseJSON();
                                if(loginResponse!=null)
                                {
                                    AppLog.d(TAG, "onSuccess : " + loginResponse);
                                    ModelApp.eventBus.post(loginResponse);
                                }else{
                                    AppLog.d(TAG,"Null Response");
                                    ErrorMessage errorMessage = new ErrorMessage();
                                    errorMessage.setErrorMessage(ErrorMessage.LOGIN_ERROR);
                                    ModelApp.eventBus.post(errorMessage);
                                }
                            }else{
                                AppLog.d(TAG, "onError : " + baseResponse.getResponseCode());
                                ErrorMessage errorMessage = new ErrorMessage();
                                errorMessage.setErrorMessage(ErrorMessage.LOGIN_ERROR);
                                ModelApp.eventBus.post(errorMessage);
                            }
                        } else {
                            AppLog.d(TAG, "Null Response");
                            AppLog.d(TAG, "onError : Null response");
                            ErrorMessage errorMessage = new ErrorMessage();
                            errorMessage.setErrorMessage(ErrorMessage.LOGIN_ERROR);
                            ModelApp.eventBus.post(errorMessage);
                        }
                    }else{
                        AppLog.d(TAG, "onError : Server error");
                        ErrorMessage errorMessage = new ErrorMessage();
                        errorMessage.setErrorMessage(ErrorMessage.LOGIN_ERROR);
                        ModelApp.eventBus.post(errorMessage);
                    }
                }else{
                    AppLog.d(TAG, "onError : Server Error");
                    ErrorMessage errorMessage = new ErrorMessage();
                    errorMessage.setErrorMessage(ErrorMessage.LOGIN_ERROR);
                    ModelApp.eventBus.post(errorMessage);
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<LoginResponse>> call, Throwable t) {
                AppLog.d(TAG, "onFailure : error in log in");
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.setErrorMessage(ErrorMessage.LOGIN_ERROR);
                ModelApp.eventBus.post(errorMessage);
            }
        });

//        call.enqueue(new Callback<List<LoginResponse>>() {
//            @Override
//            public void onResponse(Call<List<LoginResponse>> call, Response<List<LoginResponse>> response) {
//
//
//                if (response.body() != null && response.body().size() > 0) {
//                    AppLog.d(TAG, "onResponse : " + response.body().get(0));
//
//                    LoginResponse mResponse = response.body().get(0);
//
//
//                    BaseResponse<LoginResponse> baseResponse = new BaseResponse<LoginResponse>();
//                    baseResponse.setStatus(0);
//                    baseResponse.setErrorCode(null);
//                    baseResponse.setArrayResponseJson(response.body());
//
//                    ModelApp.eventBus.post(baseResponse);
//                } else {
//                    AppLog.d(TAG, "Empty Response");
//                    BaseResponse<LoginResponse> baseResponse = new BaseResponse<LoginResponse>();
//                    baseResponse.setStatus(1);
//                    baseResponse.setErrorCode("Wrong Pin");
//                    baseResponse.setArrayResponseJson(null);
//
//                    ModelApp.eventBus.post(baseResponse);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<LoginResponse>> call, Throwable t) {
//                AppLog.d(TAG, "onFailure : " + t.getStackTrace());
//                AppLog.d(TAG, "Empty Response");
//                BaseResponse<LoginResponse> baseResponse = new BaseResponse<LoginResponse>();
//                baseResponse.setStatus(1);
//                baseResponse.setErrorCode("Wrong Pin");
//                baseResponse.setArrayResponseJson(null);
//
//                ModelApp.eventBus.post(baseResponse);
//            }
//        });
    }

}
