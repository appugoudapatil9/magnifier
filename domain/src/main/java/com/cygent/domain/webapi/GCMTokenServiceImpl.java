package com.cygent.domain.webapi;

import android.os.AsyncTask;

import com.cygent.framework.BaseApplication;
import com.cygent.framework.utils.AppLog;
import com.cygent.framework.utils.Constants;
import com.cygent.model.ModelApp;
import com.cygent.model.entities.request.DeviceTokenRequest;
import com.cygent.model.entities.response.BaseResponse;
import com.cygent.model.entities.response.DeviceTokenResponse;
import com.cygent.model.entities.response.ErrorMessage;
import com.cygent.model.entities.response.ThemeCustomizeSetting;
import com.cygent.model.rest.RestApis;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.orhanobut.logger.Logger;
import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by 1064 on 5/12/2016.
 */
public class GCMTokenServiceImpl implements GCMTokenService {
    private static final String TAG = "GCMTokenServiceImpl";
    protected RestApis mApiService;

    public GCMTokenServiceImpl(RestApis aApiService) {
        mApiService = aApiService;
    }

    @Override

    public void registerDeviceToken(final DeviceTokenRequest aDeviceToken) {
        Call<BaseResponse<DeviceTokenResponse>> call = mApiService.registerDeviceTokem(aDeviceToken);

        call.enqueue(new Callback<BaseResponse<DeviceTokenResponse>>() {
            @Override
            public void onResponse(Call<BaseResponse<DeviceTokenResponse>> call, Response<BaseResponse<DeviceTokenResponse>> response) {

                if (response.body() != null) {
                    BaseResponse<DeviceTokenResponse> baseResponse = response.body();

                    if (baseResponse.getResponseCode() == 200) {
                        DeviceTokenResponse deviceTokenResponse = baseResponse.getResponseJSON();
                        ModelApp.preferencePutString(Constants.SharedPrefKeys.GCM_TOKEN,aDeviceToken.getDeviceToken());

                        //Download theme images
                        new DownloadThemeAsync().execute(deviceTokenResponse);
                        //
//                        ModelApp.eventBus.post(deviceTokenResponse);
                    } else {
                        ErrorMessage errorMessage = new ErrorMessage();
                        errorMessage.setErrorMessage(ErrorMessage.DEVICE_TOKEN_REGISTER_ERROR);
                        ModelApp.eventBus.post(errorMessage);
                    }
                } else {
                    AppLog.d(TAG, "null Response");
                    ErrorMessage errorMessage = new ErrorMessage();
                    errorMessage.setErrorMessage(ErrorMessage.DEVICE_TOKEN_REGISTER_ERROR);
                    ModelApp.eventBus.post(errorMessage);
                }

            }

            @Override
            public void onFailure(Call<BaseResponse<DeviceTokenResponse>> call, Throwable t) {
                AppLog.d(TAG, "null Response");
                ErrorMessage errorMessage = new ErrorMessage();
                errorMessage.setErrorMessage(ErrorMessage.DEVICE_TOKEN_REGISTER_ERROR);
                ModelApp.eventBus.post(errorMessage);
            }
        });

    }

    private class DownloadThemeAsync extends AsyncTask<DeviceTokenResponse,Void,DeviceTokenResponse>{

        @Override
        protected DeviceTokenResponse doInBackground(DeviceTokenResponse... params) {
            DeviceTokenResponse mDeviceTokenResponse = params[0];
            if(mDeviceTokenResponse!=null) {
                ThemeCustomizeSetting mThemeCustomizeSetting = mDeviceTokenResponse.getIpadCustomizeSetting();
                if (mThemeCustomizeSetting != null) {
                    DisplayImageOptions options = new DisplayImageOptions.Builder()
                            .cacheInMemory(true)
                            .cacheOnDisk(true)
                            .build();
                    BaseApplication.imageLoader.loadImageSync(mThemeCustomizeSetting.getBackgroundImage(), options);
                    Logger.d("Downloading Images From Path: " + mThemeCustomizeSetting.getBackgroundImage());
                    BaseApplication.imageLoader.loadImageSync(mThemeCustomizeSetting.getPortraitImage(), options);
                    Logger.d("Downloading Images From Path: " + mThemeCustomizeSetting.getPortraitImage());
                    BaseApplication.imageLoader.loadImageSync(mThemeCustomizeSetting.getLogoImage(), options);
                    Logger.d("Downloading Images From Path: " + mThemeCustomizeSetting.getLogoImage());
                }
            }
            return mDeviceTokenResponse;
        }

        @Override
        protected void onPostExecute(DeviceTokenResponse deviceTokenResponse) {
            ModelApp.eventBus.post(deviceTokenResponse);
        }
    }
}
//        call.enqueue(new Callback<List<DeviceTokenResponse>>() {
//            @Override
//            public void onResponse(Call<List<DeviceTokenResponse>> call, Response<List<DeviceTokenResponse>> response) {
//
//
//                if (response.body() != null && response.body().size() > 0) {
//                    try {
//                        Logger.d(TAG, "onResponse : " + response.body().get(0));
//
//                        DeviceTokenResponse mResponse = response.body().get(0);
//
//                        BaseResponse<DeviceTokenResponse> baseResponse = new BaseResponse<>();
////                        baseResponse.setStatus(0);
////                        baseResponse.setErrorCode(null);
////                        baseResponse.setArrayResponseJson(response.body());
//
////                        ModelApp.eventBus.post(baseResponse.getArrayResponseJson().get(0));
//                    } catch (Throwable aThrowable) {
//                        Logger.d(TAG, aThrowable.getMessage());
//                        BaseResponse<DeviceTokenResponse> baseResponse = new BaseResponse<>();
////                        baseResponse.setStatus(1);
////                        baseResponse.setErrorCode("Device token not registered");
////                        baseResponse.setArrayResponseJson(null);
//
//                        ModelApp.eventBus.post(baseResponse);
//                    }
//                } else {
//                    Logger.d(TAG, "Empty Response");
//                    BaseResponse<DeviceTokenResponse> baseResponse = new BaseResponse<>();
////                    baseResponse.setStatus(1);
////                    baseResponse.setErrorCode("Device token not registered");
////                    baseResponse.setArrayResponseJson(null);
//
//                    ModelApp.eventBus.post(baseResponse);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<DeviceTokenResponse>> call, Throwable t) {
//                Logger.d(TAG, "onFailure : " + t.getStackTrace());
//                Logger.d(TAG, "Empty Response");
//                BaseResponse<DeviceTokenResponse> baseResponse = new BaseResponse<>();
////                baseResponse.setStatus(1);
////                baseResponse.setErrorCode("Device token not registered");
////                baseResponse.setArrayResponseJson(null);
//                ModelApp.eventBus.post(baseResponse);
//            }
//        });
//    }
