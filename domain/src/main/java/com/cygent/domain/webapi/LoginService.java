package com.cygent.domain.webapi;

/**
 * Created by hsnirmal on 5/9/2016.
 */
public interface LoginService {

    void doLogin(String pin);
}
