package com.magnifigroup.haagstreitapp.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cygent.framework.utils.AppLog;
import com.cygent.framework.utils.Constants;
import com.cygent.framework.utils.DateUtil;
import com.cygent.framework.utils.DialogUtils;
import com.cygent.framework.utils.FileUtils;
import com.cygent.framework.utils.NetworkUtils;
import com.cygent.framework.utils.network.DownLoadUtils;
import com.cygent.model.ModelApp;
import com.cygent.model.entities.database.ResourceEntity;
import com.cygent.model.entities.request.GetResourceRequest;
import com.cygent.model.entities.response.DeviceTokenResponse;
import com.cygent.model.entities.response.ErrorMessage;
import com.cygent.model.entities.response.Folder;
import com.cygent.model.entities.response.GetResourceResponse;
import com.cygent.model.entities.response.Resource;
import com.cygent.model.entities.response.ThemeCustomizeSetting;
import com.cygent.model.entities.response.VersionCheck;
import com.magnifigroup.haagstreitapp.BuildConfig;
import com.magnifigroup.haagstreitapp.HaagstreitApp;
import com.magnifigroup.haagstreitapp.R;
import com.magnifigroup.haagstreitapp.adapter.NavigationAdapter;
import com.magnifigroup.haagstreitapp.adapter.ViewPagerAdapter;
import com.magnifigroup.haagstreitapp.controls.CircleImageView;
import com.magnifigroup.haagstreitapp.controls.CirclePageIndicator;
import com.magnifigroup.haagstreitapp.fragment.GridlayoutFragment;
import com.magnifigroup.haagstreitapp.gcm.RegistrationIntentService;
import com.magnifigroup.haagstreitapp.mvp.presenters.GetResourcesPresenter;
import com.magnifigroup.haagstreitapp.utils.AppConstants;
import com.magnifigroup.haagstreitapp.utils.CommonUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import dataservice.DataResultReceiver;
import dataservice.DataService;


public class MainActivity extends AppBaseActivity implements View.OnClickListener, Filterable, DataResultReceiver.Listener {

    //    List<ResourceEntity> mAllResourceEntityList;
    List<ResourceEntity> mResourceEntitiesList;
    private boolean mIsToShowDisclaimer;

    private static final String TAG = MainActivity.class.getSimpleName();
    private GetResourcesPresenter mGetResourcesPresenter;
    //    private ThemePresenter mThemePresenter;
    private ViewHolder mViewHolder;

    private boolean mIsFromLogin;
    private boolean mIsResourceDownloaded;

    ResourceEntity baseEntity = null;
    ResourceEntity clickedEntity = null;
    List<ResourceEntity> mDataList;
    List<ResourceEntity> mOriginalList;
    ResourceFilter mFilter;
    ViewPagerAdapter adapterViewpager;
    int elementParPage;

    List<ResourceEntity> mDataListNavigation;

    NavigationAdapter mNavigationAdapter;
    ResourceEntity downloadedResourceEntity;
    boolean isFromConfigChange = false;
    View.OnClickListener mOnClickListener;
    DataResultReceiver resultReceiver = null;


    //Document Open Date Time
    String openDate;
    //Document close date
    String closeDate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mViewHolder = new ViewHolder(this);
        mBaseViewHolder = mViewHolder;
        mOnClickListener = this;
        mViewHolder.setListeners();

        //Service listener
        resultReceiver = new DataResultReceiver(new Handler());
        resultReceiver.setListener(this);
        //


        mIsToShowDisclaimer = true;

        mGetResourcesPresenter = new GetResourcesPresenter(this);
//        mThemePresenter = new ThemePresenter(this);
        setUpToolbar(mViewHolder.mToolbar);
        mIsFromLogin = getIntent().getBooleanExtra(AppConstants.BundleKeys.IS_FROM_LOGIN, false);

        registeDeviceToken();

        AppLog.d(TAG, "GCM Token : " + ModelApp.preferenceGetString(Constants.SharedPrefKeys.GCM_TOKEN, null));


        mDataListNavigation = new ArrayList<>();

        mNavigationAdapter = new NavigationAdapter(this, mDataListNavigation, this);

        mViewHolder.mRecyclerNavigation.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        mViewHolder.mRecyclerNavigation.setAdapter(mNavigationAdapter);


        //check config and orientation

        Configuration config = getResources().getConfiguration();

//        mAllResourceEntityList = new ArrayList<>();
        mDataList = new ArrayList<>();
        mOriginalList = new ArrayList<>();
        adapterViewpager = new ViewPagerAdapter(getSupportFragmentManager(), mDataList, this);
        mViewHolder.mViewPager.setAdapter(adapterViewpager);
        mViewHolder.mViewPager.setOffscreenPageLimit(0);

        // Checks the orientation of the screen
        if (config.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Log.d(TAG, "Orientation : Landscape");
            Log.d(TAG, "Number of elements for page : " + CommonUtils.getNumberOfElementsParPage(this) * 3);
            elementParPage = CommonUtils.getNumberOfElementsParPage(this) * 3;
//            initPages(elementParPage);
        } else if (config.orientation == Configuration.ORIENTATION_PORTRAIT) {
            Log.d(TAG, "Orientation : portrait");
            Log.d(TAG, "Number of elements for page : " + CommonUtils.getNumberOfElementsParPage(this) * 2);
            elementParPage = CommonUtils.getNumberOfElementsParPage(this) * 2;
//            initPages(elementParPage);
        }


        mViewHolder.mPagerIndicators.setFillColor(getResources().getColor(R.color.colorPrimaryDark));
        mViewHolder.mPagerIndicators.setStrokeColor(getResources().getColor(R.color.colorPrimaryDark));
        mViewHolder.mPagerIndicators.setViewPager(mViewHolder.mViewPager);

        mViewHolder.addTextWatcher();

        //Set up app theme
        mViewHolder.setUpAppTheme();


        //Get resource list by calling api
        getResourceList();

        showPermissionDialog();

        updateResourceViewCount();
    }

    private void registeDeviceToken() {
        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            if (NetworkUtils.getConnectivityStatus(MainActivity.this)) {
                Intent intent = new Intent(this, RegistrationIntentService.class);
                startService(intent);
            } else {
                DialogUtils.showErrorAlert(MainActivity.this, getString(R.string.error_alert), getString(R.string.login_error_network_error), getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mViewHolder.setUpAppTheme();
//        adapterViewpager.notifyDataSetChanged();
        AppLog.d(TAG, "onResume called");
//        notifyViewPager();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if (mDataListNavigation.size() > 1) {
            goToPreviousResourceEntity();
        } else {
            super.onBackPressed();
        }
    }

    private void updateResourceViewCount() {
        long preDate = ModelApp.preferenceGetLong(Constants.SharedPrefKeys.REPORT_SYNC_LONG_DATE, 0L);
        long currDate = System.currentTimeMillis();
        if (preDate != 0L) {
            int hours = DateUtil.getDateDifferenceInHour(preDate, currDate);
            if (hours > 24) {
                mGetResourcesPresenter.callUpdateResourceViewCount();
            }
        }
    }


    private void getResourceList() {
        if (mIsFromLogin && !mIsResourceDownloaded) {
            if (NetworkUtils.getConnectivityStatus(MainActivity.this)) {
                GetResourceRequest getResourceRequest = CommonUtils.getResourceRequest();
                if (getResourceRequest != null) {
                    DialogUtils.showProgressDialog(MainActivity.this, getString(R.string.downloading_resources), ProgressDialog.STYLE_SPINNER);
//                    showProgress(mViewHolder.mProgressBarPanel);
                    mGetResourcesPresenter.getResourceList(getResourceRequest);
                }
            } else {
                DialogUtils.showErrorAlert(MainActivity.this, getString(R.string.error_alert), getString(R.string.login_error_network_error), getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
            }
        } else {
            initBaseEntity();
//            initPages(elementParPage);
            getAllResources();
        }

    }

    public void getAllResources() {
        DialogUtils.showProgressDialog(this, getString(R.string.fetching_resources), ProgressDialog.STYLE_SPINNER);
        DataService.startActionFetchResources(MainActivity.this, resultReceiver);
//        mGetResourcesPresenter.getAllResourceList();
    }

    private void getUpdatedResources() {
        if (NetworkUtils.getConnectivityStatus(MainActivity.this)) {
            GetResourceRequest getResourceRequest = CommonUtils.getResourceRequest();
            if (getResourceRequest != null) {
                DialogUtils.showProgressDialog(MainActivity.this, getString(R.string.refreshing_resources), ProgressDialog.STYLE_SPINNER);
//                    showProgress(mViewHolder.mProgressBarPanel);
                mGetResourcesPresenter.getResourceList(getResourceRequest);
                //Check for App Update
                mGetResourcesPresenter.checkForAppUpdate();
            }
        } else {
            DialogUtils.showErrorAlert(MainActivity.this, getString(R.string.error_alert), getString(R.string.login_error_network_error), getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        AppLog.d(TAG, "onStart called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        AppLog.d(TAG, "onPause called");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        AppLog.d(TAG, "onRestart called");
    }

    @Override
    public void onStop() {
        super.onStop();
        AppLog.d(TAG, "onStop called");
    }

    @Override
    public void applicationWillEnterForeground() {
        super.applicationWillEnterForeground();
        if (mIsToShowDisclaimer) {
            showDisclaimerDialog(this);
        }
        mIsToShowDisclaimer = true;
    }


    @Subscribe
    public void onEvent(DeviceTokenResponse response) {
        //Set Device theme and DeviceToken pref
        ModelApp.preferencePutBoolean(Constants.SharedPrefKeys.GCM_TOKEN_REGISTERED, true);

        if (response.getIsThemeUpdateRequire()) {
            //update or save current theme
            AppLog.d(TAG, " Theme Response : " + response.getIpadCustomizeSetting());
            CommonUtils.saveAppTheme(response.getIpadCustomizeSetting());
            CommonUtils.saveAppDisclaimer(response.getDeviceTokenResponse());
            adapterViewpager.notifyDataSetChanged();
        }
//        ModelApp.preferencePutBoolean(Constants.SharedPrefKeys.IS_THEME_UPDATE_REQUIRED, response.getIsThemeUpdateRequire());
        mViewHolder.setUpAppTheme();
    }

    @Subscribe
    public void onEvent(final VersionCheck response) {

        //Check version code if update avilable then propt user to
        //get update
        if (BuildConfig.VERSION_CODE < response.getVersionCode()) {
            AppLog.d(TAG, "App Update is available");
            //App update available
            DialogUtils.showOptionDialog(MainActivity.this, getString(R.string.error_alert), getString(R.string.app_update_message), getString(R.string.update_option), getString(R.string.update_cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //Positive click
                    dialog.dismiss();
                    DialogUtils.showProgressDialog(MainActivity.this, getString(R.string.progress_pls_wait), ProgressDialog.STYLE_SPINNER);
                    mGetResourcesPresenter.downloadApkFile(response.getVersionCode());
                }
            }, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //on Cancel click
                    dialog.dismiss();
                }
            });
        } else {
            AppLog.d(TAG, "U r using latest app version");
        }

    }

    @Subscribe
    public void onEvent(ThemeCustomizeSetting themeResponse) {
        //Apply theme here
        mViewHolder.setUpAppTheme();
        //
    }

    @Subscribe
    public void onEvent(DownLoadUtils.DownloadResult result) {
        DialogUtils.dismissProgressDialog();
        DialogUtils.hideDownloadProgress();

        if (downloadedResourceEntity != null && mGetResourcesPresenter.isResourceExist(downloadedResourceEntity)) {
            mIsToShowDisclaimer = false;
            openDate = DateUtil.getCurrentDateInFormat("dd-MM-yyyy HH:mm:ss");
            openResourceEntity(downloadedResourceEntity);

            //here update that resource object in global array list
            updateAllResourceList();
            //
        }

        adapterViewpager.notifyDataSetChanged();
//        GridlayoutFragment gridlayoutFragment = adapterViewpager.getCurrentFragment(mViewHolder.mViewPager.getId(), mViewHolder.mViewPager.getCurrentItem());
//        if (gridlayoutFragment != null)
//            gridlayoutFragment.notifyAdapter(mDataList.get(mViewHolder.mViewPager.getCurrentItem()).getResourceEntityList());

     /*   DialogUtils.showErrorAlert(MainActivity.this,getString(R.string.error_alert), "Download completed" ,getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });*/

    }

    private void updateAllResourceList() {
        if (HaagstreitApp.allResourceEntityList.contains(downloadedResourceEntity)) {
            try {
                int position = HaagstreitApp.allResourceEntityList.indexOf(downloadedResourceEntity);
                HaagstreitApp.allResourceEntityList.set(position, downloadedResourceEntity);
            } catch (IndexOutOfBoundsException ex) {
                AppLog.d(TAG, ex.getMessage());
            }
        }
    }

    @Subscribe
    public void onEvent(GetResourceResponse response) {
//        DialogUtils.dismissProgressDialog();
//        applicationWillEnterForeground();
//        hideProgress(mViewHolder.mProgressBarPanel);
        //Do database operation here

        mResourceEntitiesList = new ArrayList<>();

        List<Folder> mFolderList = response.getFolderList();
        List<Resource> mResourceList = response.getResourceList();

        for (Folder mFolder : mFolderList) {
            ResourceEntity mResourceEntity = new ResourceEntity();
            mResourceEntity.setFolderId(mFolder.getFolderID());
            mResourceEntity.setResourceId(0L);
            mResourceEntity.setName(mFolder.getFolderName());
            mResourceEntity.setParentFolderId(mFolder.getParentFolderID());
            mResourceEntity.setSeqNumber(mFolder.getSequence());
            mResourceEntity.setResourceCount(mFolder.getResourcesCount());
            mResourceEntity.setIsResource(false);
            mResourceEntity.setNumberOfViews(0);
            mResourceEntity.setSendViaMail(false);
            mResourceEntity.setImagePath(mFolder.getResourceFolderImage());
            mResourceEntity.setLocalPath("");
            mResourceEntity.setServerPath("");
            mResourceEntity.setResourceType("");
            mResourceEntity.setSyncStatus(0);
            mResourceEntity.setUserStatus(0);
            mResourceEntity.setDownloaded(false);
            mResourceEntity.setResourceHierarchy("");
            mResourceEntity.setLastAccessDate("");
            mResourceEntity.setTimeSpent(0L);

            mResourceEntitiesList.add(mResourceEntity);
        }

        for (Resource mResource : mResourceList) {
            ResourceEntity mResourceEntity = new ResourceEntity();
            mResourceEntity.setFolderId(0L);
            mResourceEntity.setResourceId(mResource.getUserResourceID());
            mResourceEntity.setName(mResource.getResourceName());
            mResourceEntity.setParentFolderId(mResource.getResourceFolderID());
            mResourceEntity.setSeqNumber(mResource.getResourceSequence());
            mResourceEntity.setResourceCount(0);
            mResourceEntity.setIsResource(true);
            mResourceEntity.setNumberOfViews(0);
            mResourceEntity.setSendViaMail(mResource.getIsSendViaMail());
            mResourceEntity.setImagePath(mResource.getResourceImage());
            mResourceEntity.setLocalPath("");
            mResourceEntity.setServerPath(mResource.getResourcePath());
            mResourceEntity.setResourceType(mResource.getResourceType());
            mResourceEntity.setSyncStatus(mResource.getSyncStatus());
            mResourceEntity.setUserStatus(mResource.getUserStatus());
            mResourceEntity.setDownloaded(false);
            mResourceEntity.setResourceHierarchy(mResource.getResourceFolderHierarchy());
            mResourceEntity.setLastAccessDate("");
            mResourceEntity.setTimeSpent(0L);

            //Set file size for resources
            if (mResource.getResourceSizeInKB() != null && !(mResource.getResourceSizeInKB().isEmpty())) {
                mResourceEntity.setResourceSizeInKB(Double.valueOf(mResource.getResourceSizeInKB()).longValue());
                String fileSize = mResource.getResourceSizeInKB();
                String formattedFileSize = FileUtils.formatFileSize(Double.valueOf(fileSize).longValue());
                mResourceEntity.setResourceSize(formattedFileSize);
            } else {
                mResourceEntity.setResourceSize(null);
                mResourceEntity.setResourceSizeInKB(0L);
            }
            mResourceEntitiesList.add(mResourceEntity);
        }

        if (mIsFromLogin && !mIsResourceDownloaded) {
            mGetResourcesPresenter.insertUpdateResource(mResourceEntitiesList);
            mIsResourceDownloaded = true;
        } else {
            if (mResourceList != null && mResourceList.size() > 0) {
                Resource resourceEntityToStatusCheck = mResourceList.get(0);
                int userStatus = resourceEntityToStatusCheck.getUserStatus();

                if (userStatus == 0) {//activated
                    mGetResourcesPresenter.insertUpdateResource(mResourceEntitiesList);
                    mIsResourceDownloaded = true;
                } else if (userStatus == 1) {//deactivated
                    DialogUtils.showErrorAlert(MainActivity.this, getString(R.string.error_alert), ErrorMessage.USER_DEACTIVATED, getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                } else if (userStatus == 2) {//expired
                    DialogUtils.showErrorAlert(MainActivity.this, getString(R.string.error_alert), ErrorMessage.USER_EXPIRED, getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                }
            }
        }

        goToRootFolder();
        DialogUtils.dismissProgressDialog();
        applicationWillEnterForeground();
        //Call here View count update api
        mGetResourcesPresenter.callUpdateResourceViewCount();
        //
//        Toast.makeText(MainActivity.this, "Resource list downloaded", Toast.LENGTH_SHORT).show();
    }

    private void goToRootFolder() {
        mDataListNavigation.clear();
        baseEntity = null;
        initBaseEntity();
        getAllResources();
    }

    @Subscribe
    public void onEvent(ErrorMessage errorResponse) {
        mIsResourceDownloaded = false;
        DialogUtils.dismissProgressDialog();
        DialogUtils.hideDownloadProgress();
        DialogUtils.showErrorAlert(MainActivity.this, getString(R.string.error_alert), errorResponse.getErrorMessage(), getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
//        hideProgress(mViewHolder.mProgressBarPanel);
        //Do database operation here
//        Toast.makeText(MainActivity.this, "Resource list downloaded", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onClick(View v) {
        ResourceEntity clickedResource;

        switch (v.getId()) {
            case R.id.txt_header:

//                Toast.makeText(this, "Got click from list ", Toast.LENGTH_SHORT).show();
                int position = (int) v.getTag();

                if (position != 0 && position != (mDataListNavigation.size() - 1)) {
                    baseEntity = mDataListNavigation.get(position);

                    int i = mDataListNavigation.size() - 1;
                    while (i >= position) {

                        if (mDataListNavigation != null && !mDataListNavigation.isEmpty()) {
                            mDataListNavigation.remove(i);
                        }

                        i--;
                    }
                    mNavigationAdapter.notifyDataSetChanged();

                    mViewHolder.mViewPager.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.out));
                    initBaseEntity();
//                    initPages(elementParPage);

//                    if(mDataListNavigation.size() <= 1)
//                    {
//                        mViewHolder.mImgIcBack.setVisibility(View.GONE);
//                    }
                }
                break;
            case R.id.imgFileFolder:
                clickedResource = (ResourceEntity) v.getTag();
                goToNextResourceEntity(clickedResource);
                break;

            case R.id.btn_accept:

                break;

            case R.id.img_downloaded:
//                Toast.makeText(MainActivity.this, "Got click for DM", Toast.LENGTH_SHORT).show();
                mIsToShowDisclaimer = false;
                Intent intent = new Intent(MainActivity.this, DownloadedResourceActivity.class);
                startActivityForResult(intent, AppConstants.IntentKeys.DOWNLOAD_MANAGER_REQ_CODE);
                break;

            case R.id.img_refresh:
//                getResourceList();
//                showDisclaimerDialog(null);
                if (NetworkUtils.getConnectivityStatus(MainActivity.this)) {
                    registeDeviceToken();
                    getUpdatedResources();
                } else {
                    DialogUtils.showErrorAlert(MainActivity.this, getString(R.string.error_alert), getString(R.string.login_error_network_error), getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                }
                break;
            case R.id.ic_back:
              /*  try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }*/
                goToPreviousResourceEntity();

                break;

            case R.id.ll_res_desc:
                clickedResource = (ResourceEntity) v.getTag();
                goToNextResourceEntity(clickedResource);
                break;

            case R.id.img_logo:
                if (mDataListNavigation != null && mDataListNavigation.size() > 1) {
                    goToRootFolder();
                }
                break;

        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == AppConstants.IntentKeys.DOWNLOAD_MANAGER_REQ_CODE) {
            // Make sure the request was successful
            if (resultCode == AppConstants.IntentKeys.DOWNLOAD_MANAGER_RESULT_UPDATE_SUCCESS) {
                //Some update has been made in database
                //Goto root folder
                goToRootFolder();
            }
        }

        if (requestCode == AppConstants.IntentKeys.OPEN_IMAGE_RESOURCE_REQ) {
            // Make sure the request was successful
            if (resultCode == AppConstants.IntentKeys.OPEN_IMAGE_RESOURCE_RESULT_OK) {
                //Some update has been made in database
                //Goto root folder
                clickedEntity.setTimeSpent(clickedEntity.getTimeSpent() + getTimeSpent());
                mGetResourcesPresenter.updateResource(clickedEntity);
            }
        }

        if (requestCode == AppConstants.IntentKeys.OPEN_VIDEO_RESOURCE_REQ) {
            // Make sure the request was successful
            if (resultCode == AppConstants.IntentKeys.OPEN_VIDEO_RESOURCE_RESULT_OK) {
                //Some update has been made in database
                //Goto root folder
                clickedEntity.setTimeSpent(clickedEntity.getTimeSpent() + getTimeSpent());
                mGetResourcesPresenter.updateResource(clickedEntity);
            }
        }
    }

    private long getTimeSpent() {

        AppLog.d(TAG, "Clicked Entity : " + clickedEntity);

        closeDate = DateUtil.getCurrentDateInFormat(AppConstants.DATE_FORMAT_LAST_ACCESS_DATE);
        String lastAccessDate = clickedEntity.getLastAccessDate();
        long diffInSecond = 0L;
        try {
            Date fromDate = DateUtil.convertStrToDate(lastAccessDate, AppConstants.DATE_FORMAT_LAST_ACCESS_DATE);
            Date toDate = DateUtil.convertStrToDate(closeDate, AppConstants.DATE_FORMAT_LAST_ACCESS_DATE);
            diffInSecond = DateUtil.getDateDifferenceInSec(fromDate, toDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return diffInSecond;
    }


    private void goToNextResourceEntity(ResourceEntity clikedResourceEntity) {

        mViewHolder.mEdtSearch.setText("");
        clickedEntity = clikedResourceEntity;
        //  Toast.makeText(this, "On Click :" + clikedResourceEntity.getName() + "  Model ID : " + clikedResourceEntity.getFolderId(), Toast.LENGTH_SHORT).show();

        if (!clikedResourceEntity.getIsResource()) {
//            clikedResourceEntity.setResourceEntityList(mGetResourcesPresenter.getResourceListForEntity(clikedResourceEntity));
            baseEntity = clikedResourceEntity;
            initBaseEntity();
//                    mViewHolder.mImgIcBack.setVisibility(View.VISIBLE);\


//            initPages(elementParPage);
        } else {
            // Toast.makeText(MainActivity.this, "Clicked item is resource", Toast.LENGTH_SHORT).show();
            if (mGetResourcesPresenter.isResourceExist(clikedResourceEntity)) {
                mIsToShowDisclaimer = false;
                openResourceEntity(clikedResourceEntity);
            } else {
                //Go and download it and open
                if (NetworkUtils.getConnectivityStatus(MainActivity.this)) {

//                    if(FileUtils.isSpaceAvailable(MainActivity.this,clikedResourceEntity.getResourceSizeInKB())){
//                        DialogUtils.showProgressDialog(MainActivity.this, "Please wait...", ProgressDialog.STYLE_SPINNER);
                    downloadedResourceEntity = clikedResourceEntity;
                    DialogUtils.showDownloadProgress(MainActivity.this, String.format(getString(R.string.downloading_resource), downloadedResourceEntity.getName()), null, downloadCancelListener);
                    mGetResourcesPresenter.downloadResource(clikedResourceEntity);
//                        new DownloadTask().execute(clikedResourceEntity);
//                    }else{
//                        DialogUtils.showErrorAlert(MainActivity.this, getString(R.string.error_alert), getString(R.string.memory_error), getString(android.R.string.ok), new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//                            }
//                        });
//                    }
                } else {
                    DialogUtils.showErrorAlert(MainActivity.this, getString(R.string.error_alert), getString(R.string.login_error_network_error), getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                }
            }
            //Here write code to download and open resource
            //first check if file exist and downloaded
        }
    }


    DialogInterface.OnClickListener downloadCancelListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            mGetResourcesPresenter.cancelDonwload();
            DialogUtils.showProgressDialog(MainActivity.this, getString(R.string.pls_wait), ProgressDialog.STYLE_SPINNER);
        }
    };

/*    private void goToPreviousResourceEntity()
    {
        if(mDataListNavigation.size() > 1) {
            mViewHolder.mEdtSearch.setText("");
            baseEntity = mDataListNavigation.get(mDataListNavigation.size() - 2);

            int listSizeNedded = mDataListNavigation.size() - 2;
            while (mDataListNavigation.size() > listSizeNedded) {
                mDataListNavigation.remove(mDataListNavigation.size() - 1);
            }
            mNavigationAdapter.notifyDataSetChanged();

//                if(mDataListNavigation.size() <= 1)
//                {
//                    mViewHolder.mImgIcBack.setVisibility(View.GONE);
//                }
            mViewHolder.mViewPager.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.out));
            initPages(elementParPage);
        }
    }*/

    private void goToPreviousResourceEntity() {
        if (mDataListNavigation.size() > 1) {
            mViewHolder.mEdtSearch.setText("");
            baseEntity = mDataListNavigation.get(mDataListNavigation.size() - 2);

//            int listSizeNedded = mDataListNavigation.size() - 2;
//            while (mDataListNavigation.size() > listSizeNedded) {
//                mDataListNavigation.remove(mDataListNavigation.size() - 1);
//            }

            int listSizeNedded = mDataListNavigation.size() - 2;
            int listSize = mDataListNavigation.size();
            while (listSize > listSizeNedded) {
                mDataListNavigation.remove(listSize - 1);
                listSize--;
            }

            mNavigationAdapter.notifyDataSetChanged();

//                if(mDataListNavigation.size() <= 1)
//                {
//                    mViewHolder.mImgIcBack.setVisibility(View.GONE);
//                }
            mViewHolder.mViewPager.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.out));

            initBaseEntity();

//            initPages(elementParPage);
        }
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null)
            mFilter = new ResourceFilter();

        return mFilter;
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        switch (resultCode) {
            case DataResultReceiver.RESULT_FETCH_RESOURCE:
                DialogUtils.dismissProgressDialog();
                ResourceEntity resourceEntity1 = (ResourceEntity) resultData.getParcelable(DataResultReceiver.RESULT_OBJECT);
                HaagstreitApp.allResourceEntityList.clear();
                HaagstreitApp.allResourceEntityList.addAll(resourceEntity1.getResourceEntityList());
                mFilter = null;

                break;

            case DataResultReceiver.RESULT_GET_ENTITY_RESOURCES:
                DialogUtils.dismissProgressDialog();
                ResourceEntity resourceEntity2 = (ResourceEntity) resultData.getParcelable(DataResultReceiver.RESULT_OBJECT);
                baseEntity.setResourceEntityList(resourceEntity2.getResourceEntityList());
//                baseEntity.setResourceCount(resourceEntity2.getResourceEntityList().size());
                mViewHolder.mViewPager.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.in));
                initPages(elementParPage);
                break;
        }

    }


    private class ResourceFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            FilterResults filterResults = new FilterResults();
            ArrayList<ResourceEntity> refList = new ArrayList<ResourceEntity>();
            refList.clear();
            refList.addAll(HaagstreitApp.allResourceEntityList);

            if (charSequence != null && charSequence.toString().length() > 0) {
                List<ResourceEntity> tempList = new ArrayList<ResourceEntity>();
                for (ResourceEntity mTempModel : refList) {
                    if (mTempModel.getName() != null && !mTempModel.getName().isEmpty()) {
                        Log.d(TAG, "Filter By FileModel Name : true");
                        Log.d(TAG, "Search FileModel Name : "
                                + mTempModel.getName().toUpperCase(Locale.ENGLISH));
                        Log.d(TAG, "Search For Char Seq Value :"
                                + charSequence.toString().toUpperCase(Locale.ENGLISH));
                        if (mTempModel
                                .getName()
                                .toUpperCase(Locale.ENGLISH)
                                .contains(
                                        charSequence.toString()
                                                .toUpperCase(Locale.ENGLISH))) {
                            if (!tempList.contains(mTempModel)) {
                                tempList.add(mTempModel);
                            }
                        }
                    }
                }


                filterResults.values = tempList;
                filterResults.count = tempList.size();

            } else {
                filterResults.values = mOriginalList;
                filterResults.count = mOriginalList.size();
            }
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            if (results.count == 0) {
                Log.d(TAG, "No Result count is 0");
                mDataList.clear();
                adapterViewpager.notifyDataSetChanged();
//                notifyViewPager();
            } else {
                mDataList.clear();
                mDataList.addAll(ResourceEntity.getFragmentedList((ArrayList<ResourceEntity>) results.values, elementParPage));
                AppLog.d(TAG, "Get Result");
                AppLog.d(TAG, "Result List :" + mDataList);
                adapterViewpager.notifyDataSetChanged();
//                notifyViewPager();
            }
        }
    }

    public class ViewHolder extends BaseViewHolder implements View.OnClickListener {
        @BindView(R.id.toolbar)
        Toolbar mToolbar;
        @BindView(R.id.progressBarPanel)
        LinearLayout mProgressBarPanel;
        @BindView(R.id.viewPager)
        ViewPager mViewPager;
        @BindView(R.id.recyclerView_navigation)
        RecyclerView mRecyclerNavigation;
        @BindView(R.id.pagerIndicators)
        CirclePageIndicator mPagerIndicators;
        @BindView(R.id.edt_search)
        EditText mEdtSearch;
        @BindView(R.id.img_downloaded)
        ImageView mImgDownloadManager;
        @BindView(R.id.img_background)
        ImageView mImgBackground;
        @BindView(R.id.img_logo)
        CircleImageView mImgLogo;
        @BindView(R.id.img_refresh)
        ImageView mImgRefresh;
        @BindView(R.id.ic_back)
        ImageView mImgIcBack;
        @BindView(R.id.txt_empty_view)
        TextView mTxtEmptyView;

        ViewHolder(AppCompatActivity aAppCompatActivity) {
            ButterKnife.bind(this, aAppCompatActivity);
        }

        public void addTextWatcher() {
            mEdtSearch.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                    // When user changed the Text
//                MainActivity.this.adapter.getFilter().filter(cs);\
                    AppLog.d(TAG, "Current viewPager : " + mViewHolder.mViewPager.getCurrentItem());
                   /* GridlayoutFragment gridlayoutFragment = adapterViewpager.getCurrentFragment(mViewHolder.mViewPager.getId(), mViewHolder.mViewPager.getCurrentItem());
                    if (gridlayoutFragment != null)
                        gridlayoutFragment.performFilter(cs.toString());*/
                    getFilter().filter(cs.toString());
                }

                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                              int arg3) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void afterTextChanged(Editable arg0) {
                    // TODO Auto-generated method stub
                }
            });
        }

        public void setListeners() {
            mImgDownloadManager.setOnClickListener(MainActivity.this);
            mImgRefresh.setOnClickListener(MainActivity.this);
            mImgIcBack.setOnClickListener(MainActivity.this);
            mImgLogo.setOnClickListener(MainActivity.this);
        }


   /*     public void setUpAppTheme() {
            if (ModelApp.preferenceGetBoolen(Constants.SharedPrefKeys.IS_THEME_SAVED, false)) {
                setHeaderColor(mToolbar);
                loadBackGroundImg(mImgBackground);
                loadLogoImg(mImgLogo);
            }
        }*/

        @Override
        public void setUpAppTheme() {
            if (ModelApp.preferenceGetBoolen(Constants.SharedPrefKeys.IS_THEME_SAVED, false)) {
                setHeaderColor(mToolbar);
                loadBackGroundImg(mImgBackground);
                loadLogoImg(mImgLogo);
                adapterViewpager.notifyDataSetChanged();
                mNavigationAdapter.notifyDataSetChanged();
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
               /* case R.id.ic_back:
                    mOnClickListener.onClick(v);
                    break;
                case R.id.img_logo:
                    goToRootFolder();
                    break;*/
            }
        }
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Log.d(TAG, "OnConfig Changed called");

        Log.d(TAG, "Number Of Elements :" + CommonUtils.getNumberOfElementsParPage(this));

        mViewHolder.setUpAppTheme();

//        mDataListNavigation.clear();
        if (mDataListNavigation.size() > 0) {
            mDataListNavigation.remove(mDataListNavigation.size() - 1);
        }

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Log.d(TAG, "Orientation : Landscape");
            Log.d(TAG, "Number of elements for page : " + CommonUtils.getNumberOfElementsParPage(this) * 3);
            elementParPage = CommonUtils.getNumberOfElementsParPage(this) * 3;
            if (baseEntity != null)
                initPages(elementParPage);

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            Log.d(TAG, "Orientation : portrait");
            Log.d(TAG, "Number of elements for page : " + CommonUtils.getNumberOfElementsParPage(this) * 2);
            elementParPage = CommonUtils.getNumberOfElementsParPage(this) * 2;
            if (baseEntity != null)
                initPages(elementParPage);
        }

    }

    public void initBaseEntity() {
        //Init Base Folder
        //This is base Folder
        if (baseEntity == null) {
            baseEntity = new ResourceEntity();
            baseEntity.setFolderId(0L);
            baseEntity.setName("");
            baseEntity.setParentFolderId(0L);
        }
        //for resource count database query is needed
//        List<ResourceEntity> resourceEntityList = mGetResourcesPresenter.getResourceListForEntity(baseEntity);

        DialogUtils.showProgressDialog(MainActivity.this, "Fetching resources...", ProgressDialog.STYLE_SPINNER);

        DataService.startActionGetEntityResources(MainActivity.this, baseEntity.getFolderId(), resultReceiver);
        //mGetResourcesPresenter.getResourceListForEntity(baseEntity);
        //
    }


    public void initPages(int elementsParPage) {

//        mViewHolder.mViewPager.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.in));
        mDataListNavigation.add(baseEntity);
        mNavigationAdapter.notifyDataSetChanged();

        if (mDataListNavigation.size() > 1) {
            mViewHolder.mImgIcBack.setVisibility(View.VISIBLE);
        } else {
            mViewHolder.mImgIcBack.setVisibility(View.GONE);
        }

        //this list will used for filtering
        mOriginalList.clear();
        mOriginalList.addAll(baseEntity.getResourceEntityList());

        //here we have elements par pages
        mDataList.clear();
        mDataList.addAll(ResourceEntity.getFragmentedList(baseEntity, elementsParPage));


        adapterViewpager.notifyDataSetChanged();
        mViewHolder.mViewPager.setOffscreenPageLimit(mDataList.size());
//        adapterViewpager = new ViewPagerAdapter(getSupportFragmentManager(), mDataList, this);
//        mViewHolder.mViewPager.setAdapter(adapterViewpager);

    /*    GridlayoutFragment gridlayoutFragment = adapterViewpager.getCurrentFragment(mViewHolder.mViewPager.getId(), mViewHolder.mViewPager.getCurrentItem());
        if (gridlayoutFragment!=null && mDataList!=null && !(mDataList.isEmpty()))
            gridlayoutFragment.notifyAdapter(mDataList.get(mViewHolder.mViewPager.getCurrentItem()).getResourceEntityList());*/


//        notifyViewPager();

        mViewHolder.mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

//                adapterViewpager.notifyDataSetChanged();

                /*GridlayoutFragment gridlayoutFragment = adapterViewpager.getCurrentFragment(mViewHolder.mViewPager.getId(), mViewHolder.mViewPager.getCurrentItem());
                if (gridlayoutFragment != null && mDataList!=null && !(mDataList.isEmpty()))
                    gridlayoutFragment.notifyAdapter(mDataList.get(mViewHolder.mViewPager.getCurrentItem()).getResourceEntityList());*/

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        setEmptyView();
    }

    private void setEmptyView() {
        if (mDataList.size() == 0) {
            //set Empty view Here
            mViewHolder.mTxtEmptyView.setVisibility(View.VISIBLE);
        } else {
            mViewHolder.mTxtEmptyView.setVisibility(View.GONE);
        }
    }

    private void notifyViewPager() {
        mViewHolder.mViewPager.getAdapter().notifyDataSetChanged();
        GridlayoutFragment gridlayoutFragment = adapterViewpager.getCurrentFragment(mViewHolder.mViewPager.getId(), mViewHolder.mViewPager.getCurrentItem());
        if (gridlayoutFragment != null && mDataList != null && !(mDataList.isEmpty()))
            gridlayoutFragment.notifyAdapter(mDataList.get(mViewHolder.mViewPager.getCurrentItem()).getResourceEntityList());
    }

    class DownloadTask extends AsyncTask<ResourceEntity, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Show progress dialog here
        }

        @Override
        protected Void doInBackground(ResourceEntity... params) {

            mGetResourcesPresenter.downloadResource(params[0]);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //close progress dialog here
            //Open downloaded file
        }
    }

}
