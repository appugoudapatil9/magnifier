package com.magnifigroup.haagstreitapp.fragment;


import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.cygent.framework.utils.AppLog;
import com.cygent.model.entities.database.ResourceEntity;
import com.magnifigroup.haagstreitapp.R;
import com.magnifigroup.haagstreitapp.activity.MainActivity;
import com.magnifigroup.haagstreitapp.adapter.GridViewAdapter;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link GridlayoutFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class GridlayoutFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//    private static final String ARG_PARAM1 = "param1";
//    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
//    private String mParam1;
//    private String mParam2;

    GridView gridview;
    GridViewAdapter adapter;

    private List<ResourceEntity> mDataList = new ArrayList<>();
    private List<ResourceEntity> mOriginalList = new ArrayList<>();
    private View.OnClickListener mOnclickListener;
    public int fragmentPosition;
    private View view;

    public GridlayoutFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment GridlayoutFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GridlayoutFragment newInstance(View.OnClickListener argOnClickListener, int position, List<ResourceEntity> aDataList) {
        GridlayoutFragment fragment = new GridlayoutFragment();
        fragment.mOnclickListener = argOnClickListener;
        fragment.fragmentPosition = position;
        fragment.mDataList.addAll(aDataList);
        fragment.mOriginalList.addAll(aDataList);
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AppLog.d("GridlayoutFragment OnCreate View :","Called");
        if(view == null ) {
            view = inflater.inflate(R.layout.fragment_gridlayout, container, false);
            // Inflate the layout for this fragment

//        mDataList = FileModel.getData(10);
            gridview = (GridView) view.findViewById(R.id.gridview);
            adapter = new GridViewAdapter(getActivity(), mDataList, mOriginalList, mOnclickListener);
            View emptyView = inflater.inflate(R.layout.empty_view,null,false);
            container.addView(emptyView);
            gridview.setEmptyView(emptyView);
            gridview.setAdapter(adapter);
            gridview.setOnTouchListener(new View.OnTouchListener(){
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    InputMethodManager inputMethodManager =(InputMethodManager)v.getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    return true;
                }
            });
        }
        return view;
    }

    @Override
    public void onDestroyView() {
        if(view !=null && view.getParent() !=null){
            ((ViewGroup)view.getParent()).removeAllViews();
        }
        super.onDestroyView();
    }

    public void notifyAdapter(List<ResourceEntity> fileModelList) {
        AppLog.d("GridlayoutFragment :","notifyAdapter Called");
        if (fileModelList != null) {
            mDataList.clear();
            mDataList.addAll(fileModelList);
            mOriginalList.clear();
            mOriginalList.addAll(fileModelList);
        } else {
            mDataList.clear();
            mOriginalList.clear();
        }
        if (adapter != null) adapter.notifyDataSetChanged();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        AppLog.d("GridlayoutFragment setUserVisibleHint :",""+this.isVisible());
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            gridview.setNumColumns(3);
        }else {
            gridview.setNumColumns(2);
        }
        super.onConfigurationChanged(newConfig);
    }


    /* public void performFilter(String filterTxt)
    {
        if (TextUtils.isEmpty(filterTxt.toString())) {
            adapter.getFilter().filter("");
            adapter.notifyDataSetChanged();
        } else {
            adapter.getFilter().filter(filterTxt.toString());
            adapter.notifyDataSetChanged();
        }
    }*/

}
