package com.magnifigroup.haagstreitapp;

import com.crashlytics.android.Crashlytics;
import com.cygent.framework.utils.AppLog;
import com.cygent.model.ModelApp;
import com.cygent.model.entities.database.ResourceEntity;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.orhanobut.logger.AndroidLogTool;
import com.orhanobut.logger.LogLevel;
import com.orhanobut.logger.Logger;

import io.fabric.sdk.android.Fabric;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hsnirmal on 5/9/2016.
 */
public class HaagstreitApp extends ModelApp {

    private static final String TAG = "HaagstreitApp";

    public static boolean isTab = false;


    public static List<ResourceEntity> allResourceEntityList = new ArrayList<>();
//    public static ImageLoader imageLoader = null;


    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        isTab = getResources().getBoolean(R.bool.isTab);
        AppLog.d(TAG, " isTAB : " + isTab);

        Logger
                .init("Haagstreit App")                 // default PRETTYLOGGER or use just init()
                .methodCount(6)                 // default 2
                .hideThreadInfo()               // default shown
                .logLevel(LogLevel.FULL)        // default LogLevel.FULL
                .methodOffset(2)                // default 0
                .logTool(new AndroidLogTool());


        // Device density
        float density = getResources().getDisplayMetrics().density;
        AppLog.d(TAG,"density is : "+density);
        //

        //Image Loader Init
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
        .build();
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);
        //
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        if(level==TRIM_MEMORY_RUNNING_LOW){
            System.gc();
        }
    }
}
