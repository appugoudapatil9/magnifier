package com.magnifigroup.haagstreitapp.mvp.presenters;

import com.cygent.domain.webapi.LoginServiceImpl;

/**
 * Created by hsnirmal on 5/9/2016.
 */
public class LoginPresenter extends GCMPresenter {

    private LoginServiceImpl loginService;

    public LoginPresenter() {
        loginService = new LoginServiceImpl();
        setGCMTokenService(loginService);
    }

    public void doLogin(String pin) {
        loginService.doLogin(pin);
    }

}
