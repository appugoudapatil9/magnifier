package com.magnifigroup.haagstreitapp.mvp.presenters;

import android.content.Context;

import com.cygent.domain.webapi.ResourceServiceImpl;
import com.cygent.domain.webapi.ThemeService;
import com.cygent.domain.webapi.ThemeServiceImpl;
import com.cygent.model.rest.RetrofitClient;

import retrofit2.Retrofit;

/**
 * Created by hsnirmal on 6/2/2016.
 */
public class ThemePresenter {

    private ThemeService themeService;
    private Context mContext;

    public ThemePresenter(Context context)
    {
        themeService = new ThemeServiceImpl(RetrofitClient.get());
    }

    public void getAppTheme(Long clientId)
    {
        themeService.getAppTheme(clientId);
    }

}
