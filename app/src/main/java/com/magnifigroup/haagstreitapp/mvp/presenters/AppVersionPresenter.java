package com.magnifigroup.haagstreitapp.mvp.presenters;

import android.content.Context;

import com.cygent.domain.webapi.AppVersionServiceImpl;
import com.cygent.model.entities.request.AppVersionRequest;

public class AppVersionPresenter extends GCMPresenter {

    private AppVersionServiceImpl appVersionServiceImpl;

    public AppVersionPresenter(Context context) {
        appVersionServiceImpl = new AppVersionServiceImpl(context);
    }

    public void updateAppVersion(AppVersionRequest appVersionRequest){
        appVersionServiceImpl.updateAppVersion(appVersionRequest);
    }
}
