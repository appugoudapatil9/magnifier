package com.magnifigroup.haagstreitapp.mvp.presenters;

import android.content.Context;

import com.cygent.domain.webapi.ResourceServiceImpl;
import com.cygent.model.entities.database.ResourceEntity;
import com.cygent.model.entities.request.AcknowledgementRequest;
import com.cygent.model.entities.request.GetResourceRequest;
import com.cygent.model.entities.request.UpdateViewCountRequest;

import java.util.List;

/**
 * Created by hsnirmal on 5/11/2016.
 */
public class GetResourcesPresenter extends GCMPresenter {
    private ResourceServiceImpl resourceService;

    public GetResourcesPresenter(Context context) {
        resourceService = new ResourceServiceImpl(context);
        setGCMTokenService(resourceService);
    }

    public void getResourceList(GetResourceRequest requestObj) {
        resourceService.getResourceList(requestObj);
    }

    @Override
    public void registerDeviceToken(String deviceToken) {
        super.registerDeviceToken(deviceToken);
    }

    public void insertUpdateResource(List<ResourceEntity> argList) {
        resourceService.insertUpdateResource(argList);
    }

    public void getResourceListForEntity(ResourceEntity resourceEntity)
    {
         resourceService.getResourceListForEntity(resourceEntity);
    }

    public boolean isResourceExist(ResourceEntity resourceEntity)
    {
        return  resourceService.isResourceExist(resourceEntity);
    }

    public void downloadResource(ResourceEntity resourceEntity) {
        resourceService.downloadResource(resourceEntity);
    }

    public void cancelDonwload(){
        resourceService.cancelDownload();
    }

    public void updateResourceViewCount(ResourceEntity resourceEntity)
    {
        resourceService.updateResourceViewCount(resourceEntity);
    }

    public List<ResourceEntity> getDownloadedResourceList()
    {
        return resourceService.getDownloadedResourceList();
    }

    public int deleteResource(ResourceEntity resourceEntity)
    {
        return resourceService.deleteResource(resourceEntity);
    }

    public void updateResource(ResourceEntity resourceEntity)
    {
         resourceService.updateResource(resourceEntity);
    }

    public void updateFolderHierarchy(ResourceEntity resourceEntity){
        resourceService.updateFolderHierarchy(resourceEntity);
    }

    public void updateFolderHierarchyForDownloadManager(ResourceEntity resourceEntity){
        resourceService.updateFolderHierarchyForDownloadManager(resourceEntity);
    }

    public void getAllResourceList(){
        resourceService.getAllResourceList();
    }

    public void callUpdateResourceViewCount()
    {
        resourceService.callUpdateResourceViewCount();
    }

    public void checkForAppUpdate()
    {
        resourceService.checkForAppUpdate();
    }

    public void downloadApkFile(int versionCode){resourceService.downloadApkFile(versionCode);}

    public void updateSyncStatusToDeleteResource(ResourceEntity argResourceEntity){
        resourceService.updateSyncStatus(argResourceEntity);
    }

}
