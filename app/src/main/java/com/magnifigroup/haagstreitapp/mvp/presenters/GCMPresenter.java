package com.magnifigroup.haagstreitapp.mvp.presenters;

import com.cygent.domain.webapi.GCMTokenServiceImpl;
import com.cygent.framework.utils.Constants;
import com.cygent.model.ModelApp;
import com.cygent.model.entities.request.DeviceTokenRequest;
import com.orhanobut.logger.Logger;

/**
 * Created by 1064 on 5/12/2016.
 */
public class GCMPresenter {
    private GCMTokenServiceImpl mGCMTokenService;


    public void setGCMTokenService(GCMTokenServiceImpl aGCMTokenService) {
        mGCMTokenService = aGCMTokenService;
    }

    /**
     * Created by Dev 1064 on 5/12/2016
     * Modified by Dev 1064 on 5/12/2016
     * <p/>
     * To register device token with device id to get push notification
     *
     * @param deviceToken device token registered
     */
    public void registerDeviceToken(String deviceToken) {
        if (deviceToken == null) {
            return;
        }

        Logger.d("Device Token: "+deviceToken);
        final long clientId = ModelApp.preferenceGetLong(Constants.SharedPrefKeys.CLIENT_ID, 0L);
        final long userId = ModelApp.preferenceGetLong(Constants.SharedPrefKeys.USER_ID, 0L);
        final String userName = ModelApp.preferenceGetString(Constants.SharedPrefKeys.USER_NAME, null);
        final int themeVersion = ModelApp.preferenceGetInteger(Constants.SharedPrefKeys.THEME_VERSION,0);

        if (clientId != 0) {
//            if (!ModelApp.preferenceGetBoolen(Constants.SharedPrefKeys.GCM_TOKEN_REGISTERED, false) || !deviceToken.equals(ModelApp.preferenceGetString(Constants.SharedPrefKeys.GCM_TOKEN, null))) {
                DeviceTokenRequest deviceTokenRequest = new DeviceTokenRequest();
                deviceTokenRequest.setDeviceToken(deviceToken);
                deviceTokenRequest.setClientID(clientId);
                deviceTokenRequest.setUserID(userId);
//                deviceTokenRequest.setUsername(userName);
                deviceTokenRequest.setThemeVersion(themeVersion);
                mGCMTokenService.registerDeviceToken(deviceTokenRequest);
//            }
        } else {
            ModelApp.preferencePutString(Constants.SharedPrefKeys.GCM_TOKEN, deviceToken);
            ModelApp.preferencePutBoolean(Constants.SharedPrefKeys.GCM_TOKEN_REGISTERED, false);
        }
    }

}
