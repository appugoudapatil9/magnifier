package com.magnifigroup.haagstreitapp.mvp.presenters;

/**
 * Created by hrdudhat on 1/8/2016.
 * <p/>
 * This interface is used to register EventBus for Activity or Fragment or any UI view
 */
public interface Presenter {

    /**
     * Created by Dev hrdudhat on 1/08/2016
     * Modified by Dev hrdudhat on 1/08/2016
     * <p/>
     * To register Eventbus to trigger callback method once any task is completed
     * <p/>
     * Eventbus should be register in onResume() or onStart() of activity based on the need. And in fragment it should be registered in onCreateView() or onAttach().
     */
    void registerBus();

    /**
     * Created by Dev hrdudhat on 1/08/2016
     * Modified by Dev hrdudhat on 1/08/2016
     * <p/>
     * Eventbus should be unregistered in
     * Activity's lifecycle method based on when it is registered:
     * 1) onPause() if it is registered in onResume().
     * 2) onStop() if it is registered in onStart() .
     * <p/>
     * Fragment's lifecycle method based on when it is registered:
     * 1) onDestroyView() if it is registered in onCreateView().
     * 2) onDetach() if it is registered in onAttach() .
     */
    void unRegisterBus();
}
