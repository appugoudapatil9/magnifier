package com.magnifigroup.haagstreitapp.mvp.presenters;


import android.content.Context;

import com.cygent.domain.webapi.AuthServiceImpl;
import com.cygent.model.entities.request.AuthRequest;
import com.cygent.model.entities.request.GetResourceRequest;

/**
 * Created by appupatil on 15/10/18.
 */

public class AuthPresenter extends GCMPresenter {

    private AuthServiceImpl authServiceImpl;

    public AuthPresenter(Context context) {
        authServiceImpl = new AuthServiceImpl(context);
    }

    public void isAuthorized(AuthRequest authRequest) {
        authServiceImpl.isAuthorized(authRequest);
    }

}
