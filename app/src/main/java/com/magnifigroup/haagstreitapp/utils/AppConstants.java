package com.magnifigroup.haagstreitapp.utils;

/**
 * Created by 1064 on 5/13/2016.
 * <p/>
 * This contains all constants value for app module
 */
public class AppConstants {

    public static final String DATE_FORMAT_24 = "dd-MM-yyyy HH:mm:ss";
    public static final String DATE_FORMAT_LAST_ACCESS_DATE = "yyyy-MM-dd hh:mm:ss";
    public static final String DATE_FORMAT_TO_DISPLAY = "MMM d, yyyy";
    public static final String TIME_FORMAT_TO_DISPLAY = "hh:mm a";
    public static final String DATE_FORMAT_ON_HOME = "dd-MM-yyyy";

    public static final String ACTION_THEME_CHANGED = "ACTION_THEME_CHANGED";

    public static class BundleKeys {
        public static final String IS_FROM_LOGIN = "is_from_login";
        public static final String IS_APP_LAUNCH = "is_app_launch";

    }

    public static class IntentKeys{

        public static final int DOWNLOAD_MANAGER_REQ_CODE = 1001;
        public static final int DOWNLOAD_MANAGER_RESULT_UPDATE_SUCCESS = 1002;
        public static final int DOWNLOAD_MANAGER_RESULT_UPDATE_CANCEL = 1003;

        public static final int OPEN_IMAGE_RESOURCE_REQ = 1004;
        public static final int OPEN_IMAGE_RESOURCE_RESULT_OK = 1005;

        public static final int OPEN_VIDEO_RESOURCE_REQ = 1006;
        public static final int OPEN_VIDEO_RESOURCE_RESULT_OK = 1007;

    }

    public static class NotificationKeys{
        public static final String NOTIFICATION_TYPE = "NotificationType";
        public static final String NOTIFICATION_MESSAGE = "NotificationMessage";
        public static final String LOGO_IMAGE = "LogoImage";
        public static final String VERSION_NUMBER = "VersionNumber";
        public static final String HEADER_COLOR = "HeaderColor";
        public static final String FONT_COLOR = "FontColor";
        public static final String BACKGROUND_IMAGE = "BackgroundImage";

    }

}
