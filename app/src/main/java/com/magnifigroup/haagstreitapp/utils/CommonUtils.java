package com.magnifigroup.haagstreitapp.utils;

import android.content.Context;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.util.TypedValue;

import com.cygent.domain.webapi.ResourceServiceImpl;
import com.cygent.framework.utils.AppLog;
import com.cygent.framework.utils.Constants;
import com.cygent.framework.utils.FileUtils;
import com.cygent.framework.utils.network.DownLoadUtils;
import com.cygent.model.ModelApp;
import com.cygent.model.entities.request.AppVersionRequest;
import com.cygent.model.entities.request.AuthRequest;
import com.cygent.model.entities.request.GetResourceRequest;
import com.cygent.model.entities.response.Disclaimer;
import com.cygent.model.entities.response.ThemeCustomizeSetting;
import com.magnifigroup.haagstreitapp.BuildConfig;
import com.magnifigroup.haagstreitapp.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.ResponseBody;

/**
 * Created by 1064 on 5/12/2016.
 */
public class CommonUtils {

    public static final String TAG = CommonUtils.class.getSimpleName();

    /**
     * Created by Dev 1064 on 5/12/2016
     * Modified by Dev 1064 on 5/12/2016
     * <p/>
     * To prepare request for get Resource list api
     */
    public static GetResourceRequest getResourceRequest() {
        long clientId = ModelApp.preferenceGetLong(Constants.SharedPrefKeys.CLIENT_ID, 0L);
        long userId = ModelApp.preferenceGetLong(Constants.SharedPrefKeys.USER_ID, 0L);
        String userName = ModelApp.preferenceGetString(Constants.SharedPrefKeys.USER_NAME, null);
        if ( userId != 0L && clientId!=0L) {
            GetResourceRequest request = new GetResourceRequest();
            request.setClientID(clientId);
            request.setUserID(userId);
            return request;
        }
        return null;
    }

    public static int getNumberOfElementsParPage(Context context) {

        AppLog.d(TAG, "Action bar height :" + getActionBarSize(context));
        AppLog.d(TAG, "Status bar height :" + getStatusBarHeight(context));
        int statusbarHeightPx = getStatusBarHeight(context);
        int actionBarheightPx = getActionBarSize(context);

        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
//        float dpHeight = (displayMetrics.heightPixels-(statusbarHeightPx+(actionBarheightPx*2)))/ displayMetrics.density;
        float dpHeight = (displayMetrics.heightPixels - (statusbarHeightPx + (actionBarheightPx * 2)
                + context.getResources().getDimensionPixelSize(R.dimen.navbar_height) + (context.getResources().getDimensionPixelSize(R.dimen.margin_container)) * 2));
        int numberOfElements = (int) (dpHeight / context.getResources().getDimensionPixelSize(R.dimen.row_item));

        AppLog.d(TAG, "Device Height :" + dpHeight);
        AppLog.d(TAG, "NumberOfElements :" + numberOfElements);

        return (int) (numberOfElements);
//        int height = displayMetrics.heightPixels;

//        itemsPerRow = dpWidth / cellWidthDp;
    }

    public static float getActionBarSizeInDp(Context context) {
        int actionBarHeight = 0;
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        TypedValue tv = new TypedValue();
        if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, context.getResources().getDisplayMetrics());
            actionBarHeight = (int) (actionBarHeight / displayMetrics.density);
        }
        return actionBarHeight;
    }

    public static int getActionBarSize(Context context) {
        int actionBarHeight = 0;
        TypedValue tv = new TypedValue();
        if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, context.getResources().getDisplayMetrics());
        }
        return actionBarHeight;
    }

    public static int getStatusBarHeight(Context context) {
        int statusBarHeight = 0;
        int resource = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resource > 0) {
            statusBarHeight = context.getResources().getDimensionPixelSize(resource);
        }
        return statusBarHeight;
    }

    public static void saveAppTheme(ThemeCustomizeSetting themeCustomizeSetting) {
        ModelApp.preferencePutBoolean(Constants.SharedPrefKeys.IS_THEME_SAVED,true);
        ModelApp.preferencePutInt(Constants.SharedPrefKeys.THEME_VERSION, themeCustomizeSetting.getVersionNumber());
        ModelApp.preferencePutString(Constants.SharedPrefKeys.THEME_FONT_COLOR, themeCustomizeSetting.getFontColor());
        ModelApp.preferencePutString(Constants.SharedPrefKeys.THEME_HEADER_COLOR, themeCustomizeSetting.getHeaderColor());
        ModelApp.preferencePutString(Constants.SharedPrefKeys.THEME_IMG_BACKGROUND, themeCustomizeSetting.getBackgroundImage());
        ModelApp.preferencePutString(Constants.SharedPrefKeys.THEME_IMG_LOGO, themeCustomizeSetting.getLogoImage());
        ModelApp.preferencePutString(Constants.SharedPrefKeys.THEME_IMG_PORTRAIT, themeCustomizeSetting.getPortraitImage());
    }

    public static void saveAppDisclaimer(Disclaimer disclaimerResponse)
    {
        ModelApp.preferencePutString(Constants.SharedPrefKeys.DISCLAIMER_TEXT,disclaimerResponse.getLibraryDisclaimer());
        ModelApp.preferencePutBoolean(Constants.SharedPrefKeys.DISCLAIMER_HIDE_DATE_SIZE,true);
        ModelApp.preferencePutBoolean(Constants.SharedPrefKeys.DISCLAIMER_RIGHTS_FLAG,true);

//        ModelApp.preferencePutBoolean(Constants.SharedPrefKeys.DISCLAIMER_HIDE_DATE_SIZE,disclaimerResponse.getHasRightsforHideDateandSize());
//        ModelApp.preferencePutBoolean(Constants.SharedPrefKeys.DISCLAIMER_RIGHTS_FLAG,disclaimerResponse.getHasRightsforLibraryDisclaimer());
    }

    public static int getColorFromHex(String aHexColor) {
        try {
            return Color.parseColor(aHexColor);
        } catch (Throwable throwable) {
            return Color.parseColor("#000000");
        }
    }

    public static String hexfix(String str) {
        int v = 0, w;
        v = Integer.parseInt(str.replace("#",""), 16);
        if (str.length() == 4) {
            // in rrggbb
            // nybble colors - fix to hex colors
            //  0x00000rgb              -> 0x000r0g0b
            //  0x000r0g0b | 0x00r0g0b0 -> 0x00rrggbb
            w = ((v & 0xF00) << 8) | ((v & 0x0F0) << 4) | (v & 0x00F);
            v = w | (w << 4);
        }
        return "#"+String.format("%06x",v).toUpperCase();
    }

    public static AuthRequest getAuthRequest() {
        long clientId = ModelApp.preferenceGetLong(Constants.SharedPrefKeys.CLIENT_ID, 0L);
        long userId = ModelApp.preferenceGetLong(Constants.SharedPrefKeys.USER_ID, 0L);
        if ( userId != 0L && clientId!=0L) {
            AuthRequest request = new AuthRequest();
            request.setClientID(clientId);
            request.setUserID(userId);
            return request;
        }
        return null;
    }

    public static AppVersionRequest getAppVersionRequest() {
        long userId = ModelApp.preferenceGetLong(Constants.SharedPrefKeys.USER_ID, 0L);
        String androidVersion = BuildConfig.VERSION_NAME;
        if ( userId != 0L && androidVersion!=null) {
            AppVersionRequest request = new AppVersionRequest();
            request.setAndroidVersion(androidVersion);
            request.setUserID(userId);
            return request;
        }
        return null;
    }
}
