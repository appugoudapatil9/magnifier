package com.magnifigroup.haagstreitapp.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cygent.framework.utils.AppLog;
import com.cygent.framework.utils.Constants;
import com.cygent.framework.utils.DialogUtils;
import com.cygent.framework.utils.NetworkUtils;
import com.cygent.framework.utils.StringUtils;
import com.cygent.model.ModelApp;
import com.cygent.model.entities.response.ErrorMessage;
import com.cygent.model.entities.response.LoginResponse;
import com.magnifigroup.haagstreitapp.R;
import com.magnifigroup.haagstreitapp.mvp.presenters.LoginPresenter;
import com.magnifigroup.haagstreitapp.utils.AppConstants;

import org.greenrobot.eventbus.Subscribe;


public class LoginActivity extends AppBaseActivity{

    private Context mContext;
    private RelativeLayout rl_ParentView;
    private String TAG = "LoginActivity";
    private LoginPresenter presenter;
    RelativeLayout progressBarPanel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showDisclaimer = false;
        setContentView(R.layout.activity_login);
        mContext = this;
        presenter = new LoginPresenter();

        Button btnSubmit = (Button) findViewById(R.id.btn_submit);
        final EditText edtPin = (EditText) findViewById(R.id.edt_enterpin);
        progressBarPanel = (RelativeLayout) findViewById(R.id.progressBarPanel);

//        edtPin.setText("8-0061aa8a4668c");
        //for test
//        edtPin.setText("12-5184324b98835");
//        12-5239b4b2e09d8
        //For live
//        12-5279721cc629f
//        12-5239b4b2e09d8
//        edtPin.setText("12-5279721cc629f");
//        edtPin.setText("12-5239b4b2e09d8");
//        edtPin.setText("12-299ad377a1a18");
//        edtPin.setText("12-5218f81b1f4ad");
//        edtPin.setText("ca288bf952745d5"); // prod pin
//        edtPin.setText("caf8af81fba2791"); // prod pin
//        edtPin.setText("1c245e63c4322b9"); // UAT pin
//        edtPin.setText("8d42f47a7db20a1"); // UAT
//        edtPin.setText("e4044c9a2b76f51"); // prod pin
        edtPin.setText("d1b2b382822ad18"); // UAT PIN
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (StringUtils.isTrimmedEmpty(edtPin.getText().toString())) {
                    edtPin.requestFocus();
                    edtPin.setError("Pin is required");
                }else{
//                    showProgress(progressBarPanel);
//                Toast.makeText(mContext,"Submit entered",Toast.LENGTH_SHORT).show();
                    if (NetworkUtils.getConnectivityStatus(LoginActivity.this)) {
                        DialogUtils.showProgressDialog(LoginActivity.this, "Please wait" ,ProgressDialog.STYLE_SPINNER);
                        presenter.doLogin(edtPin.getText().toString());
                    } else {
//                        hideProgress(progressBarPanel);
                        DialogUtils.showErrorAlert(LoginActivity.this, mContext.getString(R.string.error_alert), mContext.getString(R.string.login_error_network_error), mContext.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                    }
                }
            }
        });

        edtPin.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    hideKeyboard(v);
                }
            }
        });
    }


    @Subscribe
    public void onEvent(LoginResponse response) {/* Do something */
        AppLog.d(TAG, "onEvent LoginResponse : " + response);

        saveLoginPrefrences(response);

        //Temp
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.putExtra(AppConstants.BundleKeys.IS_FROM_LOGIN,true);
        startActivity(intent);
        LoginActivity.this.onBackPressed();
        DialogUtils.dismissProgressDialog();
        //

//        registeDeviceToken();
        /*if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent1 = new Intent(this, RegistrationIntentService.class);
            startService(intent1);
        }*/
    }

    /*private void registeDeviceToken() {
        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            if (NetworkUtils.getConnectivityStatus(LoginActivity.this)) {
                Intent intent = new Intent(this, RegistrationIntentService.class);
                startService(intent);
            }else {
                DialogUtils.showErrorAlert(LoginActivity.this, getString(R.string.error_alert), getString(R.string.login_error_network_error), getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
            }
        }
    }*/

   /* @Subscribe
    public void onEvent(DeviceTokenResponse deviceTokenResponse)
    {

        ModelApp.preferencePutBoolean(Constants.SharedPrefKeys.GCM_TOKEN_REGISTERED, true);
        ModelApp.preferencePutBoolean(Constants.SharedPrefKeys.IS_THEME_UPDATE_REQUIRED,deviceTokenResponse.getIsThemeUpdateRequire());
        CommonUtils.saveAppTheme(deviceTokenResponse.getIpadCustomizeSetting());

        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.putExtra(AppConstants.BundleKeys.IS_FROM_LOGIN,true);
        startActivity(intent);
        LoginActivity.this.onBackPressed();
        DialogUtils.dismissProgressDialog();
    }*/


    @Subscribe
    public void onEvent(ErrorMessage errorMessage) {/* Do something */
        AppLog.d(TAG, "onEvent ErrorMessage : ");
        DialogUtils.dismissProgressDialog();
//        hideProgress(progressBarPanel);
        Toast.makeText(LoginActivity.this,errorMessage.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }

    public void saveLoginPrefrences(LoginResponse response) {
        ModelApp.preferencePutString(Constants.SharedPrefKeys.CLIENT_ADMIN_EMAIL, response.getClientAdminEmail());
        ModelApp.preferencePutLong(Constants.SharedPrefKeys.CLIENT_ID, response.getClientID());
        ModelApp.preferencePutString(Constants.SharedPrefKeys.LIB_DISCLAIMER, response.getLibraryDisclaimer());
        ModelApp.preferencePutString(Constants.SharedPrefKeys.SITE_NAME, response.getSiteName());
        ModelApp.preferencePutLong(Constants.SharedPrefKeys.USER_ID, response.getUserID());
        ModelApp.preferencePutString(Constants.SharedPrefKeys.USER_NAME, response.getUsername());
        ModelApp.preferencePutBoolean(Constants.SharedPrefKeys.HAS_RIGHTS_FOR_LIB_DISCLAIMER, true);
        ModelApp.preferencePutBoolean(Constants.SharedPrefKeys.DISCLAIMER_RIGHTS_FLAG,true);
        ModelApp.preferencePutString(Constants.SharedPrefKeys.DISCLAIMER_TEXT,response.getLibraryDisclaimer());
        ModelApp.preferencePutBoolean(Constants.SharedPrefKeys.HAS_LOGED_IN, true);
    }




   /* public void showEnterpinDialog() {
        dialogEnterPin = new Dialog(this);
        dialogEnterPin.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogEnterPin.setContentView(R.layout.popup_addpin);

        Button btnSubmit = (Button) dialogEnterPin.findViewById(R.id.btn_submit);
        final EditText edtPin = (EditText) dialogEnterPin.findViewById(R.id.edt_enterpin);
        progressBarPanel = (RelativeLayout) dialogEnterPin.findViewById(R.id.progressBarPanel);
        edtPin.setText("8-0061aa8a4668c");
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (StringUtils.isTrimmedEmpty(edtPin.getText().toString())) {
                    edtPin.setError("Pin is required");
                }else{
                    showProgress(progressBarPanel);
//                Toast.makeText(mContext,"Submit entered",Toast.LENGTH_SHORT).show();
                    if (NetworkUtils.getConnectivityStatus(LoginActivity.this)) {
                        presenter.doLogin(edtPin.getText().toString());
                    } else {
                        hideProgress(progressBarPanel);
                        DialogUtils.showErrorAlert(LoginActivity.this, mContext.getString(R.string.error_alert), mContext.getString(R.string.login_error_network_error), mContext.getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                    }
                }
            }
        });
        dialogEnterPin.setCancelable(false);
        dialogEnterPin.show();
    }*/


    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}
