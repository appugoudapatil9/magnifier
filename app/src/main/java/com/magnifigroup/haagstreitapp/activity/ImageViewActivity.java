package com.magnifigroup.haagstreitapp.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cygent.framework.utils.AppLog;
import com.cygent.framework.utils.Constants;
import com.cygent.framework.utils.DialogUtils;
import com.cygent.framework.utils.FileUtils;
import com.cygent.model.ModelApp;
import com.cygent.model.entities.database.ResourceEntity;
import com.magnifigroup.haagstreitapp.R;
import com.magnifigroup.haagstreitapp.controls.CircleImageView;
import com.magnifigroup.haagstreitapp.controls.TouchImageView;
import com.magnifigroup.haagstreitapp.displayingbitmaps.util.ImageWorker;
import com.magnifigroup.haagstreitapp.utils.AppConstants;

import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ImageViewActivity extends AppBaseActivity implements ImageWorker.OnImageLoadedListener {

    private static final String IMAGE_CACHE_DIR = "images";
    private static final String TAG = ImageViewActivity.class.getSimpleName();

    private String mFilePath;
    private ResourceEntity mResourceEntity;
    Bitmap mImgBitmap = null;

    ViewHolder mViewHolder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);

        mViewHolder = new ViewHolder(this);
        mBaseViewHolder = mViewHolder;

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            mResourceEntity = (ResourceEntity) bundle.getParcelable(Constants.IntentKeys.INTENT_IMAGE_FILE);
        }

        if (mResourceEntity != null) {
            if (mResourceEntity.getLocalPath() != null && !(mResourceEntity.getLocalPath().isEmpty())) {

//                HaagstreitApp.imageLoader.displayImage("file://" + mResourceEntity.getLocalPath(), mViewHolder.mImageViewer);
                loadImageFile(mResourceEntity.getLocalPath(), mViewHolder.mImageViewer);
            }
        } else {
            DialogUtils.showErrorAlert(ImageViewActivity.this, getString(R.string.error_alert), "Error occurred", getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ImageViewActivity.this.finish();
                }
            });
        }

        setUpToolbar(mViewHolder.mToolbar);
        mViewHolder.setUpAppTheme();
        mViewHolder.setData(mResourceEntity);
        mViewHolder.setListeners();

        showPermissionDialog();

    }

    @OnClick(R.id.img_logo)
    public void onHomeClick(){
        onHomeButtonClick();
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mViewHolder.setUpAppTheme();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getImageFetcher().setExitTasksEarly(false);
        getAuthorization();
    }

    @Override
    protected void onPause() {
        super.onPause();
        getImageFetcher().setExitTasksEarly(true);
        getImageFetcher().flushCache();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getImageFetcher().clearCache();
        getImageFetcher().closeCache();

        if (mViewHolder.mImageViewer != null) {
            // Cancel any pending image work
            ImageWorker.cancelWork(mViewHolder.mImageViewer);
            mViewHolder.mImageViewer.setImageDrawable(null);
        }
    }

    public void loadImageFile(String filePath, ImageView imageView) {

/*        int Measuredwidth = 0;
        int Measuredheight = 0;
        Point size = new Point();
        WindowManager w = getWindowManager();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            w.getDefaultDisplay().getSize(size);
            Measuredwidth = size.x;
            Measuredheight = size.y;
        } else {
            Display d = w.getDefaultDisplay();
            Measuredwidth = d.getWidth();
            Measuredheight = d.getHeight();
        }

        mImgBitmap = decodeSampledBitmapFromResource(filePath, Measuredwidth, Measuredheight);
        imageView.setImageBitmap(mImgBitmap);*/

        DialogUtils.showProgressDialog(ImageViewActivity.this,"Loading...", ProgressDialog.STYLE_SPINNER);
        getImageFetcher().loadImage(filePath, imageView, ImageViewActivity.this);
//        ImageReader imageReader = new ImageReader();
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        int height = options.outHeight;
        int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(String arg_FilePath,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(arg_FilePath, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(arg_FilePath, options);
    }

    @Override
    public void onBackPressed() {
        setResult(AppConstants.IntentKeys.OPEN_IMAGE_RESOURCE_RESULT_OK);
        mImgBitmap = null;
        super.onBackPressed();
    }

    @Override
    public void onImageLoaded(boolean success) {
// Set loading spinner to gone once image has loaded. Cloud also show
        // an error view here if needed.
//        mViewHolder.mProgressBar.setVisibility(View.GONE);
        DialogUtils.dismissProgressDialog();
    }

    class ViewHolder extends BaseViewHolder implements View.OnClickListener {
        @BindView(R.id.img_viewer)
        TouchImageView mImageViewer;
        @BindView(R.id.txt_title)
        TextView mTxtTitle;
        @BindView(R.id.ic_back)
        ImageView mImgIcBack;
        @BindView(R.id.toolbar)
        Toolbar mToolbar;
        @BindView(R.id.ic_share)
        ImageView mIcShare;
        @BindView(R.id.ic_send_mail)
        ImageView mIcSendMail;
//        @BindView(R.id.img_background)
//        ImageView mImgBackground;
        @BindView(R.id.img_logo)
        CircleImageView mImgLogo;
//        @BindView(R.id.progressbar)
//        ProgressBar mProgressBar;


        public ViewHolder(AppCompatActivity activity) {
            ButterKnife.bind(this, activity);
        }

        public void setData(ResourceEntity resourceEntity) {
            mTxtTitle.setText(resourceEntity.getName());

            if (resourceEntity.getSendViaMail()) {
                mIcSendMail.setVisibility(View.VISIBLE);
            } else {
                mIcSendMail.setVisibility(View.GONE);
            }
        }

        public void setListeners() {
            mImgIcBack.setOnClickListener(this);
            mIcShare.setOnClickListener(this);
            mIcSendMail.setOnClickListener(this);
            mImgLogo.setOnClickListener(this);
        }

/*        public void setUpAppTheme() {
            if (ModelApp.preferenceGetBoolen(Constants.SharedPrefKeys.IS_THEME_SAVED, false)) {
                setHeaderColor(mToolbar);
                loadBackGroundImg(mImgBackground);
                loadLogoImg(mImgLogo);
                setFontColor(mTxtTitle);
            }
        }*/

        @Override
        public void setUpAppTheme() {
            super.setUpAppTheme();
            if (ModelApp.preferenceGetBoolen(Constants.SharedPrefKeys.IS_THEME_SAVED, false)) {
                setHeaderColor(mToolbar);
//                loadBackGroundImg(mImgBackground);
                loadLogoImg(mImgLogo);
                setFontColor(mTxtTitle);
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ic_back:

                    getImageFetcher().clearCache();
                    getImageFetcher().closeCache();
                    if (mViewHolder.mImageViewer != null) {
                        // Cancel any pending image work
                        ImageWorker.cancelWork(mViewHolder.mImageViewer);
                        mViewHolder.mImageViewer.setImageDrawable(null);
                    }
                    setResult(AppConstants.IntentKeys.OPEN_IMAGE_RESOURCE_RESULT_OK);
                   /* mImgBitmap = null;

                    Drawable drawable = mImageViewer.getDrawable();
                    if (drawable instanceof BitmapDrawable) {
                        BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
                        Bitmap bitmap = bitmapDrawable.getBitmap();
                        bitmap.recycle();
                    }*/

                    finish();
//                    ImageViewActivity.super.onBackPressed();
                    break;
                case R.id.ic_share:
                    try {
                        FileUtils.openFileWithDefaultIntent(ImageViewActivity.this, new File(mResourceEntity.getLocalPath()));
                    } catch (IOException e) {
                        AppLog.d(TAG, "File IO Exception");
                        e.printStackTrace();
                    }
                    break;
                case R.id.ic_send_mail:

                    if (mResourceEntity.getSendViaMail()) {
                        if (mResourceEntity.getResourceSizeInKB() < 20000L) {
                            //can share
                            try {
                                FileUtils.shareFileViaMail(ImageViewActivity.this, new File(mResourceEntity.getLocalPath()));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                            //can not share
                            DialogUtils.showErrorAlert(ImageViewActivity.this, getString(R.string.error_alert), getString(R.string.file_size_large_error), getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                        }
                    } else {
                        //can not share
                        DialogUtils.showErrorAlert(ImageViewActivity.this, getString(R.string.error_alert), getString(R.string.send_via_mail_error), getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                    }

                    break;
                case R.id.img_logo:
                    onHomeButtonClick();
                    break;
            }
        }
    }
    @Subscribe
    public void onEvent(Integer response) {/* Do something */
        AppLog.d(TAG, "onEvent AuthorizationResponse : " + response);
        if(response != 200) {
            removeLoginPrefrences();
            Intent intent = new Intent(ImageViewActivity.this, LoginActivity.class);
            startActivity(intent);
            return;
        }

        DialogUtils.dismissProgressDialog();
    }

}
