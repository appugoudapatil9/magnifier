package com.magnifigroup.haagstreitapp.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;
import android.widget.MediaController;

import com.cygent.framework.utils.AppLog;
import com.cygent.framework.utils.Constants;
import com.cygent.framework.utils.DialogUtils;
import com.cygent.framework.utils.FileUtils;
import com.cygent.model.ModelApp;
import com.cygent.model.entities.database.ResourceEntity;
import com.magnifigroup.haagstreitapp.R;
import com.magnifigroup.haagstreitapp.controls.CircleImageView;
import com.magnifigroup.haagstreitapp.utils.AppConstants;

import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoPlayerActivity extends AppBaseActivity {

    private static final String TAG = VideoPlayerActivity.class.getSimpleName();
    private ViewHolder mViewHolder;
    private ResourceEntity mResourceEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        mViewHolder = new ViewHolder(this);
        mBaseViewHolder = mViewHolder;

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            mResourceEntity = (ResourceEntity) bundle.getParcelable(Constants.IntentKeys.INTENT_VIDEO_FILE);
        }

        //Creating MediaController
        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(mViewHolder.mVideoView);

        //specify the location of media file
        if (mResourceEntity != null) {
            if (mResourceEntity.getLocalPath() != null && !(mResourceEntity.getLocalPath().isEmpty())) {
                try{
                    Uri uri = Uri.parse(mResourceEntity.getLocalPath());
                    if(uri!=null) {
                        mViewHolder.mVideoView.setMediaController(mediaController);
                        mViewHolder.mVideoView.setVideoURI(uri);
                        mViewHolder.mVideoView.requestFocus();
                        mViewHolder.mVideoView.start();
                    }else{
                        DialogUtils.showErrorAlert(VideoPlayerActivity.this, getString(R.string.error_alert), "Error occurred", getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                VideoPlayerActivity.this.finish();
                            }
                        });
                    }
                } catch (Exception e) {
                    Log.d("Error", e.getMessage());
                    e.printStackTrace();
                }
            }
        } else {
            DialogUtils.showErrorAlert(VideoPlayerActivity.this, getString(R.string.error_alert), "Error occurred", getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    VideoPlayerActivity.this.finish();
                }
            });
        }

        setUpToolbar(mViewHolder.mToolbar);
        mViewHolder.setData(mResourceEntity);
        mViewHolder.setUpAppTheme();
        mViewHolder.setListeners();

        showPermissionDialog();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getAuthorization();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mViewHolder.setUpAppTheme();
    }

    @Override
    public void onBackPressed() {
        setResult(AppConstants.IntentKeys.OPEN_VIDEO_RESOURCE_RESULT_OK);
        super.onBackPressed();
    }

    class ViewHolder extends BaseViewHolder implements View.OnClickListener {
        @BindView(R.id.video_view)
        VideoView mVideoView;
        @BindView(R.id.ic_back)
        ImageView mImgIcBack;
        @BindView(R.id.txt_title)
        TextView mTxtTitle;
        @BindView(R.id.toolbar)
        Toolbar mToolbar;
        @BindView(R.id.ic_share)
        ImageView mIcShare;
        @BindView(R.id.ic_send_mail)
        ImageView mIcSendMail;
//        @BindView(R.id.img_background)
//        ImageView mImgBackground;
        @BindView(R.id.img_logo)
        CircleImageView mImgLogo;

        public ViewHolder(AppCompatActivity activity) {
            ButterKnife.bind(this, activity);
        }

        public void setData(ResourceEntity resourceEntity) {
            mTxtTitle.setText(resourceEntity.getName());

            if(resourceEntity.getSendViaMail())
            {
                mIcSendMail.setVisibility(View.VISIBLE);
            }else{
                mIcSendMail.setVisibility(View.GONE);
            }
        }

        public void setListeners() {
            mImgIcBack.setOnClickListener(this);
            mIcShare.setOnClickListener(this);
            mIcSendMail.setOnClickListener(this);
            mImgLogo.setOnClickListener(this);
        }

/*        public void setUpAppTheme() {
            if (ModelApp.preferenceGetBoolen(Constants.SharedPrefKeys.IS_THEME_SAVED, false)) {
                setHeaderColor(mToolbar);
                loadBackGroundImg(mImgBackground);
                loadLogoImg(mImgLogo);
                setFontColor(mTxtTitle);
            }
        }*/

        @Override
        public void setUpAppTheme() {
            super.setUpAppTheme();
            if (ModelApp.preferenceGetBoolen(Constants.SharedPrefKeys.IS_THEME_SAVED, false)) {
                setHeaderColor(mToolbar);
//                loadBackGroundImg(mImgBackground);
                loadLogoImg(mImgLogo);
                setFontColor(mTxtTitle);
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ic_back:
                    setResult(AppConstants.IntentKeys.OPEN_VIDEO_RESOURCE_RESULT_OK);
                    VideoPlayerActivity.super.onBackPressed();
                    break;
                case R.id.ic_share:
                    try {
                        FileUtils.openFileWithDefaultIntent(VideoPlayerActivity.this, new File(mResourceEntity.getLocalPath()));
                    } catch (IOException e) {
                        AppLog.d(TAG, "File IO Exception");
                        e.printStackTrace();
                    }
                    break;
                case R.id.ic_send_mail:
                    if (mResourceEntity.getSendViaMail()) {
                        if (mResourceEntity.getResourceSizeInKB() < 20000L) {
                            //can share
                            try {
                                FileUtils.shareFileViaMail(VideoPlayerActivity.this, new File(mResourceEntity.getLocalPath()));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                            //can not share
                            DialogUtils.showErrorAlert(VideoPlayerActivity.this, getString(R.string.error_alert), getString(R.string.file_size_large_error), getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                        }
                    } else {
                        //can not share
                        DialogUtils.showErrorAlert(VideoPlayerActivity.this, getString(R.string.error_alert), getString(R.string.send_via_mail_error), getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                    }

                    break;

                case R.id.img_logo:
                    onHomeButtonClick();
                    break;
            }
        }

    }

    @Subscribe
    public void onEvent(Integer response) {/* Do something */
        AppLog.d(TAG, "onEvent AuthorizationResponse : " + response);
        if(response != 200) {
            removeLoginPrefrences();
            Intent intent = new Intent(VideoPlayerActivity.this, LoginActivity.class);
            startActivity(intent);
            return;
        }

        DialogUtils.dismissProgressDialog();
    }

}
