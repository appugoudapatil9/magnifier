package com.magnifigroup.haagstreitapp.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RelativeLayout;

import com.cygent.framework.utils.AppLog;
import com.cygent.framework.utils.Constants;
import com.cygent.framework.utils.DialogUtils;
import com.cygent.framework.utils.NetworkUtils;
import com.cygent.model.ModelApp;
import com.cygent.model.entities.request.GetResourceRequest;
import com.magnifigroup.haagstreitapp.R;
import com.magnifigroup.haagstreitapp.mvp.presenters.GetResourcesPresenter;

public class DisclaimerActivity extends AppBaseActivity {

    public static boolean isAppWentToBg = false;
    private Dialog discalimerDialog = null;
    private GetResourcesPresenter presenter;

    //Widgets
    private Toolbar toolbar;
    RelativeLayout progressBarPanel;
    //

    private static final String TAG = "DisclaimerActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_disclaimer);
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setUpToolbar(toolbar);

        presenter = new GetResourcesPresenter(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        applicationWillEnterForeground();
    }

    @Override
    public void onStop() {
        super.onStop();
        applicationGoesInBackground();
    }

    @Override
    public void applicationWillEnterForeground() {
        super.applicationWillEnterForeground();
                showDisclaimerDialog();
    }


    public void applicationGoesInBackground() {
        hideDisclimerDialog();
    }

    public void showDisclaimerDialog()
    {
        boolean hasDisclimerRights = ModelApp.preferenceGetBoolen(Constants.SharedPrefKeys.HAS_RIGHTS_FOR_LIB_DISCLAIMER,false);
        // custom dialog

        if(hasDisclimerRights) {
            discalimerDialog = new Dialog(this);
            discalimerDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            discalimerDialog.setContentView(R.layout.dialog_disclaimer);

            CheckBox chkBox = (CheckBox) discalimerDialog.findViewById(R.id.chk_disclaimer);
            final Button btnAccept = (Button) discalimerDialog.findViewById(R.id.btn_accept);
            progressBarPanel = (RelativeLayout)discalimerDialog.findViewById(R.id.progressBarPanel);

            btnAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(NetworkUtils.getConnectivityStatus(DisclaimerActivity.this))
                    {
                        showProgress(progressBarPanel);
                        long clientId = ModelApp.preferenceGetLong(Constants.SharedPrefKeys.CLIENT_ID,0L);
                        long userId   = ModelApp.preferenceGetLong(Constants.SharedPrefKeys.USER_ID,0L);
                        String userName = ModelApp.preferenceGetString(Constants.SharedPrefKeys.USER_NAME,null);
                        if( userId != 0L && clientId!=0L )
                        {
                            GetResourceRequest request = new GetResourceRequest();
                            request.setUserID(userId);
                            request.setClientID(clientId);
                            presenter.getResourceList(request);
                        }
                    }else{
                        DialogUtils.showErrorAlert(DisclaimerActivity.this, getString(R.string.error_alert), getString(R.string.login_error_network_error), getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                    }
                }
            });

            chkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(((CheckBox)v).isChecked()) {
                        AppLog.d(TAG, " IS Checked ");
                        btnAccept.setEnabled(true);
                    }else{
                        AppLog.d(TAG,"Is Not Checked");
                        btnAccept.setEnabled(false);
                    }
                }
            });

            discalimerDialog.setCancelable(false);
            discalimerDialog.show();
        }

    }


    public void hideDisclimerDialog()
    {
        if(discalimerDialog!=null)
        {
            discalimerDialog.dismiss();
        }
    }

}
