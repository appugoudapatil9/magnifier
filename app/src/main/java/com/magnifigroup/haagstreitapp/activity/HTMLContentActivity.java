package com.magnifigroup.haagstreitapp.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebSettings;
import android.webkit.WebViewClient;
import android.content.Intent;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;

import com.cygent.framework.views.activities.BaseActivity;
import com.cygent.model.ModelApp;
import com.magnifigroup.haagstreitapp.R;

public class HTMLContentActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_htmlcontent);
        String html_path ="";
        WebView.setWebContentsDebuggingEnabled(true);
        WebView webView = (WebView) findViewById(R.id.wv_indexcontent);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAllowFileAccess(true);
        webSettings.setDomStorageEnabled(true);

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }
        });

        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        if(bd != null)
        {
            html_path = (String) bd.get("html_path");
            Log.i("HTMLContentActivity", "FilePath out : "+html_path);
            File file = new File(html_path);
            webView.loadUrl("file://" + file);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
