package com.magnifigroup.haagstreitapp.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.ViewSwitcher;

import com.cygent.framework.utils.AppLog;
import com.cygent.framework.utils.Constants;
import com.cygent.framework.utils.DialogUtils;
import com.cygent.framework.utils.NetworkUtils;
import com.cygent.framework.utils.network.DeleteUtils;
import com.cygent.framework.utils.network.DownLoadUtils;
import com.cygent.model.ModelApp;
import com.cygent.model.entities.database.ResourceEntity;
import com.cygent.model.entities.request.AuthRequest;
import com.cygent.model.entities.response.DeviceTokenResponse;
import com.cygent.model.entities.response.ErrorMessage;
import com.cygent.model.entities.response.Resource;
import com.magnifigroup.haagstreitapp.HaagstreitApp;
import com.magnifigroup.haagstreitapp.R;
import com.magnifigroup.haagstreitapp.adapter.DownloadedResourceAdapter;
import com.magnifigroup.haagstreitapp.controls.CircleImageView;
import com.magnifigroup.haagstreitapp.controls.DividerItemDecoration;
import com.magnifigroup.haagstreitapp.displayingbitmaps.util.Utils;
import com.magnifigroup.haagstreitapp.mvp.presenters.AuthPresenter;
import com.magnifigroup.haagstreitapp.mvp.presenters.GetResourcesPresenter;
import com.magnifigroup.haagstreitapp.utils.AppConstants;
import com.magnifigroup.haagstreitapp.utils.CommonUtils;
import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import dataservice.DataResultReceiver;
import dataservice.DataService;

public class DownloadedResourceActivity extends AppBaseActivity implements View.OnClickListener, DataResultReceiver.Listener {

    public static final String TAG = DownloadedResourceActivity.class.getSimpleName();

    ResourceEntity resourceToBeDownloaded = null;
    List<ResourceEntity> mDatalist;
    List<ResourceEntity> mDownloadedList;
    List<ResourceEntity> mTobeDownloadedList;
//    List<ResourceEntity> mCurrentList;
    List<ResourceEntity> selectedList;
    Iterator<ResourceEntity> selectedIterator;
    DownloadedResourceAdapter mDownloadedAdapter;
    DownloadedResourceAdapter mToBeDownloadedAdapter;
    ViewHolder mViewHolder;
    private GetResourcesPresenter mGetResourcesPresenter;
    private AuthPresenter mGetAuthorizedPresenter;

    boolean isResorceSelected = false;

    private boolean updateFlag;
    DataResultReceiver resultReceiver=null;
    Context mContext;

    int positionToDelete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downloaded_resource);
        mContext = this;
        updateFlag = false;
        mGetResourcesPresenter = new GetResourcesPresenter(this);
        mGetAuthorizedPresenter = new AuthPresenter(this);
        mViewHolder = new ViewHolder(this);
        mBaseViewHolder = mViewHolder;
        mViewHolder.setListeners(this);

        //Service listener
        resultReceiver = new DataResultReceiver(new Handler());
        resultReceiver.setListener(this);
        //

        mDatalist = new ArrayList<>();
        mDownloadedList = new ArrayList<>();
        mTobeDownloadedList = new ArrayList<>();

        //Set Data for folder
//        mDatalist.addAll(mGetResourcesPresenter.getDownloadedResourceList());

//        setEmptyRecyclerView();
        //

        setUpResourceList();

//        mDownloadedAdapter = new DownloadedResourceAdapter(this, mCurrentList,this);

        setUpToolbar(mViewHolder.mToolbar);
        mViewHolder.setUpAppTheme();

        showPermissionDialog();

        DialogUtils.showProgressDialog(this,getString(R.string.fetching_resources), ProgressDialog.STYLE_SPINNER);
        getAllResources();
    }

    public void getAllResources()
    {
//        DialogUtils.showProgressDialog(this,getString(R.string.fetching_resources), ProgressDialog.STYLE_SPINNER);
        DataService.startActionFetchResources(DownloadedResourceActivity.this,resultReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mViewHolder.setUpAppTheme();
        getAuthorization();

    }

    private void setEmptyRecyclerView(List<ResourceEntity> mArgList) {
        if(mArgList.size()==0){
            mViewHolder.mRecyclerViewResources.setVisibility(View.GONE);
            mViewHolder.mTxtEmptyView.setVisibility(View.VISIBLE);
        }else{
            mViewHolder.mRecyclerViewResources.setVisibility(View.VISIBLE);
            mViewHolder.mTxtEmptyView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        if(updateFlag) {
            setResult(AppConstants.IntentKeys.DOWNLOAD_MANAGER_RESULT_UPDATE_SUCCESS);
        }else{
            setResult(AppConstants.IntentKeys.DOWNLOAD_MANAGER_RESULT_UPDATE_CANCEL);
        }
        super.onBackPressed();
    }

    @Subscribe
    public void onEvent(DeviceTokenResponse response) {

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mViewHolder.setUpAppTheme();
    }

    @Override
    public void onClick(View v) {
        if(v!=null) {
            switch (v.getId()) {
                case R.id.ic_back:
                    if (updateFlag) {
                        setResult(AppConstants.IntentKeys.DOWNLOAD_MANAGER_RESULT_UPDATE_SUCCESS);
                    } else {
                        setResult(AppConstants.IntentKeys.DOWNLOAD_MANAGER_RESULT_UPDATE_CANCEL);
                    }
                    super.onBackPressed();

                    break;
                case R.id.img_delete:
                    //call api for sync status and after success delete resource

                    if(NetworkUtils.getConnectivityStatus(DownloadedResourceActivity.this)) {
                        try {
                            DialogUtils.showProgressDialog(DownloadedResourceActivity.this, getString(R.string.deleting_resource), ProgressDialog.STYLE_SPINNER);
                            positionToDelete = (Integer) v.getTag();
                            ResourceEntity mResourceEntity = mDownloadedList.get(positionToDelete);
                            mGetResourcesPresenter.updateSyncStatusToDeleteResource(mResourceEntity);
                        }catch (Exception e){
                            Logger.d("Index out of bound exception may be occurred"+e.getMessage());
                        }
                    }else{
                        Toast.makeText(DownloadedResourceActivity.this,getString(R.string.network_error_delete_resource),Toast.LENGTH_LONG).show();
                    }
//
                    break;

                case R.id.ll_res_desc:
                    openResourceEntity(mDownloadedList.get((Integer) v.getTag()));
                    break;

                case R.id.img_resource:
                    openResourceEntity(mDownloadedList.get((Integer) v.getTag()));
                    break;
                case R.id.img_logo:
                    onHomeButtonClick();
                    break;
                case R.id.rl_row_parent:
                    ResourceEntity selectedResource = mTobeDownloadedList.get((Integer) v.getTag());
                    selectedResource.setSelected(!(selectedResource.isSelected()));
                    selectedResource.setListPosition((Integer) v.getTag());
                    mToBeDownloadedAdapter.notifyDataSetChanged();
                    showHideDownloadButton();
                    break;
                case R.id.img_resource_download:
                    downloadResources();
                    break;
                case R.id.btn_view_downloaded:
                    mViewHolder.mBtnToggedEdit.setChecked(false);
                    mViewHolder.mSwitchDownloader.showNext();
                    setUpResourceList();
                    break;
                case R.id.btn_view_tobe_downloaded:
                    mViewHolder.mBtnToggedEdit.setChecked(false);
                    mViewHolder.mSwitchDownloader.showPrevious();
                    setUpResourceList();
                    break;
                case R.id.btn_toggel_edit:
                    selectedList = null;
                    setListEditable();
                    break;
                case R.id.chk_select_all:
                    selectAllResources();
                    break;
            }
        }

    }

    private void showHideDownloadButton() {
        isResorceSelected = false;
        for(ResourceEntity mResourceEntity : mTobeDownloadedList){
            if(mResourceEntity.isSelected()){
                isResorceSelected = true;
                break;
            }
        }

        if(isResorceSelected ){
            mViewHolder.imgResourceDownload.setVisibility(View.VISIBLE);
        }else{
            mViewHolder.imgResourceDownload.setVisibility(View.GONE);
        }
    }

    private void selectAllResources() {
        if(mViewHolder.mChkSelectAll.isChecked()){
            mViewHolder.imgResourceDownload.setVisibility(View.VISIBLE);
        }else{
            mViewHolder.imgResourceDownload.setVisibility(View.GONE);
        }
        for(ResourceEntity mEntity : mTobeDownloadedList){
            mEntity.setSelected(mViewHolder.mChkSelectAll.isChecked());
            mEntity.setListPosition(null);
        }
        mToBeDownloadedAdapter.notifyDataSetChanged();
        setEmptyRecyclerView(mTobeDownloadedList);
    }

    private void setListEditable() {
        for(ResourceEntity mEntity : mTobeDownloadedList){
            mEntity.setSelected(false);
            if(selectedList!=null && selectedList.size()>0){
                for(ResourceEntity selectedEntity : selectedList){
                    if(selectedEntity.equals(mEntity)){
                        mEntity.setSelected(true);
                    }
                }
            }
            mEntity.setListPosition(null);
            mEntity.setEditable(true);
//            mEntity.setEditable(mViewHolder.mBtnToggedEdit.isChecked());
        }
        mToBeDownloadedAdapter.notifyDataSetChanged();
        setEmptyRecyclerView(mTobeDownloadedList);
        /*if(mViewHolder.mBtnToggedEdit.isChecked()) {
            mViewHolder.mChkSelectAll.setVisibility(View.VISIBLE);
            mViewHolder.mChkSelectAll.setChecked(false);
//            mViewHolder.imgResourceDownload.setVisibility(View.VISIBLE);
        }else {
            mViewHolder.mChkSelectAll.setVisibility(View.GONE);
            mViewHolder.imgResourceDownload.setVisibility(View.GONE);
        }*/
    }

    private void downloadResources() {
        selectedList = new ArrayList<>();
        selectedList.clear();
        for(ResourceEntity mResourceEntity : mTobeDownloadedList){
            if(mResourceEntity.isSelected()) {
                if (!(selectedList.contains(mResourceEntity))) {
                    selectedList.add(mResourceEntity);
                }
            }
        }

        if(selectedList!=null && selectedList.size()>0) {
            selectedIterator = selectedList.iterator();
            downloadSelectedResources();
        }else{
            Toast.makeText(mContext,getString(R.string.pls_select_resource),Toast.LENGTH_SHORT).show();
        }
    }

    private void downloadSelectedResources(){
        if(NetworkUtils.getConnectivityStatus(DownloadedResourceActivity.this)){
            if(selectedIterator!=null) {
                if (selectedIterator.hasNext()) {
                    resourceToBeDownloaded = selectedIterator.next();
//                    DialogUtils.showProgressDialog(DownloadedResourceActivity.this,String.format(getString(R.string.downloading_resource),resourceToBeDownloaded.getName()), ProgressDialog.STYLE_SPINNER);
                    DialogUtils.showDownloadProgress(DownloadedResourceActivity.this,String.format(getString(R.string.downloading_resource),resourceToBeDownloaded.getName()),null,downloadCancelListener);
                    mGetResourcesPresenter.downloadResource(resourceToBeDownloaded);
//                    selectedIterator.remove();
//                    setListEditable();
                }else {
                    mViewHolder.mBtnToggedEdit.setChecked(false);
                    mViewHolder.mSwitchDownloader.showPrevious();
                    setUpResourceList();

                    DialogUtils.dismissProgressDialog();
                    DialogUtils.hideDownloadProgress();
                }
            }
        }else {
            DialogUtils.showErrorAlert(DownloadedResourceActivity.this, getString(R.string.error_alert), getString(R.string.login_error_network_error), getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }
    }

    DialogInterface.OnClickListener downloadCancelListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            selectedList = null;
            selectedIterator = null;
            mGetResourcesPresenter.cancelDonwload();
            DialogUtils.showProgressDialog(mContext,getString(R.string.pls_wait),ProgressDialog.STYLE_SPINNER);
        }
    };

    public void updateResourceAfterDelete(int position){
        ResourceEntity mResourceEntity = mDownloadedList.get(position);
        mResourceEntity.setDownloaded(false);
        mResourceEntity.setLocalPath("");
        mResourceEntity.setResourceDownloadDateTime("");

        mGetResourcesPresenter.updateResource(mResourceEntity);
        mGetResourcesPresenter.updateFolderHierarchyForDownloadManager(mResourceEntity);
//        int rowAffected = mGetResourcesPresenter.updateResource(mDatalist.get(position));

       /* if(rowAffected == 1)
        {*/
            updateFlag = true;
            //Also delete file from internal storage
            try{
                File fileToDelete = new File(mDownloadedList.get(position).getLocalPath());
                fileToDelete.delete();
            }catch (Exception e){
                AppLog.d(TAG,"deleteResource : error");
            }
        mDownloadedAdapter.removeItem(position);
        setEmptyRecyclerView(mDownloadedList);
        getAllResources();

        /*}else{
            AppLog.d(TAG,"Error occurred in deleting record");
            DialogUtils.showErrorAlert(this, getString(R.string.error_alert), ErrorMessage.RESOURCE_DELETE_ERROR, getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }*/
    }

    public void updateResourceAfterDownload(ResourceEntity argResource, DownLoadUtils.DownloadResult argResult){

       /* ResourceEntity mResourceEntity = argResource;
        mResourceEntity.setDownloaded(true);
        mResourceEntity.setLocalPath(argResult.getDownloadPath());

        mGetResourcesPresenter.updateResource(mResourceEntity);
        mGetResourcesPresenter.updateFolderHierarchy(mResourceEntity);
        */

        updateFlag = true;
        try {
            if (argResource.getListPosition() != null) {
                mToBeDownloadedAdapter.removeItem(argResource.getListPosition());
            }
        }catch (Exception e){
            Logger.d("Exception occurred for indexOutOfBound may be"+e.getMessage());
        }
        setEmptyRecyclerView(mTobeDownloadedList);
        getAllResources();
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case DataResultReceiver.RESULT_FETCH_RESOURCE:
                DialogUtils.dismissProgressDialog();
                ResourceEntity resourceEntity = (ResourceEntity) resultData.getParcelable(DataResultReceiver.RESULT_OBJECT);
                mDatalist.clear();
                mDownloadedList.clear();
                mTobeDownloadedList.clear();
                mDatalist.addAll(resourceEntity.getResourceEntityList());
                for(ResourceEntity mResourceEntity : mDatalist){
                    if(mResourceEntity.getDownloaded()){

                        if(!(mDownloadedList.contains(mResourceEntity))){
                            mDownloadedList.add(mResourceEntity);
                        }
                    }else{
                        if(!(mTobeDownloadedList.contains(mResourceEntity))){
                           /* if(selectedList!=null && selectedList.size()>0) {
                                for (ResourceEntity selectedEntity : selectedList){
                                    if(mResourceEntity.equals(selectedEntity)){
                                        mResourceEntity.setSelected(true);
                                    }
                                }
                            }*/
                            mTobeDownloadedList.add(mResourceEntity);
                        }
                    }
                }
                if(mToBeDownloadedAdapter!=null)
                {
                    setListEditable();
                    mToBeDownloadedAdapter.notifyDataSetChanged();
                    setEmptyRecyclerView(mTobeDownloadedList);
                }

                if(mDownloadedAdapter!=null)
                {
                    mDownloadedAdapter.notifyDataSetChanged();
                    setEmptyRecyclerView(mDownloadedList);
                }
//                setUpResourceList();
//                mAdapter.notifyDataSetChanged();
//                HaagstreitApp.allResourceEntityList.clear();
//                HaagstreitApp.allResourceEntityList.addAll(resourceEntity.getResourceEntityList());
                break;
        }
    }

    @Subscribe
    public void onEvent(DownLoadUtils.DownloadResult result) {
//        if(result.getResultCode()==DownLoadUtils.DownloadResult.DOWNLOAD_SUCCESS) {
        updateResourceAfterDownload(resourceToBeDownloaded, result);
        try{
            selectedIterator.remove();
        }catch (Exception e){
            Logger.d("Exception occurred for indexOutOfBound may be"+e.getMessage());
        }
        downloadSelectedResources();
        Logger.d("Event Called After Download Success");
//        }
    }

    @Subscribe
    public void onEvent(DeleteUtils deleteResult) {
        updateResourceAfterDelete(positionToDelete);
        Logger.d("Event Called After Delete Success");
        DialogUtils.dismissProgressDialog();
    }

    @Subscribe
    public void onEvent(ErrorMessage errorResponse) {
//        mIsResourceDownloaded = false;
        DialogUtils.dismissProgressDialog();
        DialogUtils.hideDownloadProgress();

        DialogUtils.showErrorAlert(DownloadedResourceActivity.this, getString(R.string.error_alert), errorResponse.getErrorMessage(), getString(android.R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
//        hideProgress(mViewHolder.mProgressBarPanel);
        //Do database operation here
//        Toast.makeText(MainActivity.this, "Resource list downloaded", Toast.LENGTH_SHORT).show();
    }


    public void setUpResourceList(){
        mViewHolder.mRecyclerViewResources.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        mViewHolder.mRecyclerViewResources.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        switch (mViewHolder.mSwitchDownloader.getCurrentView().getId()){
            case R.id.ll_view_downloaded:
                mViewHolder.imgResourceDownload.setVisibility(View.GONE);
                mDownloadedAdapter = new DownloadedResourceAdapter(this, mDownloadedList,this,true);
                mViewHolder.mRecyclerViewResources.setAdapter(mDownloadedAdapter);
                setEmptyRecyclerView(mDownloadedList);
                mViewHolder.mBtnToggedEdit.setVisibility(View.GONE);
                mViewHolder.mChkSelectAll.setVisibility(View.GONE);
                mViewHolder.mTxtSelectAll.setVisibility(View.GONE);
                break;
            case R.id.ll_view_tobe_downloaded:
                mViewHolder.imgResourceDownload.setVisibility(View.GONE);
                mToBeDownloadedAdapter = new DownloadedResourceAdapter(this, mTobeDownloadedList,this,false);
                mViewHolder.mRecyclerViewResources.setAdapter(mToBeDownloadedAdapter);
                setEmptyRecyclerView(mTobeDownloadedList);
//                mViewHolder.mBtnToggedEdit.setVisibility(View.VISIBLE);
//                mViewHolder.mBtnToggedEdit.setChecked(false);
//                mViewHolder.mChkSelectAll.setVisibility(View.GONE);
                mViewHolder.mChkSelectAll.setVisibility(View.VISIBLE);
                mViewHolder.mChkSelectAll.setChecked(false);
                mViewHolder.mTxtSelectAll.setVisibility(View.VISIBLE);
                selectedList = new ArrayList<>();
                setListEditable();
                break;
        }
    }

    private void setUpDownloadedResourceList(){

    }

    private void setUpTobeDonwloadedResourceList(){

    }

    class ViewHolder extends BaseViewHolder{

        @BindView(R.id.recyclerView_resources)
        RecyclerView mRecyclerViewResources;
        @BindView(R.id.toolbar)
        Toolbar mToolbar;
        @BindView(R.id.txt_empty_view)
        TextView mTxtEmptyView;
        @BindView(R.id.ic_back)
        ImageView mImgIcBack;
        @BindView(R.id.img_background)
        ImageView mImgBackground;
        @BindView(R.id.img_logo)
        CircleImageView mImgLogo;
        @BindView(R.id.txt_title)
        TextView mTxtTitle;
        @BindView(R.id.switch_downloader)
        ViewSwitcher mSwitchDownloader;
        @BindView(R.id.btn_view_downloaded)
        Button mBtnViewDownloaded;
        @BindView(R.id.btn_view_tobe_downloaded)
        Button mBtnViewTobeDownloaded;
        @BindView(R.id.ll_view_downloaded)
        LinearLayout mllViewDownloaded;
        @BindView(R.id.ll_view_tobe_downloaded)
        LinearLayout mllViewTobeDownloaded;
        @BindView(R.id.img_resource_download)
        ImageView imgResourceDownload;
        @BindView(R.id.btn_toggel_edit)
        ToggleButton mBtnToggedEdit;
        @BindView(R.id.chk_select_all)
        AppCompatCheckBox mChkSelectAll;
        @BindView(R.id.txtSelectAll)
        TextView mTxtSelectAll;


        public ViewHolder(AppCompatActivity aAppCompatActivity)
        {
            ButterKnife.bind(this,aAppCompatActivity);
        }

      /*  public void setUpAppTheme()
        {
            if (ModelApp.preferenceGetBoolen(Constants.SharedPrefKeys.IS_THEME_SAVED, false)) {
                setHeaderColor(mToolbar);
                loadBackGroundImg(mImgBackground);
                loadLogoImg(mImgLogo);
                setFontColor(mTxtTitle);
            }
        }*/

        @Override
        public void setUpAppTheme() {
            super.setUpAppTheme();
            if (ModelApp.preferenceGetBoolen(Constants.SharedPrefKeys.IS_THEME_SAVED, false)) {
                setHeaderColor(mToolbar);
                loadBackGroundImg(mImgBackground);
                loadLogoImg(mImgLogo);
                setFontColor(mTxtTitle);
                mDownloadedAdapter.notifyDataSetChanged();
            }
        }

        public void setListeners(View.OnClickListener arg_OnClickListener)
        {
            mImgIcBack.setOnClickListener(arg_OnClickListener);
            mImgLogo.setOnClickListener(arg_OnClickListener);
            mBtnViewDownloaded.setOnClickListener(arg_OnClickListener);
            mBtnViewTobeDownloaded.setOnClickListener(arg_OnClickListener);
            imgResourceDownload.setOnClickListener(arg_OnClickListener);
            mBtnToggedEdit.setOnClickListener(arg_OnClickListener);
            mChkSelectAll.setOnClickListener(arg_OnClickListener);
        }

    }

    @Subscribe
    public void onEvent(Integer response) {/* Do something */
        AppLog.d(TAG, "onEvent AuthorizationResponse : " + response);
        if(response != 200) {
            removeLoginPrefrences();
            Intent intent = new Intent(DownloadedResourceActivity.this, LoginActivity.class);
            startActivity(intent);
            return;
        }

        DialogUtils.dismissProgressDialog();
    }
}
