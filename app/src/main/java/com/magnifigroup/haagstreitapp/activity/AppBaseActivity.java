package com.magnifigroup.haagstreitapp.activity;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cygent.framework.utils.AppLog;
import com.cygent.framework.utils.CommonMethods;
import com.cygent.framework.utils.Constants;
import com.cygent.framework.utils.DateUtil;
import com.cygent.framework.utils.DialogUtils;
import com.cygent.framework.utils.NetworkUtils;
import com.cygent.framework.views.activities.BaseActivity;
import com.cygent.model.ModelApp;
import com.cygent.model.entities.Item;
import com.cygent.model.entities.database.ResourceEntity;
import com.cygent.model.entities.request.AppVersionRequest;
import com.cygent.model.entities.request.AuthRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.magnifigroup.haagstreitapp.BuildConfig;
import com.magnifigroup.haagstreitapp.HaagstreitApp;
import com.magnifigroup.haagstreitapp.R;
import com.magnifigroup.haagstreitapp.adapter.IntentItemAdapter;
import com.magnifigroup.haagstreitapp.displayingbitmaps.util.ImageCache;
import com.magnifigroup.haagstreitapp.displayingbitmaps.util.ImageFetcher;
import com.magnifigroup.haagstreitapp.mvp.presenters.AppVersionPresenter;
import com.magnifigroup.haagstreitapp.mvp.presenters.AuthPresenter;
import com.magnifigroup.haagstreitapp.mvp.presenters.GetResourcesPresenter;
import com.magnifigroup.haagstreitapp.utils.AppConstants;
import com.magnifigroup.haagstreitapp.utils.CommonUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import android.os.Handler;
import android.widget.Toast;

import java.io.BufferedOutputStream;
/**
 * Created by 1064 on 5/12/2016.
 */
public class AppBaseActivity extends BaseActivity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 1001;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1009;
    private static final String IMAGE_CACHE_DIR = "images";


    private Dialog disclaimerDialog = null;
    private static final String TAG = "AppBaseActivity";
    protected boolean showDisclaimer = true;
    private static GetResourcesPresenter mGetResourcesPresenter;
    private AuthPresenter mGetAuthorizedPresenter;
    private AppVersionPresenter mAppVersionPresenter;

    private RelativeLayout progressBarPanel;

    private ImageFetcher mImageFetcher;

    public BaseViewHolder mBaseViewHolder;
    ThemeBroadCast mThemeBroadCast;

    static ResourceEntity mClikedResourceEntity;
    final private ArrayList<File> fileList = new ArrayList<File>();
    final private ArrayList<File> directoryList = new ArrayList<File>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGetResourcesPresenter = new GetResourcesPresenter(this);
        mGetAuthorizedPresenter = new AuthPresenter(this);
        mAppVersionPresenter = new AppVersionPresenter(this);

        //Init ImageFatcher
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final int height = displayMetrics.heightPixels;
        final int width = displayMetrics.widthPixels;


        final int longest = (int) ((height > width ? height : width) * 1.5f);

        ImageCache.ImageCacheParams cacheParams =
                new ImageCache.ImageCacheParams(this, IMAGE_CACHE_DIR);
        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new ImageFetcher(this, longest);
        mImageFetcher.addImageCache(getSupportFragmentManager(), cacheParams);
        mImageFetcher.setImageFadeIn(false);
        //

        mThemeBroadCast = new ThemeBroadCast();

//        showPermissionDialog();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mThemeBroadCast, new IntentFilter(AppConstants.ACTION_THEME_CHANGED));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mThemeBroadCast);
    }


    public void onHomeButtonClick()
    {
        if(this instanceof MainActivity){
            return;
        }

        if(this instanceof ImageViewActivity){
            setResult(AppConstants.IntentKeys.OPEN_IMAGE_RESOURCE_RESULT_OK);
        }else if(this instanceof VideoPlayerActivity){
            setResult(AppConstants.IntentKeys.OPEN_VIDEO_RESOURCE_RESULT_OK);
        }else if(this instanceof DownloadedResourceActivity){
            setResult(AppConstants.IntentKeys.DOWNLOAD_MANAGER_RESULT_UPDATE_SUCCESS);
        }

        finish();
//        Intent homeIntent = new Intent(this,MainActivity.class);
//        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(homeIntent);
    }


    /**
     * Called by the ViewPager child fragments to load images via the one ImageFetcher
     */
    public ImageFetcher getImageFetcher() {
        return mImageFetcher;
    }

    public void loadBackGroundImg(ImageView imageView) {

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.default_bg) // resource or drawable
                .showImageForEmptyUri(R.drawable.default_bg) // resource or drawable
                .showImageOnFail(R.drawable.default_bg) // resource or drawable
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            if (!(ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_IMG_BACKGROUND, "")).isEmpty()) {
                String url = (ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_IMG_BACKGROUND, "http://"));
                AppLog.d(TAG, "BackGround Image Url Landscap: " + url);
//                ImageLoader imageLoader;
//                imageLoader = CustomVolleyRequest.getInstance(this)
//                        .getImageLoader();
//                imageLoader.get(url, ImageLoader.getImageListener(imageView,
//                        R.drawable.bghome, R.drawable.bghome));
//                imageView.setImageUrl(url, imageLoader);
                HaagstreitApp.imageLoader.displayImage(url,imageView,options);
            }
        }else{
            if (!(ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_IMG_PORTRAIT, "")).isEmpty()) {
                String url = (ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_IMG_PORTRAIT, "http://"));

                AppLog.d(TAG, "BackGround Image Url portrait: " + url);
//                ImageLoader imageLoader;
//                imageLoader = CustomVolleyRequest.getInstance(this)
//                        .getImageLoader();
//                imageLoader.get(url, ImageLoader.getImageListener(imageView,
//                        R.drawable.bghome, R.drawable.bghome));
//                imageView.setImageUrl(url, imageLoader);
                HaagstreitApp.imageLoader.displayImage(url,imageView,options);
            }
        }

    }

    public void setHeaderColor(Toolbar arg_toolbar) {
        if (!((ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_HEADER_COLOR, "")).isEmpty())) {
            String headerColor = ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_HEADER_COLOR, "");
            AppLog.d(TAG, "header color : " + headerColor);
            String strHeaderColor = CommonUtils.hexfix(headerColor);
            arg_toolbar.setBackgroundColor(Color.parseColor(strHeaderColor));
        }
    }

    public void setFontColor(TextView arg_textView) {
        if (!((ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_FONT_COLOR, "").toString().isEmpty()))) {
            String fontColor = ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_FONT_COLOR, "");
            AppLog.d(TAG, "Font color : " + fontColor);
            fontColor = CommonUtils.hexfix(fontColor);
            arg_textView.setTextColor(Color.parseColor(fontColor));
        }
    }

    public void loadLogoImg(ImageView imageView) {

        if (!(ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_IMG_LOGO, "")).isEmpty()) {
            String url = (ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_IMG_LOGO, "https://"));
            AppLog.d(TAG, "Logo Image Url : " + url);
//            ImageLoader imageLoader;
//            imageLoader = CustomVolleyRequest.getInstance(this)
//                    .getImageLoader();
//            imageView.setDefaultImageResId(R.drawable.app_logo);
//            imageView.setErrorImageResId(R.drawable.app_logo);
//            imageLoader.get(url, ImageLoader.getImageListener(imageView,
//                    R.drawable.app_logo, R.drawable.app_logo));
//            imageView.setImageUrl(url, imageLoader);


            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .showImageForEmptyUri(R.drawable.logo) // resource or drawable
                    .showImageOnFail(R.drawable.logo) // resource or drawable
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .build();
            HaagstreitApp.imageLoader.displayImage(url,imageView,options);
        }else{
            imageView.setImageResource(R.drawable.logo);
        }
    }

    public void showPermissionDialog() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(AppBaseActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
           /* if (ActivityCompat.shouldShowRequestPermissionRationale(thisActivity,
                    Manifest.permission.READ_CONTACTS)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {
*/
            // No explanation needed, we can request the permission.

            ActivityCompat.requestPermissions(AppBaseActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);

            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
//            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        ModelApp.eventBus.register(this);
    }

    @Override
    public void onStop() {
        ModelApp.eventBus.unregister(this);
        super.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    return;

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    return;
                }
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    @Override
    public void applicationWillEnterForeground() {
        super.applicationWillEnterForeground();
    }

    @Override
    protected void applicationGoesInBackground() {
        super.applicationGoesInBackground();
        hideDisclimerDialog();
    }

    public RelativeLayout getProgressBarPanelOfDisclaimer() {
        return progressBarPanel;
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    protected boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    public void showDisclaimerDialog(final View.OnClickListener aOnClickListener) {
        boolean hasDisclimerRights = ModelApp.preferenceGetBoolen(Constants.SharedPrefKeys.DISCLAIMER_RIGHTS_FLAG, false);
        // custom dialog
        if(disclaimerDialog!=null){
            disclaimerDialog.dismiss();
            disclaimerDialog = null;
        }

        String disclaimerText = ModelApp.preferenceGetString(Constants.SharedPrefKeys.DISCLAIMER_TEXT, "");
        if (hasDisclimerRights && disclaimerText!=null && !(disclaimerText.isEmpty())) {
            disclaimerDialog = new Dialog(this);
            disclaimerDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            disclaimerDialog.setContentView(R.layout.dialog_disclaimer);

            CheckBox chkBox = (CheckBox) disclaimerDialog.findViewById(R.id.chk_disclaimer);
            WebView webView = (WebView) disclaimerDialog.findViewById(R.id.webview_disclaimer);
//            String summary = "<html><body>You scored <b>192</b> points.</body></html>";
//             disclaimerText = ModelApp.preferenceGetString(Constants.SharedPrefKeys.DISCLAIMER_TEXT, "");
            webView.loadData(disclaimerText, "text/html", "utf-8");
            final Button btnAccept = (Button) disclaimerDialog.findViewById(R.id.btn_accept);
            progressBarPanel = (RelativeLayout) disclaimerDialog.findViewById(R.id.progressBarPanel);

            btnAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    disclaimerDialog.dismiss();
                    if (aOnClickListener != null) {
                        aOnClickListener.onClick(v);
                    }
                }
            });

            chkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((CheckBox) v).isChecked()) {
                        AppLog.d(TAG, " IS Checked ");
                        btnAccept.setEnabled(true);
                    } else {
                        AppLog.d(TAG, "Is Not Checked");
                        btnAccept.setEnabled(false);
                    }
                }
            });

            disclaimerDialog.setCancelable(false);
            disclaimerDialog.show();
        }

    }

    public void setUpToolbar(Toolbar toolbar) {
//        toolbar.setLogo(R.drawable.logo);
        TextView txtUser = (TextView) toolbar.findViewById(R.id.txt_user);

        if (ModelApp.preferenceGetString(Constants.SharedPrefKeys.USER_NAME, null) != null) {
            txtUser.setText(ModelApp.preferenceGetString(Constants.SharedPrefKeys.USER_NAME, null));
        }
    }


    /**
     * To open file in external app or with in the app based on its type
     * <p/>
     * Note : Check its existance before opening file
     *
     * @param aClikedResourceEntity
     */
    public void openResourceEntity(ResourceEntity aClikedResourceEntity) {
        //Go and just open it
        AppLog.d(TAG, "File already exists");

        aClikedResourceEntity.setNumberOfViews(aClikedResourceEntity.getNumberOfViews() + 1);
        aClikedResourceEntity.setLastAccessDate(DateUtil.getCurrentDateInFormat(AppConstants.DATE_FORMAT_LAST_ACCESS_DATE));
        mClikedResourceEntity = aClikedResourceEntity;

        String resourceType = aClikedResourceEntity.getResourceType();
        String localPath = aClikedResourceEntity.getLocalPath();
        if (resourceType.toUpperCase().endsWith("JPG") || resourceType.toUpperCase().endsWith("JPEG") || resourceType.toUpperCase().endsWith("PNG") ||
                resourceType.toUpperCase().endsWith(".JPG") || resourceType.toUpperCase().endsWith(".JPEG") || resourceType.toUpperCase().endsWith(".PNG")) {
            //Open Image viewer
            Intent imageIntent = new Intent(this, ImageViewActivity.class);
            imageIntent.putExtra(Constants.IntentKeys.INTENT_IMAGE_FILE, aClikedResourceEntity);
            startActivityForResult(imageIntent, AppConstants.IntentKeys.OPEN_IMAGE_RESOURCE_REQ);
//            startActivity(imageIntent);
        } else if (resourceType.toUpperCase().endsWith("MP4") || resourceType.toUpperCase().endsWith("MOV") || resourceType.toUpperCase().endsWith(".MP4") || resourceType.toUpperCase().endsWith(".MOV")) {
            //Open video player
            Intent videoIntent = new Intent(this, VideoPlayerActivity.class);
            videoIntent.putExtra(Constants.IntentKeys.INTENT_VIDEO_FILE, aClikedResourceEntity);
            startActivityForResult(videoIntent, AppConstants.IntentKeys.OPEN_VIDEO_RESOURCE_REQ);
//            startActivity(videoIntent);
        }
        else if (resourceType.toUpperCase().endsWith("ZIP")) {
            //Open zip file
            try {
                String fileName = localPath.substring(localPath.lastIndexOf("/")+1);
                String baseName = getBaseName(fileName);
                String launcherFile = mClikedResourceEntity.getLauncherFile();
                unzipin(localPath, baseName, launcherFile);
            } catch (IOException e) {
                AppLog.d(TAG, "Error in Opening ZIP file");
                //e.printStackTrace();
                DialogUtils.showErrorAlert(this, getString(R.string.error_alert), "Cannot open zip file", getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
            }
        }
        else {
            try {
                openFile(this, new File(aClikedResourceEntity.getLocalPath()));
            } catch (IOException e) {
                AppLog.d(TAG, "Error in Opening file");
                e.printStackTrace();
                DialogUtils.showErrorAlert(this, getString(R.string.error_alert), "Can not open file", getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
            }
        }
    }

    public static void openFile(final Context context, File url) throws IOException {
        // Create URI
//        Uri uri = Uri.parse("content://your.package.name/" + url.getAbsolutePath());
        //Check if supported app is installed
        if (CommonMethods.isSupportedAppInstalled(context)) {
            //supported app is installed
            File file = url;
//        Uri uri = Uri.fromFile(file);

//            Uri uri = FileProvider.getUriForFile(context, "com.magnifigroup.haagstreitapp.fileprovider", file);
            Uri uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID+".fileprovider", file);

//        context.grantUriPermission("com.adobe.reader", uri, Intent.FLAG_GRANT_READ_URI_PERMISSION);

            Intent intent = new Intent(Intent.ACTION_VIEW);
            // Check what kind of file you are trying to open, by comparing the url with extensions.
            // When the if condition is matched, plugin sets the correct intent (mime) type,
            // so Android knew what application to use to open the file
            if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
                // Word document
                intent.setDataAndType(uri, "application/msword");
            } else if (url.toString().contains(".pdf")) {
                // PDF file
                intent.setDataAndType(uri, "application/pdf");
            } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
                // Powerpoint file
                intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
            } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
                // Excel file
                intent.setDataAndType(uri, "application/vnd.ms-excel");
            } else if (url.toString().contains(".rtf")) {
                // RTF file
                intent.setDataAndType(uri, "application/rtf");
            } else if (url.toString().contains(".txt")) {
                // Text file
                intent.setDataAndType(uri, "text/plain");
            } else {
                //if you want you can also define the intent type for any other file

                //additionally use else clause below, to manage other unknown extensions
                //in this case, Android will show all applications installed on the device
                //so you can choose which application to use
                intent.setDataAndType(uri, "*/*");
            }

            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            //Get matching application packages
            List<ResolveInfo> resInfoList = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

            List<String> listPackages = new ArrayList<>();

            if (CommonMethods.appInstalledOrNot(Constants.DocViewerpackages.WPS_OFFICE, context)) {
                listPackages.add(Constants.DocViewerpackages.WPS_OFFICE);
            }

            if (CommonMethods.appInstalledOrNot(Constants.DocViewerpackages.QUICK_OFFICE, context)) {
                listPackages.add(Constants.DocViewerpackages.QUICK_OFFICE);
            }

            List<Item> itemList = new ArrayList<>();
            for (String packageName : listPackages) {
                context.grantUriPermission(packageName, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                if (packageName.equalsIgnoreCase(Constants.DocViewerpackages.QUICK_OFFICE)) {
                    itemList.add(new Item(Constants.DocViewerpackages.QUICK_OFFICE_APP, R.drawable.ic_quick_office));
                }
                if (packageName.equalsIgnoreCase(Constants.DocViewerpackages.WPS_OFFICE)) {
                    itemList.add(new Item(Constants.DocViewerpackages.WPS_APP, R.drawable.ic_wps_office));
                }
            }

            if (itemList.size() > 0) {
                showCustomIntentChooser(itemList, context, intent);
            }

            //
//            context.startActivity(intent);
        } else {
            //redirect user to play store
            DialogUtils.showErrorAlert(context, "Alert", "Supported application is not installed on your device press 'ok' to get application", "Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + Constants.DocViewerpackages.WPS_OFFICE)));
                }
            });
        }
    }

    public static void showCustomIntentChooser(final List<Item> listItem, final Context aContext, final Intent aIntent) {

        final Dialog dialog = new Dialog(aContext);
        View.OnClickListener itemClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Item item = null;
                try {
                    item = listItem.get((Integer) v.getTag());

                } catch (Exception e) {

                }

                if (item != null) {
                    if (item.getText().equalsIgnoreCase(Constants.DocViewerpackages.WPS_APP)) {
                        aIntent.setPackage(Constants.DocViewerpackages.WPS_OFFICE);
                    } else if (item.getText().equalsIgnoreCase(Constants.DocViewerpackages.QUICK_OFFICE_APP))
                    {
                        aIntent.setPackage(Constants.DocViewerpackages.QUICK_OFFICE);
                    }
                    aContext.startActivity(aIntent);
                    dialog.dismiss();
                    if(mClikedResourceEntity!=null)
                    mGetResourcesPresenter.updateResource(mClikedResourceEntity);
                }

            }
        };


        dialog.setContentView(R.layout.dialog_intent);
        dialog.setTitle("Open with...");

        // set the custom dialog components - text, image and button
        RecyclerView mRecyclerView = (RecyclerView) dialog.findViewById(R.id.recycler_intent);
        Button btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);


        mRecyclerView.setLayoutManager(new LinearLayoutManager(aContext, LinearLayoutManager.VERTICAL, false));

        IntentItemAdapter intentAdapter = new IntentItemAdapter(aContext, listItem, itemClickListener);
        mRecyclerView.setAdapter(intentAdapter);

        dialog.setCancelable(false);
        dialog.show();

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Dialog canceled
                dialog.dismiss();
            }
        });
    }


    public void showProgress(View progressBarPanel) {
        if (progressBarPanel != null) {
            progressBarPanel.setVisibility(View.VISIBLE);
        }
    }

    public void hideProgress(View progressBarPanel) {
        if (progressBarPanel != null) {
            progressBarPanel.setVisibility(View.GONE);
        }
    }

    public void hideDisclimerDialog() {
        if (disclaimerDialog != null) {
            disclaimerDialog.dismiss();
        }
    }

    @Subscribe
    public void onEvent(String strEvent) {
    }


    public class BaseViewHolder {
        public void setUpAppTheme() {

        }
    }

    public class ThemeBroadCast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            //here update App Theme
            mBaseViewHolder.setUpAppTheme();
        }
    }

    public void getAuthorization() {
        if (NetworkUtils.getConnectivityStatus(AppBaseActivity.this)) {
            AuthRequest authRequest = CommonUtils.getAuthRequest();
            if (authRequest != null) {
                mGetAuthorizedPresenter.isAuthorized(authRequest);
            }
        } else {
            DialogUtils.showErrorAlert(AppBaseActivity.this, getString(R.string.error_alert), getString(R.string.login_error_network_error), getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }
    }

    public void removeLoginPrefrences() {
        ModelApp.preferencePutString(Constants.SharedPrefKeys.CLIENT_ADMIN_EMAIL, "");
        ModelApp.preferencePutLong(Constants.SharedPrefKeys.CLIENT_ID, 0L);
        ModelApp.preferencePutString(Constants.SharedPrefKeys.LIB_DISCLAIMER, "");
        ModelApp.preferencePutString(Constants.SharedPrefKeys.SITE_NAME, "");
        ModelApp.preferencePutLong(Constants.SharedPrefKeys.USER_ID, 0L);
        ModelApp.preferencePutString(Constants.SharedPrefKeys.USER_NAME, "");
        ModelApp.preferencePutBoolean(Constants.SharedPrefKeys.HAS_RIGHTS_FOR_LIB_DISCLAIMER, false);
        ModelApp.preferencePutBoolean(Constants.SharedPrefKeys.DISCLAIMER_RIGHTS_FLAG,false);
        ModelApp.preferencePutString(Constants.SharedPrefKeys.DISCLAIMER_TEXT,"");
        ModelApp.preferencePutBoolean(Constants.SharedPrefKeys.HAS_LOGED_IN, false);
    }


    //unzip file

    public void unzipin(final String zipFilePath, String destDirectory,final String launcherFile) throws IOException {
        final String basePath = zipFilePath.substring(0, zipFilePath.lastIndexOf("/")+1);
        final String destPath =  basePath + destDirectory;
        final File destDir = new File(destPath);
        if (!destDir.exists()) {
            destDir.mkdirs();
        }
        getfile(destDir);

            final ProgressDialog progressDoalog = new ProgressDialog(AppBaseActivity.this);
            progressDoalog.setMessage("extracting files....");
            progressDoalog.setTitle("Extracting Resources from Zip");
            progressDoalog.setCancelable(false);
            progressDoalog.setCanceledOnTouchOutside(false);
            progressDoalog.show();
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                          if(!directoryList.contains(destDir)) {
                              ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath));
                              ZipEntry entry = zipIn.getNextEntry();
                              // iterates over entries in the zip file
                              while (entry != null) {
                                  Log.i("AppBaseActivity-unzipin", " File----->: " + entry.getName());

                                  File file = new File(destDir,entry.getName());
                                  if(entry.isDirectory()) {
                                      file.mkdirs();
                                  } else {
                                      if(!file.exists()){
                                          (new File(file.getParent())).mkdirs();
                                      }
                                      extractFile(zipIn, file);
                                  }

                                  zipIn.closeEntry();
                                  entry = zipIn.getNextEntry();
                              }
                              zipIn.close();
                          }
                        if(launcherFile !=null && !launcherFile.isEmpty()){
                            String htmlPath = zipFilePath.substring(0, zipFilePath.lastIndexOf("/")) +"/"+ replaceSlash(launcherFile);
                            Intent intent = new Intent(AppBaseActivity.this, HTMLContentActivity.class);
                            intent.putExtra("html_path", htmlPath);
                            startActivity(intent);
                            if(progressDoalog!=null && progressDoalog.isShowing()){
                                progressDoalog.dismiss();
                            }
                            return;
                        }
                        String firstLevelDir ="";
                        boolean indexFileFound = false;
                        getfile(destDir);

                        if(directoryList.size() > 0) {
                            firstLevelDir = directoryList.get(0).getName();
                        }

                        for (int i = 0; i < fileList.size(); i++) {
                            if(fileList.get(i).getAbsolutePath().endsWith(destPath+"/index.html") || fileList.get(i).getAbsolutePath().endsWith(destPath+"/index.htm")){
                                Intent intent = new Intent(AppBaseActivity.this, HTMLContentActivity.class);
                                intent.putExtra("html_path", fileList.get(i).getAbsolutePath());
                                startActivity(intent);
                                indexFileFound = true;
                                if(progressDoalog!=null && progressDoalog.isShowing()){
                                    progressDoalog.dismiss();
                                }
                                return;
                            }
                        }

                        for (int i = 0; i < fileList.size(); i++) {
                            if(fileList.get(i).getAbsolutePath().endsWith(firstLevelDir+"/index.html") || fileList.get(i).getAbsolutePath().endsWith(firstLevelDir+"/index.htm")) {
                                Intent intent = new Intent(AppBaseActivity.this, HTMLContentActivity.class);
                                intent.putExtra("html_path", fileList.get(i).getAbsolutePath());
                                startActivity(intent);
                                indexFileFound = true;
                                if(progressDoalog!=null && progressDoalog.isShowing()){
                                    progressDoalog.dismiss();
                                }
                                return;
                            }
                        }

                        if(indexFileFound == false) {
                            DialogUtils.showErrorAlert(AppBaseActivity.this, getString(R.string.error_alert), "No Launcher/Index file found", getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                        }

                        if(progressDoalog!=null && progressDoalog.isShowing()){
                            progressDoalog.dismiss();
                        }
                    } catch (Exception e) {
                        if(progressDoalog!=null && progressDoalog.isShowing()){
                            progressDoalog.dismiss();
                        }
                        e.printStackTrace();
                    }
                }
            }).start();
    }
    /**
     * Extracts a zip entry (file entry)
     * @param zipIn
     * @param file
     * @throws IOException
     */
    private void extractFile(ZipInputStream zipIn, File file) throws IOException {
        FileOutputStream fout = new FileOutputStream(file,false);
        BufferedOutputStream bufout = new BufferedOutputStream(fout);
        try {
            byte[] buffer = new byte[1024];
            int read = 0;
            while ((read = zipIn.read(buffer)) != -1) {
                bufout.write(buffer, 0, read);
            }
        }
        finally {
            bufout.close();
            fout.close();
        }

    }

    public static String getBaseName(String fileName) {
        int index = fileName.lastIndexOf('.');
        if (index == -1) {
            return fileName;
        } else {
            return fileName.substring(0, index);
        }
    }

    public ArrayList<File> getfile(File dir) {
        File listFile[] = dir.listFiles();
        if (listFile != null && listFile.length > 0) {
            for (int i = 0; i < listFile.length; i++) {

                if (listFile[i].isDirectory()) {
                    fileList.add(listFile[i]);
                    directoryList.add(listFile[i]);
                    getfile(listFile[i]);

                } else {
                    fileList.add(listFile[i]);
                }

            }
        }
        return fileList;
    }

    private String replaceSlash(String path){
        return path.replace("\\", "/");
    }

    public void updateAppVersion() {
        if (NetworkUtils.getConnectivityStatus(AppBaseActivity.this)) {
            AppVersionRequest appVersionRequest = CommonUtils.getAppVersionRequest();
            if (appVersionRequest != null) {
                mAppVersionPresenter.updateAppVersion(appVersionRequest);
            }
        }
    }
}
