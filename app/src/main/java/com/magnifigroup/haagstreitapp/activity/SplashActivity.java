package com.magnifigroup.haagstreitapp.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.cygent.framework.utils.AppLog;
import com.cygent.framework.utils.Constants;
import com.cygent.framework.utils.DialogUtils;
import com.cygent.model.ModelApp;
import com.magnifigroup.haagstreitapp.HaagstreitApp;
import com.magnifigroup.haagstreitapp.R;
import com.magnifigroup.haagstreitapp.utils.AppConstants;
import com.orhanobut.logger.Logger;

public class SplashActivity extends AppCompatActivity {

    private static final String TAG = "SplashActivity";
    Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mHandler = new Handler();

        mHandler.postDelayed(loginRunnable,2000);

    }


    private Runnable loginRunnable = new Runnable() {
        @Override
        public void run() {
            isUserLogedIn();
        }
    };

    public void isUserLogedIn() {
        if (HaagstreitApp.isTab) {
            if (ModelApp.preferenceGetBoolen(Constants.SharedPrefKeys.HAS_LOGED_IN, false)) {
                AppLog.d(TAG, "Has Loged IN");
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                intent.putExtra(AppConstants.BundleKeys.IS_FROM_LOGIN, false);
                intent.putExtra(AppConstants.BundleKeys.IS_APP_LAUNCH, true);
                startActivity(intent);
                SplashActivity.this.finish();
            } else {
                AppLog.d(TAG, "Has not Loged IN");
                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(intent);
                SplashActivity.this.finish();
            }
        } else {
            Logger.d(TAG, "Device not supported");
            DialogUtils.showErrorAlert(this, getString(R.string.error_alert), getString(R.string.error_device_notsupported), getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    finish();
                }
            });
        }
    }

}
