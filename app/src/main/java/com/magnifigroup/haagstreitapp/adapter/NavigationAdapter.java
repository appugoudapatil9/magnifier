package com.magnifigroup.haagstreitapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cygent.framework.utils.AppLog;
import com.cygent.framework.utils.Constants;
import com.cygent.model.ModelApp;
import com.cygent.model.entities.database.ResourceEntity;
import com.magnifigroup.haagstreitapp.R;
import com.magnifigroup.haagstreitapp.utils.CommonUtils;

import java.util.List;

/**
 * Created by hsnirmal on 5/24/2016.
 */
public class NavigationAdapter extends RecyclerView.Adapter<NavigationAdapter.MyViewHolder> {

    private static final String TAG = NavigationAdapter.class.getSimpleName();

    List<ResourceEntity> mDataList;
    private LayoutInflater inflater;
    View.OnClickListener aOnClickListener;

    public NavigationAdapter(Context context, List<ResourceEntity> data, final View.OnClickListener aOnClickListener) {
        inflater = LayoutInflater.from(context);
        this.mDataList = data;
        this.aOnClickListener = aOnClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i(TAG, "onCreateViewHolder");

        View view = inflater.inflate(R.layout.list_item_headertxt, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Log.i(TAG, "onBindViewHolder" + position);

        ResourceEntity current = mDataList.get(position);
        holder.setData(current, position);
        holder.setListeners();
        holder.setUpAppTheme();
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    public void removeItem(int position) {
        mDataList.remove(position);
        notifyItemRemoved(position);
//		notifyItemRangeChanged(position, mDataList.size());
//		notifyDataSetChanged();
    }

    public void addItem(int position, ResourceEntity resourceEntity) {
        mDataList.add(position, resourceEntity);
        notifyItemInserted(position);
//		notifyItemRangeChanged(position, mDataList.size());
//		notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title;
        ImageView icNavigate;
        //		ImageView imgFileFolder;
        int position;
        ResourceEntity current;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.txt_header);
            icNavigate = (ImageView) itemView.findViewById(R.id.ic_navigate);
//			imgFileFolder    = (ImageView) itemView.findViewById(R.id.imgFileFolder);
        }

        private void setUpAppTheme() {
            if (ModelApp.preferenceGetBoolen(Constants.SharedPrefKeys.IS_THEME_SAVED, false)) {
//                if (!((ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_FONT_COLOR, "").toString().isEmpty()))) {
//                    title.setTextColor(CommonUtils.getColorFromHex((ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_FONT_COLOR, "#000000"))));
//                }

                setFontColor(title);
            }
        }

        public void setFontColor(TextView arg_textView) {
            if(!((ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_FONT_COLOR,"").toString().isEmpty())))
            {
                String fontColor = ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_FONT_COLOR, "");
                AppLog.d(TAG,"Font color : "+fontColor);
                fontColor = CommonUtils.hexfix(fontColor);
                arg_textView.setTextColor(Color.parseColor(fontColor));
            }
        }

        public void setData(ResourceEntity current, int position) {

            this.title.setText(current.getName());
            if (position != 0 && position != (mDataList.size()-1)) {
//                title.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_navigate, 0);
                icNavigate.setVisibility(View.VISIBLE);
            }else if(position==0){
                icNavigate.setVisibility(View.GONE);
            }else{
                icNavigate.setVisibility(View.INVISIBLE);
            }
//			imgFileFolder.setImageResource(current.getImageID());
            this.position = position;
            this.current = current;
        }

        public void setListeners() {
            title.setOnClickListener(MyViewHolder.this);
        }

        @Override
        public void onClick(View v) {
//			Log.i("onClick before operation", position + " " + mDataList.size());
            switch (v.getId()) {
                case R.id.txt_header:
//					removeItem(position);
                    v.setTag(position);
                    aOnClickListener.onClick(v);
                    break;

            }
//			Log.i("onClick after operation", mDataList.size() + " \n\n" + mDataList.toString());
        }
    }
}
