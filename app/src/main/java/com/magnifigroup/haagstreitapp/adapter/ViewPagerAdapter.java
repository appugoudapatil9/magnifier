package com.magnifigroup.haagstreitapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;

import com.cygent.model.entities.database.ResourceEntity;
import com.magnifigroup.haagstreitapp.fragment.GridlayoutFragment;

import java.util.List;

/**
 * Created by hsnirmal on 5/24/2016.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    public static final String TAG = ViewPagerAdapter.class.getSimpleName();
    List<ResourceEntity> mDataList;
    private LayoutInflater inflater;
    View.OnClickListener mOnClickListener;
    private FragmentManager fragmentManager;
    public ViewPagerAdapter(FragmentManager fm, List<ResourceEntity> data, View.OnClickListener aOnClickListener) {
        super(fm);
        fragmentManager = fm;
        this.mDataList = data;
        mOnClickListener = aOnClickListener;
    }


    @Override
    public Fragment getItem(int position) {

        return GridlayoutFragment.newInstance(mOnClickListener, position, mDataList.get(position).getResourceEntityList());
    }

    @Override
    public int getCount() {
        return mDataList.size();
    }


    @Override
    public int getItemPosition(Object object) {
//        GridlayoutFragment currentFragment = (GridlayoutFragment) object;
//        int fragPosition = currentFragment.fragmentPosition;
//        if(fragPosition >= 0)
//        {
//            return fragPosition;
//        }
        return POSITION_NONE;
    }

    public GridlayoutFragment getCurrentFragment(int containerID, int position) {
        return (GridlayoutFragment) fragmentManager.findFragmentByTag("android:switcher:" + containerID + ":" + position);
    }
}
