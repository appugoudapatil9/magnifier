package com.magnifigroup.haagstreitapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.cygent.framework.utils.AppLog;
import com.cygent.framework.utils.Constants;
import com.cygent.framework.utils.DateUtil;
import com.cygent.model.ModelApp;
import com.cygent.model.entities.database.ResourceEntity;
import com.magnifigroup.haagstreitapp.HaagstreitApp;
import com.magnifigroup.haagstreitapp.R;
import com.magnifigroup.haagstreitapp.controls.CircleImageView;
import com.magnifigroup.haagstreitapp.controls.CircularTextView;
import com.magnifigroup.haagstreitapp.controls.CustomVolleyRequest;
import com.magnifigroup.haagstreitapp.utils.AppConstants;
import com.magnifigroup.haagstreitapp.utils.CommonUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by hsnirmal on 5/18/2016.
 */
public class GridViewAdapter extends BaseAdapter {

    public static final String TAG = GridViewAdapter.class.getSimpleName();


    List<ResourceEntity> mDataList;
    Context context;
    View.OnClickListener mOnClickListener;
    private LayoutInflater inflater;
    //    ResourceFilter mFilter;
    List<ResourceEntity> originalList;

    public GridViewAdapter(Context context, List<ResourceEntity> data, List<ResourceEntity> arg_originalList, View.OnClickListener aOnclickListener) {
        this.mDataList = data;
        this.originalList = arg_originalList;
        this.context = context;
        this.mOnClickListener = aOnclickListener;
        inflater = LayoutInflater.from(context);

    }


    @Override
    public int getCount() {
        return mDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        MyViewHolder viewHolder;
        ResourceEntity mFileModel = mDataList.get(position);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_folder, parent, false);
            viewHolder = new MyViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (MyViewHolder) convertView.getTag();
        }

        viewHolder.setData(mFileModel, position);
        viewHolder.setListener();

        return convertView;
    }

    class MyViewHolder implements View.OnClickListener {

        TextView txtTitle;
        TextView txtSize;
        TextView txtTime;
        TextView txtBadge;
        CircleImageView imgFileFolder;
        ImageView imgIcCloud;
        int position;
        ResourceEntity current;
        LinearLayout ll_res_desc;

        MyViewHolder(View itemView) {
            txtTitle = (TextView) itemView.findViewById(R.id.txtTitle);
            txtSize = (TextView) itemView.findViewById(R.id.txtSize);
            txtTime = (TextView) itemView.findViewById(R.id.txtTime);
            txtBadge = (TextView) itemView.findViewById(R.id.txtBadge);
            imgFileFolder = (CircleImageView) itemView.findViewById(R.id.imgFileFolder);
            imgIcCloud = (ImageView) itemView.findViewById(R.id.img_ic_cloud);
            ll_res_desc = (LinearLayout) itemView.findViewById(R.id.ll_res_desc);

            if (ModelApp.preferenceGetBoolen(Constants.SharedPrefKeys.IS_THEME_SAVED, false)) {
                if (!((ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_FONT_COLOR, "").toString().isEmpty()))) {
//                    txtTitle.setTextColor(CommonUtils.getColorFromHex((ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_FONT_COLOR, ""))));
//                    txtSize.setTextColor(CommonUtils.getColorFromHex((ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_FONT_COLOR, ""))));
//                    txtTime.setTextColor(CommonUtils.getColorFromHex((ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_FONT_COLOR, ""))));

                    setFontColor(txtSize);
                    setFontColor(txtTime);
                    setFontColor(txtTitle);
                }
            }
        }

        public void setFontColor(TextView arg_textView) {
            if(!((ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_FONT_COLOR,"").toString().isEmpty())))
            {
                String fontColor = ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_FONT_COLOR, "");
                AppLog.d(TAG,"Font color : "+fontColor);
                fontColor = CommonUtils.hexfix(fontColor);
                arg_textView.setTextColor(Color.parseColor(fontColor));
            }
        }

        public void setData(ResourceEntity aCurrent, int aPosition) {
            this.current = aCurrent;
            this.position = aPosition;
            this.txtTitle.setText(aCurrent.getName());
            this.txtBadge.setText(current.getResourceCount().toString());

            if (aCurrent.getIsResource()) {
                this.txtSize.setVisibility(View.VISIBLE);
                this.txtTime.setVisibility(View.VISIBLE);

//                if (ModelApp.preferenceGetBoolen(Constants.SharedPrefKeys.DISCLAIMER_HIDE_DATE_SIZE, false)) {
//                    this.txtSize.setText("");
//                    this.txtTime.setText("");
//                } else {
                if (aCurrent.getResourceSize() != null && !aCurrent.getResourceSize().equalsIgnoreCase("0.00000") && !(current.getResourceSize().isEmpty())) {
                    this.txtSize.setText("File Size: " + aCurrent.getResourceSize());
                } else {
                    this.txtSize.setVisibility(View.GONE);
                }
                if (aCurrent.getResourceDownloadDateTime() != null) {
                    try {
                        this.txtTime.setText(DateUtil.convertStrDateFormat(aCurrent.getResourceDownloadDateTime(), AppConstants.DATE_FORMAT_24, AppConstants.TIME_FORMAT_TO_DISPLAY) + "|" + DateUtil.convertStrDateFormat(aCurrent.getResourceDownloadDateTime(), AppConstants.DATE_FORMAT_24, AppConstants.DATE_FORMAT_ON_HOME));
                    } catch (ParseException e) {
                        AppLog.d(TAG, "Date parse exception : " + e.getMessage());
                    }
                } else {
                    this.txtTime.setText("");
                }
//                }
            } else {
                this.txtSize.setVisibility(View.GONE);
                this.txtTime.setVisibility(View.GONE);
            }
            loadImage(this.imgFileFolder, this.current);
        }

        public void setListener() {
            imgFileFolder.setOnClickListener(MyViewHolder.this);
            ll_res_desc.setOnClickListener(MyViewHolder.this);
        }

        private void loadImageInImageView(ImageView arg_imageView, String arg_url, Drawable arg_defaultImg, final ResourceEntity  arg_resourceEntity )
        {
            if(arg_resourceEntity.getImagePath()!=null && !(arg_resourceEntity.getImagePath().isEmpty())) {
                DisplayImageOptions options = new DisplayImageOptions.Builder()
//                    .showImageOnLoading(arg_defaultImg) // resource or drawable
                        .showImageForEmptyUri(arg_defaultImg) // resource or drawable
                        .showImageOnFail(arg_defaultImg) // resource or drawable
                        .cacheInMemory(true)
                        .cacheOnDisk(true)
                        .build();
                HaagstreitApp.imageLoader.displayImage(arg_url, arg_imageView, options, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {

                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        hideShowBadge(arg_resourceEntity);
                        hideShowCloudImage(arg_resourceEntity);
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                        hideShowBadge(arg_resourceEntity);
                        hideShowCloudImage(arg_resourceEntity);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        hideShowBadge(arg_resourceEntity);
                        hideShowCloudImage(arg_resourceEntity);
                    }
                });
            }else{
                arg_imageView.setImageDrawable(arg_defaultImg);
                hideShowBadge(arg_resourceEntity);
                hideShowCloudImage(arg_resourceEntity);
            }
        }

        private void loadImage(ImageView imageView, ResourceEntity arg_entity) {
//            ImageLoader imageLoader;
//            imageLoader = CustomVolleyRequest.getInstance(context)
//                    .getImageLoader();

            String url = !(arg_entity.getImagePath().isEmpty()) ? arg_entity.getImagePath() : "http://";

            if (arg_entity.getIsResource()) {

                if (arg_entity.getResourceType().equalsIgnoreCase("JPG") || arg_entity.getResourceType().equalsIgnoreCase("JPEG") || arg_entity.getResourceType().equalsIgnoreCase("PNG") ||
                arg_entity.getResourceType().equalsIgnoreCase(".JPG") || arg_entity.getResourceType().equalsIgnoreCase(".JPEG") || arg_entity.getResourceType().equalsIgnoreCase(".PNG")) {
//                    HaagstreitApp.imageLoader.displayImage(arg_entity.getImagePath(), this.imgFileFolder,HaagstreitApp.displayImageImageFile);

//                    imageLoader.get(url, ImageLoader.getImageListener(imageView,
//                            R.drawable.ic_image, R.drawable.ic_image));

                        loadImageInImageView(imageView, url, ContextCompat.getDrawable(context, R.drawable.ic_image), arg_entity);
                } else if (arg_entity.getResourceType().equalsIgnoreCase("MP4") || arg_entity.getResourceType().equalsIgnoreCase("MOV") ||
                arg_entity.getResourceType().equalsIgnoreCase(".MP4") || arg_entity.getResourceType().equalsIgnoreCase(".MOV")) {
//                    HaagstreitApp.imageLoader.displayImage(arg_entity.getImagePath(), this.imgFileFolder,HaagstreitApp.displayImageVideoFile);
//                    imageLoader.get(url, ImageLoader.getImageListener(imageView,
//                            R.drawable.ic_video, R.drawable.ic_video));
                    loadImageInImageView(imageView,url, ContextCompat.getDrawable(context,R.drawable.ic_video),arg_entity);
                } else if (arg_entity.getResourceType().equalsIgnoreCase("MP3") || arg_entity.getResourceType().equalsIgnoreCase(".MP3")) {

//                    HaagstreitApp.imageLoader.displayImage(arg_entity.getImagePath(), this.imgFileFolder,HaagstreitApp.displayImageAudioFile);
//                    imageLoader.get(url, ImageLoader.getImageListener(imageView,
//                            R.drawable.ic_audio, R.drawable.ic_audio));
                    loadImageInImageView(imageView,url, ContextCompat.getDrawable(context,R.drawable.ic_audio),arg_entity);
                } else if (arg_entity.getResourceType().equalsIgnoreCase("PPT") || arg_entity.getResourceType().equalsIgnoreCase(".PPT") ||
                        arg_entity.getResourceType().equalsIgnoreCase("PPTX") || arg_entity.getResourceType().equalsIgnoreCase(".PPTX")) {

//                    HaagstreitApp.imageLoader.displayImage(arg_entity.getImagePath(), this.imgFileFolder,HaagstreitApp.displayImagePPTFile);

//                    imageLoader.get(url, ImageLoader.getImageListener(imageView,
//                            R.drawable.ic_ppt, R.drawable.ic_ppt));
                    loadImageInImageView(imageView,url, ContextCompat.getDrawable(context,R.drawable.ic_ppt),arg_entity);
                } else if (arg_entity.getResourceType().equalsIgnoreCase("XLS") || arg_entity.getResourceType().equalsIgnoreCase(".XLS")) {
//                    HaagstreitApp.imageLoader.displayImage(arg_entity.getImagePath(), this.imgFileFolder,HaagstreitApp.displayImageXLSFile);
//                    imageLoader.get(url, ImageLoader.getImageListener(imageView,
//                            R.drawable.ic_xls, R.drawable.ic_xls));
                    loadImageInImageView(imageView,url, ContextCompat.getDrawable(context,R.drawable.ic_xls),arg_entity);
                } else {
//                    HaagstreitApp.imageLoader.displayImage(arg_entity.getImagePath(), this.imgFileFolder,HaagstreitApp.displayImageDocFile);
//                    imageLoader.get(url, ImageLoader.getImageListener(imageView,
//                            R.drawable.ic_doc, R.drawable.ic_doc));
                    loadImageInImageView(imageView,url, ContextCompat.getDrawable(context,R.drawable.ic_doc),arg_entity);
                }

                /*if (arg_entity.getDownloaded()) {
                    this.imgIcCloud.setVisibility(View.GONE);
                } else {
                    this.imgIcCloud.setVisibility(View.VISIBLE);
                }

                this.txtBadge.setVisibility(View.GONE);*/

//                hideShowCloudImage(arg_entity);
//                hideShowBadge(arg_entity);

            } else {
//                HaagstreitApp.imageLoader.displayImage(arg_entity.getImagePath(), this.imgFileFolder,HaagstreitApp.displayImageFolder);
//                imageLoader.get(url, ImageLoader.getImageListener(imageView,
//                        R.drawable.ic_folder, R.drawable.ic_folder));
                loadImageInImageView(imageView,url, ContextCompat.getDrawable(context,R.drawable.ic_folder),arg_entity);

                /*this.imgIcCloud.setVisibility(View.GONE);
                if(arg_entity.getResourceCount()==0)
                {
                    this.txtBadge.setVisibility(View.GONE);
                }else {
                    this.txtBadge.setVisibility(View.VISIBLE);
                }*/

//                hideShowCloudImage(arg_entity);
//                hideShowBadge(arg_entity);
            }

//            imageView.setImageUrl(url, imageLoader);
        }

        public void hideShowCloudImage(ResourceEntity arg_entity){
            if(arg_entity.getIsResource()){
                if (arg_entity.getDownloaded()) {
                    this.imgIcCloud.setVisibility(View.GONE);
                } else {
                    this.imgIcCloud.setVisibility(View.VISIBLE);
                }
            }else{
                this.imgIcCloud.setVisibility(View.GONE);
            }
        }

        public void hideShowBadge(ResourceEntity arg_entity){
            if(arg_entity.getIsResource()){
                this.txtBadge.setVisibility(View.GONE);
            }else{
                if(arg_entity.getResourceCount()==0)
                {
                    this.txtBadge.setVisibility(View.GONE);
                }else {
                    this.txtBadge.setVisibility(View.VISIBLE);
                }
            }
        }

        @Override
        public void onClick(View v) {
//            Toast.makeText(context,"Got Click On Folder",Toast.LENGTH_SHORT).show();
            v.setTag(current);
            mOnClickListener.onClick(v);
            InputMethodManager inputMethodManager =(InputMethodManager)v.getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }
}
