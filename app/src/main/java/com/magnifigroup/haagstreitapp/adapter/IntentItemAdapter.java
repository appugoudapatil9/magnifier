package com.magnifigroup.haagstreitapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cygent.framework.utils.AppLog;
import com.cygent.framework.utils.Constants;
import com.cygent.model.ModelApp;
import com.cygent.model.entities.Item;
import com.cygent.model.entities.database.ResourceEntity;
import com.magnifigroup.haagstreitapp.R;
import com.magnifigroup.haagstreitapp.utils.CommonUtils;

import java.util.List;

/**
 * Created by hsnirmal on 7/26/2016.
 */
public class IntentItemAdapter  extends RecyclerView.Adapter<IntentItemAdapter.MyViewHolder> {

    private static final String TAG = NavigationAdapter.class.getSimpleName();

    List<Item> mDataList;
    private LayoutInflater inflater;
    View.OnClickListener mOnClickListener;

    public IntentItemAdapter(Context context, List<Item> data, View.OnClickListener arg_OnClickListener) {
        inflater = LayoutInflater.from(context);
        this.mDataList = data;
        this.mOnClickListener = arg_OnClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i(TAG, "onCreateViewHolder");

        View view = inflater.inflate(R.layout.list_intent, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Log.i(TAG, "onBindViewHolder" + position);

        Item current = mDataList.get(position);
        holder.setData(current, position);
        holder.setListeners();
        holder.setUpAppTheme();
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView txtIntent;

        @Override
        public void onClick(View v) {
            v.setTag(position);
            mOnClickListener.onClick(v);
        }

        ImageView imgIntent;
        //		ImageView imgFileFolder;
        int position;
        Item current;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtIntent = (TextView) itemView.findViewById(R.id.txt_intent);
            imgIntent = (ImageView) itemView.findViewById(R.id.img_intent);
//			imgFileFolder    = (ImageView) itemView.findViewById(R.id.imgFileFolder);
        }

        private void setUpAppTheme() {
            if (ModelApp.preferenceGetBoolen(Constants.SharedPrefKeys.IS_THEME_SAVED, false)) {
//                if (!((ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_FONT_COLOR, "").toString().isEmpty()))) {
//                    title.setTextColor(CommonUtils.getColorFromHex((ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_FONT_COLOR, "#000000"))));
//                }

                setFontColor(txtIntent);
            }
        }

        public void setFontColor(TextView arg_textView) {
            if(!((ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_FONT_COLOR,"").toString().isEmpty())))
            {
                String fontColor = ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_FONT_COLOR, "");
                AppLog.d(TAG,"Font color : "+fontColor);
                fontColor = CommonUtils.hexfix(fontColor);
                arg_textView.setTextColor(Color.parseColor(fontColor));
            }
        }

        public void setData(Item current, int position) {

            this.txtIntent.setText(current.getText());
            this.imgIntent.setImageResource(current.getIcon());
            this.position = position;
            this.current = current;
        }

        public void setListeners() {
//            title.setOnClickListener(MyViewHolder.this);
            txtIntent.setOnClickListener(this);
            imgIntent.setOnClickListener(this);
        }
    }
}

