package com.magnifigroup.haagstreitapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.nfc.Tag;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.cygent.framework.utils.AppLog;
import com.cygent.framework.utils.Constants;
import com.cygent.framework.utils.DateUtil;
import com.cygent.model.ModelApp;
import com.cygent.model.entities.database.ResourceEntity;
import com.magnifigroup.haagstreitapp.HaagstreitApp;
import com.magnifigroup.haagstreitapp.R;
import com.magnifigroup.haagstreitapp.controls.CircleImageView;
import com.magnifigroup.haagstreitapp.controls.CustomVolleyRequest;
import com.magnifigroup.haagstreitapp.utils.AppConstants;
import com.magnifigroup.haagstreitapp.utils.CommonUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import java.text.ParseException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by hsnirmal on 5/26/2016.
 */
public class DownloadedResourceAdapter extends RecyclerView.Adapter<DownloadedResourceAdapter.MyViewHolder> {

    private static final String TAG = NavigationAdapter.class.getSimpleName();

    List<ResourceEntity> mDataList;
    private LayoutInflater inflater;
    View.OnClickListener aOnClickListener;
    Context context;
    boolean isFromDownloaded;
    boolean selectionFlag = false;

    public DownloadedResourceAdapter(Context context, List<ResourceEntity> data, final View.OnClickListener aOnClickListener, boolean arg_isfromDownloaded) {
        inflater = LayoutInflater.from(context);
        this.mDataList = data;
        this.aOnClickListener = aOnClickListener;
        this.context = context;
        this.isFromDownloaded = arg_isfromDownloaded;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i(TAG, "onCreateViewHolder");

        View view = inflater.inflate(R.layout.list_item_resource, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Log.i(TAG, "onBindViewHolder" + position);

        ResourceEntity current = mDataList.get(position);
        holder.setData(current, position);
        if(isFromDownloaded) {
            holder.setListeners();
        }
        holder.setUpAppTheme();
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    public void removeItem(int position) {
        mDataList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mDataList.size());
//		notifyDataSetChanged();
    }

    public void addItem(int position, ResourceEntity fileModel) {
        mDataList.add(position, fileModel);
        notifyItemInserted(position);
        notifyItemRangeChanged(position, mDataList.size());
//		notifyDataSetChanged();
    }

    public boolean isSelectionFlag() {
        return selectionFlag;
    }

    public void setSelectionFlag(boolean selectionFlag) {
        this.selectionFlag = selectionFlag;
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.img_resource)
        CircleImageView imgResource;
        @BindView(R.id.txt_title)
        TextView txtTitle;
        @BindView(R.id.txt_hierarchy)
        TextView txtResourceHierarchy;
        @BindView(R.id.txt_size)
        TextView txtSize;
        @BindView(R.id.txt_time)
        TextView txtTime;
        @BindView(R.id.img_delete)
        ImageView imgDelete;
        @BindView(R.id.ll_res_desc)
        LinearLayout llResDesc;
        @BindView(R.id.chk_select)
        AppCompatCheckBox checkBoxSelected;
        @BindView(R.id.rl_row_parent)
        RelativeLayout rlRowParent;

        //		ImageView imgFileFolder;
        int position;
        ResourceEntity current;
        View convertView;


        public MyViewHolder(View itemView) {
            super(itemView);
            this.convertView = itemView;
            convertView.setOnClickListener(this);
            ButterKnife.bind(this, itemView);
        }

        private void setUpAppTheme() {
            if (ModelApp.preferenceGetBoolen(Constants.SharedPrefKeys.IS_THEME_SAVED, false)) {
                if (!((ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_FONT_COLOR, "").toString().isEmpty()))) {

//                    txtTitle.setTextColor(CommonUtils.getColorFromHex((ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_FONT_COLOR, "#000000"))));
//                    txtResourceHierarchy.setTextColor(CommonUtils.getColorFromHex((ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_FONT_COLOR, "#000000"))));
//                    txtSize.setTextColor(CommonUtils.getColorFromHex((ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_FONT_COLOR, "#000000"))));

                    setFontColor(txtTitle);
                    setFontColor(txtResourceHierarchy);
                    setFontColor(txtSize);
                    setFontColor(txtTime);
                }
            }
        }

        public void setFontColor(TextView arg_textView) {
            if(!((ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_FONT_COLOR,"").toString().isEmpty())))
            {
                String fontColor = ModelApp.preferenceGetString(Constants.SharedPrefKeys.THEME_FONT_COLOR, "");
                AppLog.d(TAG,"Font color : "+fontColor);
                fontColor = CommonUtils.hexfix(fontColor);
                arg_textView.setTextColor(Color.parseColor(fontColor));
            }
        }

        public void setData(ResourceEntity current, int position) {
            this.position = position;
            this.current = current;
            this.txtTitle.setText(current.getName());
            this.txtResourceHierarchy.setText(current.getResourceHierarchy());
            this.imgDelete.setClickable(false);
            this.imgResource.setClickable(false);
            this.llResDesc.setClickable(false);

            if(isFromDownloaded){
                if(current.getDownloaded()) {
                    this.imgDelete.setVisibility(View.VISIBLE);
                    this.checkBoxSelected.setVisibility(View.GONE);
                }
            }else{
                this.imgDelete.setVisibility(View.GONE);
                if(current.isEditable()) {
                    this.checkBoxSelected.setVisibility(View.VISIBLE);
                }else{
                    this.checkBoxSelected.setVisibility(View.GONE);
                }
                if(current.isSelected()){
                    selectionFlag = true;
                    this.checkBoxSelected.setChecked(true);
                }else{
                    this.checkBoxSelected.setChecked(false);
                }
            }

//            if(ModelApp.preferenceGetBoolen(Constants.SharedPrefKeys.DISCLAIMER_HIDE_DATE_SIZE,false))
//            {
//                this.txtSize.setText("");
//                this.txtTime.setText("");
//            }else {
                try {
                    if(current.getResourceSize()!=null && !current.getResourceSize().equalsIgnoreCase("0.00000") && !(current.getResourceSize().isEmpty())){
                        this.txtSize.setText("File Size: "+current.getResourceSize());
                    }else{
                        this.txtSize.setText("");
                    }
                    if(current.getResourceDownloadDateTime()!=null && !(current.getResourceDownloadDateTime().isEmpty())){
                        this.txtTime.setText(DateUtil.convertStrDateFormat(current.getResourceDownloadDateTime(), AppConstants.DATE_FORMAT_24, AppConstants.TIME_FORMAT_TO_DISPLAY)+"|"+DateUtil.convertStrDateFormat(current.getResourceDownloadDateTime(), AppConstants.DATE_FORMAT_24,AppConstants.DATE_FORMAT_TO_DISPLAY));
                    }else{
                        this.txtTime.setText("");
                    }
//                    this.txtSize.setText("File Size : "+current.getResourceSize() + " | " + DateUtil.convertStrDateFormat(current.getResourceDownloadDateTime(), AppConstants.DATE_FORMAT_24,AppConstants.DATE_FORMAT_TO_DISPLAY));
                } catch (ParseException e) {
                    AppLog.d(TAG,"Date parse exception : "+e.getMessage());
                }
//            }
            loadImage(this.imgResource, this.current);
        }

        public void setListeners() {
            if(this.current.getDownloaded()) {
                imgDelete.setOnClickListener(MyViewHolder.this);
                imgResource.setOnClickListener(MyViewHolder.this);
                llResDesc.setOnClickListener(MyViewHolder.this);
//                rlRowParent.setOnClickListener(MyViewHolder.this);
            }
            /*}else {
                imgDelete.setOnClickListener(null);
                imgResource.setOnClickListener(null);
                llResDesc.setOnClickListener(null);
                rlRowParent.setOnClickListener(MyViewHolder.this);
                this.convertView.setOnClickListener(MyViewHolder.this);
            }*/
        }

        private void loadImageInImageView(ImageView arg_imageView, String arg_url, Drawable arg_defaultImg, ResourceEntity arg_entity)
        {
            if(arg_entity.getImagePath()!=null && !(arg_entity.getImagePath().isEmpty())) {
                DisplayImageOptions options = new DisplayImageOptions.Builder()
//                    .showImageOnLoading(arg_defaultImg) // resource or drawable
                        .showImageForEmptyUri(arg_defaultImg) // resource or drawable
                        .showImageOnFail(arg_defaultImg) // resource or drawable
                        .cacheInMemory(true)
                        .cacheOnDisk(true)
                        .build();
                HaagstreitApp.imageLoader.displayImage(arg_url, arg_imageView, options);
            }else{
                arg_imageView.setImageDrawable(arg_defaultImg);
            }
        }

        private void loadImage(ImageView imageView, ResourceEntity arg_entity) {
//            ImageLoader imageLoader;
//            imageLoader = CustomVolleyRequest.getInstance(context)
//                    .getImageLoader();

            String url = !(arg_entity.getImagePath().isEmpty()) ? arg_entity.getImagePath() : "http://";
            if (arg_entity.getResourceType().equalsIgnoreCase("JPG") || arg_entity.getResourceType().equalsIgnoreCase("JPEG") || arg_entity.getResourceType().equalsIgnoreCase("PNG")) {
//                    HaagstreitApp.imageLoader.displayImage(arg_entity.getImagePath(), this.imgFileFolder,HaagstreitApp.displayImageImageFile);

//                imageLoader.get(url, ImageLoader.getImageListener(imageView,
//                        R.drawable.ic_image, R.drawable.ic_image));

                loadImageInImageView(imageView,url, ContextCompat.getDrawable(context,R.drawable.ic_image),arg_entity);

            } else if (arg_entity.getResourceType().equalsIgnoreCase("MP4") || arg_entity.getResourceType().equalsIgnoreCase("MOV")) {
//                    HaagstreitApp.imageLoader.displayImage(arg_entity.getImagePath(), this.imgFileFolder,HaagstreitApp.displayImageVideoFile);
//                imageLoader.get(url, ImageLoader.getImageListener(imageView,
//                        R.drawable.ic_video, R.drawable.ic_video));

                loadImageInImageView(imageView,url, ContextCompat.getDrawable(context,R.drawable.ic_video),arg_entity);

            } else if (arg_entity.getResourceType().equalsIgnoreCase("MP3")) {

//                    HaagstreitApp.imageLoader.displayImage(arg_entity.getImagePath(), this.imgFileFolder,HaagstreitApp.displayImageAudioFile);
//                imageLoader.get(url, ImageLoader.getImageListener(imageView,
//                        R.drawable.ic_audio, R.drawable.ic_audio));

                loadImageInImageView(imageView,url, ContextCompat.getDrawable(context,R.drawable.ic_audio),arg_entity);

            } else if (arg_entity.getResourceType().equalsIgnoreCase("PPT")) {

//                    HaagstreitApp.imageLoader.displayImage(arg_entity.getImagePath(), this.imgFileFolder,HaagstreitApp.displayImagePPTFile);

//                imageLoader.get(url, ImageLoader.getImageListener(imageView,
//                        R.drawable.ic_ppt, R.drawable.ic_ppt));

                loadImageInImageView(imageView,url, ContextCompat.getDrawable(context,R.drawable.ic_ppt),arg_entity);
            } else if (arg_entity.getResourceType().equalsIgnoreCase("XLS")) {
//                    HaagstreitApp.imageLoader.displayImage(arg_entity.getImagePath(), this.imgFileFolder,HaagstreitApp.displayImageXLSFile);
//                imageLoader.get(url, ImageLoader.getImageListener(imageView,
//                        R.drawable.ic_xls, R.drawable.ic_xls));

                loadImageInImageView(imageView,url, ContextCompat.getDrawable(context,R.drawable.ic_xls),arg_entity);
            } else {
//                    HaagstreitApp.imageLoader.displayImage(arg_entity.getImagePath(), this.imgFileFolder,HaagstreitApp.displayImageDocFile);
//                imageLoader.get(url, ImageLoader.getImageListener(imageView,
//                        R.drawable.ic_doc, R.drawable.ic_doc));

                loadImageInImageView(imageView,url, ContextCompat.getDrawable(context,R.drawable.ic_doc),arg_entity);
            }


//            imageView.setImageUrl(url, imageLoader);

        }

        @Override
        public void onClick(View v) {
//            v.setTag(position);
//            aOnClickListener.onClick(v);
           if(v!=null) {
                switch (v.getId()) {
                    case R.id.img_delete:
                        v.setTag(position);
                        aOnClickListener.onClick(v);
                        break;
                    case R.id.ll_res_desc:
                        v.setTag(position);
                        aOnClickListener.onClick(v);
                        break;
                    case R.id.img_resource:
                        v.setTag(position);
                        aOnClickListener.onClick(v);
                        break;
                    case R.id.chk_select:
                        v.setTag(position);
                        aOnClickListener.onClick(v);
                        break;
                    case R.id.rl_row_parent:
                        if(!isFromDownloaded) {
                            v.setTag(position);
                            aOnClickListener.onClick(v);
                        }
                        break;
                }
//            v.setTag(position);
//            aOnClickListener.onClick(v);
            }
        }
    }
}
