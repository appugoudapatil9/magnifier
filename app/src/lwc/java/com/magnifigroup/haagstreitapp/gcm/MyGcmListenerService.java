/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.magnifigroup.haagstreitapp.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.cygent.framework.utils.AppLog;
import com.cygent.framework.utils.Constants;
import com.cygent.model.ModelApp;
import com.cygent.model.entities.response.NotificationResponse;
import com.cygent.model.entities.response.ThemeCustomizeSetting;
import com.google.android.gms.gcm.GcmListenerService;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;
import com.magnifigroup.haagstreitapp.R;
import com.magnifigroup.haagstreitapp.activity.MainActivity;
import com.magnifigroup.haagstreitapp.utils.AppConstants;
import com.magnifigroup.haagstreitapp.utils.CommonUtils;
import com.orhanobut.logger.Logger;

import java.util.HashMap;
import java.util.Map;

public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";
    private static final Integer NOTIFICATION_ID_THEME = 101;
    private static final Integer NOTIFICATION_ID_RESOURCE_UPDATE = 102;

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString("message");
        Logger.d("From: " + from);
        Logger.d("Message: " + message);
        NotificationResponse response = null;
        ThemeCustomizeSetting themeCustomizeSetting = null;

        Gson gson = new Gson();

        if (message != null && !(message.isEmpty())) {
            try{
                response = gson.fromJson(message, NotificationResponse.class);
                themeCustomizeSetting = response.getResponseJSON();
                if (themeCustomizeSetting != null) {
                    if (themeCustomizeSetting.getNotificationType() == 0)  //notification for theme update
                    {
                        ModelApp.preferencePutBoolean(Constants.SharedPrefKeys.IS_THEME_UPDATE_REQUIRED, true);
                        CommonUtils.saveAppTheme(themeCustomizeSetting);

                        //send theme broadcast
                        Intent intent = new Intent(AppConstants.ACTION_THEME_CHANGED);
                        sendBroadcast(intent);
                    } else if (themeCustomizeSetting.getNotificationType() == 1) // notification for remove add
                    {

                    }
                }
            }catch (JsonSyntaxException e)
            {
                AppLog.d(TAG,"JsonSyntaxException");
            }catch (JsonParseException e)
            {
                AppLog.d(TAG,"JsonParseException");
            }catch (Exception e)
            {
                AppLog.d(TAG,"Exception");
            }
        }

        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */
        if(themeCustomizeSetting!=null)
        {
            if(!(themeCustomizeSetting.getNotificationMessage().equals(""))){
            sendNotification(themeCustomizeSetting);
            }
        }
        // [END_EXCLUDE]
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    private void sendNotification(ThemeCustomizeSetting arg_response) {

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        // TODO 1064 update notification
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(arg_response.getNotificationMessage())
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (arg_response.getNotificationType() == 0) {
            notificationManager.notify(NOTIFICATION_ID_THEME /* ID of notification */, notificationBuilder.build());
        } else if (arg_response.getNotificationType() == 1) {
            notificationManager.notify(NOTIFICATION_ID_RESOURCE_UPDATE /* ID of notification */, notificationBuilder.build());
        }
    }
}
