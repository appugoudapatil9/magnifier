package com.magnifigroup.haagstreitapp.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.cygent.framework.utils.AppLog;
import com.cygent.framework.utils.Constants;
import com.cygent.model.ModelApp;
import com.cygent.model.entities.response.NotificationResponse;
import com.cygent.model.entities.response.ThemeCustomizeSetting;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;
import com.magnifigroup.haagstreitapp.R;
import com.magnifigroup.haagstreitapp.activity.MainActivity;
import com.magnifigroup.haagstreitapp.utils.AppConstants;
import com.magnifigroup.haagstreitapp.utils.CommonUtils;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMessagingServ";
    private static final Integer NOTIFICATION_ID_THEME = 101;
    private static final Integer NOTIFICATION_ID_RESOURCE_UPDATE = 102;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.e(TAG, "onMessageReceived: " + remoteMessage.getData());
        String message = remoteMessage.getData().get("message");
        NotificationResponse response = null;
        ThemeCustomizeSetting themeCustomizeSetting = null;

        Gson gson = new Gson();

        if (message != null && !(message.isEmpty())) {
            try{
                response = gson.fromJson(message, NotificationResponse.class);
                themeCustomizeSetting = response.getResponseJSON();
                if (themeCustomizeSetting != null) {
                    if (themeCustomizeSetting.getNotificationType() == 0)  //notification for theme update
                    {
                        ModelApp.preferencePutBoolean(Constants.SharedPrefKeys.IS_THEME_UPDATE_REQUIRED, true);
                        CommonUtils.saveAppTheme(themeCustomizeSetting);

                        //send theme broadcast
                        Intent intent = new Intent(AppConstants.ACTION_THEME_CHANGED);
                        sendBroadcast(intent);
                    } else if (themeCustomizeSetting.getNotificationType() == 1) // notification for remove add
                    {

                    }
                }
            }catch (JsonSyntaxException e)
            {
                AppLog.d(TAG,"JsonSyntaxException");
            }catch (JsonParseException e)
            {
                AppLog.d(TAG,"JsonParseException");
            }catch (Exception e)
            {
                AppLog.d(TAG,"Exception");
            }
        }

        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */
        if(themeCustomizeSetting!=null)
        {
            if(!(themeCustomizeSetting.getNotificationMessage().equals(""))){
                sendNotification(themeCustomizeSetting);
            }
        }
    }

    private void sendNotification(ThemeCustomizeSetting arg_response) {

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        // TODO 1064 update notification
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(arg_response.getNotificationMessage())
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (arg_response.getNotificationType() == 0) {
            notificationManager.notify(NOTIFICATION_ID_THEME /* ID of notification */, notificationBuilder.build());
        } else if (arg_response.getNotificationType() == 1) {
            notificationManager.notify(NOTIFICATION_ID_RESOURCE_UPDATE /* ID of notification */, notificationBuilder.build());
        }
    }
}
