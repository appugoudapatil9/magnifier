package com.magnifigroup.haagstreitapp.gcm;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.cygent.framework.utils.Constants;
import com.cygent.model.ModelApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.magnifigroup.haagstreitapp.mvp.presenters.LoginPresenter;

public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseInstanceIdSer";
    private LoginPresenter mLoginPresenter;

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        try {
            ModelApp.preferencePutString(Constants.SharedPrefKeys.GCM_TOKEN,refreshedToken);
            //sharedPreferences.edit().putString(QuickstartPreferences.REGISTRATION_TOKEN,refreshedToken).apply();
            sendRegistrationToServer(refreshedToken);
            sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, true).apply();
        } catch (Exception e) {
            sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false).apply();
        }
        Intent registrationComplete = new Intent(QuickstartPreferences.REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
        //SharedPrefUtils.setString(Constants.SharedPrefs.PUSH_TOKEN, refreshedToken);

    }

    private void sendRegistrationToServer(String token) {
        mLoginPresenter.registerDeviceToken(token);
    }
}
